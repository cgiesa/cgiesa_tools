# stdlib modules
from __future__ import absolute_import

# third party modules
from maya import cmds
from maya.api import OpenMaya as om2
from cgiesa_tools.maya.rigging.controller import create_ctl
from cgiesa_tools.maya.rigging.controller import create_on_transform
from cgiesa_tools.maya.rigging.controller import CTL_TYPE
from cgiesa_tools.maya.rigging.transform import lock_and_hide_transform_attrs
from cgiesa_tools.maya.rigging.node import lock_and_hide_attrs
from cgiesa_tools.maya.rigging.node import lock_and_hide_keyable_attrs

CENTER_COLOR = 17
LEFT_COLOR = 6
RIGHT_COLOR = 13
GIMBAL_COLOR = 26


def _create_groups(parent):
    rig_setup_grp = cmds.createNode("transform", name="rig_setup_GRP", parent=parent)
    clean_grp = cmds.createNode("transform", name="clean_GRP", parent=parent)

    lock_and_hide_keyable_attrs(rig_setup_grp)
    lock_and_hide_keyable_attrs(clean_grp)

    return rig_setup_grp, clean_grp


def _create_main_setup(parent):
    main_hook_offset_grp = cmds.createNode("transform", name="MAIN_HOOK_OFFSET", parent=parent)
    main_hook_grp = cmds.createNode("transform", name="MAIN_HOOK", parent=main_hook_offset_grp)
    main_ctl = create_ctl(CTL_TYPE.ARROWSQUARE, "MAIN_CTL", size=0.8, parent=main_hook_grp, color=CENTER_COLOR)
    main_gimbal_ctl = create_ctl(CTL_TYPE.CIRCLE, "MAIN_GIMBAL_CTL", size=0.45, parent=main_ctl, color=GIMBAL_COLOR)
    main_gimbal_ctl_shape = cmds.listRelatives(main_gimbal_ctl, shapes=True, fullPath=True)[0]

    cmds.setAttr("{}.translateZ".format(main_hook_offset_grp), -0.3)
    lock_and_hide_transform_attrs(main_hook_offset_grp)

    # set the controller shapes
    cmds.scale(1.4, 1.4, 1.4,
               "{}.cv[1]".format(main_gimbal_ctl_shape),
               "{}.cv[3]".format(main_gimbal_ctl_shape),
               "{}.cv[5]".format(main_gimbal_ctl_shape),
               "{}.cv[7]".format(main_gimbal_ctl_shape),
               pivot=(0, 0, -0.4))

    # attributes
    cmds.addAttr(main_ctl,
                 longName="EXTRA_________", shortName="EXTRA_________", niceName="EXTRA_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.EXTRA_________".format(main_ctl), lock=True)
    cmds.addAttr(main_ctl, longName="globalScale", attributeType="float", keyable=True, defaultValue=1.0, minValue=0.01)

    cmds.connectAttr("{}.globalScale".format(main_ctl), "{}.sx".format(main_ctl))
    cmds.connectAttr("{}.globalScale".format(main_ctl), "{}.sy".format(main_ctl))
    cmds.connectAttr("{}.globalScale".format(main_ctl), "{}.sz".format(main_ctl))

    cmds.addAttr(main_ctl,
                 longName="VISIBILITY_________", shortName="VISIBILITY_________", niceName="VISIBILITY_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.VISIBILITY_________".format(main_ctl), lock=True)
    cmds.addAttr(main_ctl, longName="showGimbal", attributeType="bool", keyable=True, defaultValue=False)
    cmds.addAttr(main_ctl, longName="showControllers", attributeType="bool", keyable=True, defaultValue=True)
    cmds.addAttr(main_ctl, longName="showRig", attributeType="bool", keyable=True, defaultValue=False)
    cmds.addAttr(main_ctl, longName="showSkinJoints", attributeType="bool", keyable=True, defaultValue=False)
    cmds.connectAttr("{}.showGimbal".format(main_ctl), "{}.visibility".format(main_gimbal_ctl_shape))

    cmds.setAttr("{}.visibility".format(main_ctl), keyable=False)
    cmds.setAttr("{}.visibility".format(main_gimbal_ctl), keyable=False)

    lock_and_hide_attrs(main_ctl, ["s", "sx", "sy", "sz"])
    lock_and_hide_attrs(main_gimbal_ctl, ["s", "sx", "sy", "sz"])

    # add blendTwoAttr for joint draw style
    blend = cmds.createNode("blendTwoAttr", name="MAIN_jointDrawStyle_BTA")
    cmds.setAttr("{}.input[0]".format(blend), 2.0)
    cmds.setAttr("{}.input[1]".format(blend), 0.0)
    cmds.setAttr("{}.input".format(blend), lock=True)
    cmds.connectAttr("{}.showRig".format(main_ctl), "{}.attributesBlender".format(blend))

    blend = cmds.createNode("blendTwoAttr", name="MAIN_skinJointDrawStyle_BTA")
    cmds.setAttr("{}.input[0]".format(blend), 2.0)
    cmds.setAttr("{}.input[1]".format(blend), 0.0)
    cmds.setAttr("{}.input".format(blend), lock=True)
    cmds.connectAttr("{}.showSkinJoints".format(main_ctl), "{}.attributesBlender".format(blend))

    return main_ctl, main_gimbal_ctl


def _create_cog_setup(parent, joint):
    cog_ctl = create_ctl(CTL_TYPE.CIRCLE, "COG_CTL", size=0.6, parent=parent, add_hook=True, color=CENTER_COLOR)
    cog_gimbal_ctl = create_ctl(CTL_TYPE.CIRCLE, "COG_GIMBAL_CTL", size=0.4, parent=cog_ctl, color=GIMBAL_COLOR)
    cog_hook = cmds.listRelatives(cog_ctl, parent=True, fullPath=True)[0]

    cog_gimbal_ctl_shape = cmds.listRelatives(cog_gimbal_ctl, shapes=True, fullPath=True)[0]

    # position the controller
    translation = cmds.xform(joint, query=True, worldSpace=True, translation=True)
    cmds.xform(cog_hook, worldSpace=True, translation=translation)

    # set the controller shapes
    cmds.move(0.0, 0.12, -0.28, "{}.cv[*]".format(cog_ctl), relative=True)
    cmds.move(0.0, 0.12, -0.28, "{}.cv[*]".format(cog_gimbal_ctl), relative=True)

    cmds.scale(0.6, 1.0, 1.0, "{}.cv[2]".format(cog_gimbal_ctl), "{}.cv[6]".format(cog_gimbal_ctl), pivot=(0, 0.12, -0.28))

    cmds.scale(0.46, 0.46, 0.46, "{}.cv[2]".format(cog_ctl), "{}.cv[6]".format(cog_ctl), pivot=(0, 0.12, -0.28))
    cmds.scale(0.9, 1.0, 0.78, "{}.cv[*]".format(cog_ctl), pivot=(0, 0.12, -0.28))

    # attributes
    lock_and_hide_transform_attrs(cog_hook)

    cmds.addAttr(cog_ctl,
                 longName="VISIBILITY_________", shortName="VISIBILITY_________", niceName="VISIBILITY_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.VISIBILITY_________".format(cog_ctl), lock=True)
    cmds.addAttr(cog_ctl, longName="showGimbal", attributeType="bool", keyable=True, defaultValue=False)
    cmds.connectAttr("{}.showGimbal".format(cog_ctl), "{}.visibility".format(cog_gimbal_ctl_shape))

    cmds.setAttr("{}.visibility".format(cog_ctl), keyable=False)
    cmds.setAttr("{}.visibility".format(cog_gimbal_ctl), keyable=False)

    lock_and_hide_attrs(cog_ctl, ["s", "sx", "sy", "sz"])
    lock_and_hide_attrs(cog_gimbal_ctl, ["s", "sx", "sy", "sz"])

    # link to joint
    cmds.parentConstraint(cog_gimbal_ctl, joint)
    cmds.scaleConstraint(cog_gimbal_ctl, joint)

    return cog_ctl, cog_gimbal_ctl


def _rename_joint_hierarchy(joint, dupl_joint, replace):
    joint_name = joint.rpartition("|")[2]
    new_name = joint_name.replace(replace[0], replace[1])
    dupl_joint = cmds.rename(dupl_joint, new_name)

    children = cmds.ls(cmds.listRelatives(joint, fullPath=True, children=True), type="joint")
    dupl_children = cmds.ls(cmds.listRelatives(dupl_joint, fullPath=True, children=True), type="joint")

    for child, dupl_child in zip(children, dupl_children):
        _rename_joint_hierarchy(child, dupl_child, replace)

    return dupl_joint


def _duplicate_joint_hierarchy(joint, replace):
    dupl_joint = cmds.duplicate(joint)[0]
    dupl_joint = _rename_joint_hierarchy(joint, dupl_joint, replace)
    return dupl_joint


def _find_foot_matrix(tip_joint):
    matrix = cmds.xform(tip_joint, query=True, worldSpace=True, matrix=True)

    position = [matrix[12], matrix[13], matrix[14]]

    vector1 = om2.MVector([matrix[0], matrix[1], matrix[2]])
    vector1.normalize()
    vector2 = om2.MVector([0.0, -1.0, 0.0])
    dot = vector1 * vector2
    length = position[1]
    hypo = length / dot

    pos = om2.MPoint([position[0], position[1], position[2], 1.0])
    foot_pos = pos + vector1 * hypo

    x_vector = om2.MVector([vector1[0], 0.0, vector1[2]])
    x_vector.normalize()
    y_vector = om2.MVector.kYaxisVector
    z_vector = x_vector ^ y_vector

    foot_matrix = [x_vector[0], x_vector[1], x_vector[2], 0.0,
                   y_vector[0], y_vector[1], y_vector[2], 0.0,
                   z_vector[0], z_vector[1], z_vector[2], 0.0,
                   foot_pos[0], foot_pos[1], foot_pos[2], 1.0]

    return foot_matrix


def _create_pole_vector(name, ik_handle, clean_grp, parents, color):
    joints = cmds.ikHandle(ik_handle, query=True, jointList=True)
    pole_vector = cmds.getAttr("{}.poleVector".format(ik_handle))[0]
    pole_vector = om2.MVector(pole_vector)
    pole_vector *= 0.04

    root_joint = joints[0]
    root_matrix = cmds.xform(root_joint, query=True, worldSpace=True, matrix=True)
    root_child = cmds.ls(cmds.listRelatives(root_joint, children=True, fullPath=True), type="joint")[0]

    child_pos = cmds.xform(root_child, query=True, worldSpace=True, translation=True)

    pole_vector_pos = [child_pos[0] + pole_vector[0], child_pos[1] + pole_vector[1], child_pos[2] + pole_vector[2]]

    pole_vector_ctl = create_ctl(CTL_TYPE.BALL, name=name, size=0.006, parent=clean_grp, add_hook=True, color=color)
    pole_vector_ctl_hook = cmds.listRelatives(pole_vector_ctl, parent=True, fullPath=True)[0]

    cmds.xform(pole_vector_ctl_hook, worldSpace=True, translation=pole_vector_pos)

    # follow setup
    parent = cmds.listRelatives(root_joint, parent=True, fullPath=True)[0]

    up_vector_grp = cmds.createNode("transform", name="{}_up_GRP".format(name), parent=parent)
    cmds.xform(up_vector_grp, worldSpace=True, matrix=root_matrix)

    tip_joint = joints[-1]
    tip_matrix = cmds.xform(tip_joint, query=True, worldSpace=True, matrix=True)
    aim = om2.MVector([tip_matrix[12] - root_matrix[12], tip_matrix[13] - root_matrix[13], tip_matrix[14] - root_matrix[14]])
    aim.normalize()

    aim_follow_grp = cmds.createNode("transform", name="{}_follow_GRP".format(name), parent=clean_grp)
    cmds.xform(aim_follow_grp, worldSpace=True, matrix=root_matrix)
    cmds.aimConstraint(ik_handle, aim_follow_grp,
                       aimVector=(1.0, 0.0, 0.0),
                       upVector=(0.0, 0.0, 1.0),
                       worldUpObject=up_vector_grp,
                       worldUpType="objectrotation",
                       worldUpVector=(0.0, 0.0, 1.0))
    cmds.pointConstraint(up_vector_grp, aim_follow_grp)

    # setup the parent follow system
    parents += [("FOLLOW", aim_follow_grp)]

    parent_names = []
    parent_nodes = []
    for parent_name, parent_node in parents:
        parent_names.append(parent_name)
        parent_nodes.append(parent_node)

    cmds.addAttr(pole_vector_ctl,
                 longName="EXTRA_________", shortName="EXTRA_________", niceName="EXTRA_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.EXTRA_________".format(pole_vector_ctl), lock=True)
    cmds.addAttr(pole_vector_ctl, longName="follow", keyable=True, attributeType="enum", enumName=":".join(parent_names))

    _setup_multi_parents(
        "parent", name, pole_vector_ctl_hook, "{}.follow".format(pole_vector_ctl),
        parent_names, parent_nodes, clean_grp)

    cmds.poleVectorConstraint(pole_vector_ctl, ik_handle)

    lock_and_hide_keyable_attrs(pole_vector_ctl_hook)
    lock_and_hide_keyable_attrs(aim_follow_grp)
    lock_and_hide_attrs(pole_vector_ctl, ["r", "rx", "ry", "rz", "s", "sx", "sy", "sz"])
    cmds.setAttr("{}.v".format(pole_vector_ctl), keyable=False, channelBox=False)

    return pole_vector_ctl, pole_vector_ctl_hook, up_vector_grp


def _blend_joint_chain(blend_joint, fk_joint, ik_joint, blend_attr):
    joint_name = blend_joint.rpartition("|")[2]
    blend = cmds.createNode("pairBlend", name="{}_IK_blend".format(joint_name))

    cmds.connectAttr("{}.tx".format(fk_joint), "{}.inTranslateX1".format(blend))
    cmds.connectAttr("{}.ty".format(fk_joint), "{}.inTranslateY1".format(blend))
    cmds.connectAttr("{}.tz".format(fk_joint), "{}.inTranslateZ1".format(blend))
    cmds.connectAttr("{}.rx".format(fk_joint), "{}.inRotateX1".format(blend))
    cmds.connectAttr("{}.ry".format(fk_joint), "{}.inRotateY1".format(blend))
    cmds.connectAttr("{}.rz".format(fk_joint), "{}.inRotateZ1".format(blend))

    cmds.connectAttr("{}.tx".format(ik_joint), "{}.inTranslateX2".format(blend))
    cmds.connectAttr("{}.ty".format(ik_joint), "{}.inTranslateY2".format(blend))
    cmds.connectAttr("{}.tz".format(ik_joint), "{}.inTranslateZ2".format(blend))
    cmds.connectAttr("{}.rx".format(ik_joint), "{}.inRotateX2".format(blend))
    cmds.connectAttr("{}.ry".format(ik_joint), "{}.inRotateY2".format(blend))
    cmds.connectAttr("{}.rz".format(ik_joint), "{}.inRotateZ2".format(blend))

    cmds.connectAttr("{}.outTranslateX".format(blend), "{}.tx".format(blend_joint))
    cmds.connectAttr("{}.outTranslateY".format(blend), "{}.ty".format(blend_joint))
    cmds.connectAttr("{}.outTranslateZ".format(blend), "{}.tz".format(blend_joint))
    cmds.connectAttr("{}.outRotateX".format(blend), "{}.rx".format(blend_joint))
    cmds.connectAttr("{}.outRotateY".format(blend), "{}.ry".format(blend_joint))
    cmds.connectAttr("{}.outRotateZ".format(blend), "{}.rz".format(blend_joint))

    cmds.connectAttr(blend_attr, "{}.weight".format(blend))
    cmds.setAttr("{}.rotInterpolation".format(blend), 1)

    blend_children = cmds.listRelatives(blend_joint, children=True, fullPath=True) or []
    fk_children = cmds.listRelatives(fk_joint, children=True, fullPath=True) or []
    ik_children = cmds.listRelatives(ik_joint, children=True, fullPath=True) or []

    for blend_child, fk_child, ik_child in zip(blend_children, fk_children, ik_children):
        _blend_joint_chain(blend_child, fk_child, ik_child, blend_attr)


def _create_point_follow_setup(name, blend_attrs, targets, target_names, transform, parent_grp):
    # build the follow setup for each target
    const_grps = []
    for target, target_name in zip(targets, target_names):
        follow_grp = cmds.createNode("transform", name="{}_follow{}_GRP".format(name, target_name), parent=parent_grp)
        follow_const_grp = cmds.createNode("transform", name="{}_followConst{}_GRP".format(name, target_name), parent=follow_grp)
        const_grps.append(follow_const_grp)

        translation = cmds.xform(target, query=True, worldSpace=True, translation=True)
        cmds.xform(follow_grp, worldSpace=True, translation=translation)
        translation = cmds.xform(transform, query=True, worldSpace=True, translation=True)
        cmds.xform(follow_const_grp, worldSpace=True, translation=translation)

        cmds.pointConstraint(target, follow_grp)

        lock_and_hide_keyable_attrs(follow_grp)
        lock_and_hide_keyable_attrs(follow_const_grp)

    # build a fix group
    fix_grp = cmds.createNode("transform", name="{}_followFix_GRP".format(name), parent=parent_grp)
    translation = cmds.xform(transform, query=True, worldSpace=True, translation=True)
    cmds.xform(fix_grp, worldSpace=True, translation=translation)
    const_grps.append(fix_grp)

    # constrain the transform
    constraint = cmds.pointConstraint(const_grps + [transform])[0]

    # setup the blending
    add_weights = cmds.createNode("plusMinusAverage", name="{}_followBlend_PMA".format(name))
    for i, attr in enumerate(blend_attrs):
        weight_plug = cmds.listConnections("{}.target[{}].targetWeight".format(constraint, i), source=True, destination=False, plugs=True)[0]
        cmds.connectAttr(attr, weight_plug)
        cmds.connectAttr(attr, "{}.input1D[{}]".format(add_weights, i))

    reverse = cmds.createNode("reverse", name="{}_followBlend_REV".format(name))
    cmds.connectAttr("{}.output1D".format(add_weights), "{}.inputX".format(reverse))

    condition = cmds.createNode("condition", name="{}_followBlend_COND".format(name))
    cmds.setAttr("{}.operation".format(condition), 2)  # greater than
    cmds.setAttr("{}.operation".format(condition), lock=True)
    cmds.setAttr("{}.secondTerm".format(condition), 1.0)
    cmds.setAttr("{}.secondTerm".format(condition), lock=True)
    cmds.setAttr("{}.colorIfTrueR".format(condition), 0.0)
    cmds.setAttr("{}.colorIfTrueR".format(condition), lock=True)
    cmds.connectAttr("{}.output1D".format(add_weights), "{}.firstTerm".format(condition))
    cmds.connectAttr("{}.outputX".format(reverse), "{}.colorIfFalseR".format(condition))

    weight_plug = cmds.listConnections("{}.target[{}].targetWeight".format(constraint, len(targets)), source=True, destination=False, plugs=True)[0]
    cmds.connectAttr("{}.outColorR".format(condition), weight_plug)


def _create_single_leg_ik(name, root_joint, mid_joint, tip_joint, joint_parent,
                          joint_parent_grp, leg_root_grp, pv_parent, show_rig_plug,
                          draw_style_plug, color):
    ctls = []

    root_ik_joint = _duplicate_joint_hierarchy(root_joint, replace=("_JNT", "_ik_JNT"))
    tip_ik_joint = cmds.ls(tip_joint.rpartition("|")[2].replace("_JNT", "_ik_JNT"))[0]

    root_ik_joint = cmds.parent(root_ik_joint, joint_parent_grp)[0]

    # ik handles
    tip_handle, tip_effector = cmds.ikHandle(
        startJoint=root_ik_joint, endEffector=tip_ik_joint, snapHandleFlagToggle=False, sticky="sticky")

    ik_ctl = create_ctl(CTL_TYPE.BOX, "{}_IK_CTL".format(name), size=0.03,
                        parent=leg_root_grp, add_hook=True, color=color)
    ik_ctl_hook = cmds.listRelatives(ik_ctl, parent=True, fullPath=True)[0]
    ctls.append(ik_ctl)

    translation = cmds.xform(tip_ik_joint, query=True, worldSpace=True, translation=True)
    cmds.xform(ik_ctl_hook, worldSpace=True, translation=translation)

    tip_pv_ctl, tip_pv_ctl_hook, tip_pv_up_grp = _create_pole_vector(
        "{}_IK_pv_CTL".format(name), tip_handle, leg_root_grp, [("COG", pv_parent)], color)

    ctls.append(tip_pv_ctl)

    tip_pv_up_offset_grp = cmds.createNode("transform", name="{}_IK_pv_up_offset_GRP".format(name), parent=joint_parent)
    matrix = cmds.xform(tip_pv_up_grp, query=True, worldSpace=True, matrix=True)
    cmds.xform(tip_pv_up_offset_grp, worldSpace=True, matrix=matrix)

    cmds.orientConstraint(tip_pv_up_offset_grp, tip_pv_up_grp)

    tip_handle = cmds.parent(tip_handle, ik_ctl)[0]

    # foot
    parent_joint = cmds.ls(cmds.listRelatives(tip_ik_joint, parent=True, fullPath=True), type="joint")[0]

    tip_ctl = create_ctl(CTL_TYPE.CIRCLEX, name="{}_tip_CTL".format(name), size=0.017,
                         parent=parent_joint, add_hook=True, color=color)
    tip_ctl_hook = cmds.listRelatives(tip_ctl, parent=True, fullPath=True)[0]

    matrix = cmds.xform(tip_ik_joint, query=True, worldSpace=True, matrix=True)
    cmds.xform(tip_ctl_hook, worldSpace=True, matrix=matrix)

    # foot_ctl = create_ctl(CTL_TYPE.CIRCLEX, name="{}_foot_IK_CTL".format(name), size=0.015,
    #                       parent=tip_ctl, add_hook=True, color=color)
    # foot_ctl_hook = cmds.listRelatives(foot_ctl, parent=True, fullPath=True)[0]

    # foot_ik_joint = cmds.ls(cmds.listRelatives(tip_ik_joint, children=True, fullPath=True), type="joint")[0]
    # matrix = cmds.xform(foot_ik_joint, query=True, worldSpace=True, matrix=True)
    # cmds.xform(foot_ctl_hook, worldSpace=True, matrix=matrix)

    cmds.orientConstraint(ik_ctl, tip_ctl_hook, maintainOffset=True)
    cmds.orientConstraint(tip_ctl, tip_ik_joint)
    # cmds.orientConstraint(foot_ctl, foot_ik_joint)

    ctls.append(tip_ctl)
    # ctls.append(foot_ctl)

    # clean
    lock_and_hide_keyable_attrs(ik_ctl_hook)
    lock_and_hide_keyable_attrs(tip_handle)
    # lock_and_hide_keyable_attrs(foot_ctl_hook)

    # lock_and_hide_attrs(foot_ctl, ["t", "tx", "ty", "tz", "s", "sx", "sy", "sz"])
    # cmds.setAttr("{}.v".format(foot_ctl), keyable=False, channelBox=False)

    lock_and_hide_attrs(ik_ctl, ["s", "sx", "sy", "sz"])
    cmds.setAttr("{}.v".format(ik_ctl), keyable=False, channelBox=False)

    cmds.setAttr("{}.overrideEnabled".format(tip_handle), True)
    cmds.connectAttr(show_rig_plug, "{}.overrideVisibility".format(tip_handle))

    for joint in cmds.ls(root_ik_joint, dag=True, type="joint"):
        cmds.connectAttr(draw_style_plug, "{}.drawStyle".format(joint))

    return root_ik_joint, ctls


def _create_double_leg_ik(name, root_joint, mid_joint, tip_joint, joint_parent,
                          joint_parent_grp, leg_root_grp, show_rig_plug,
                          draw_style_plug, color):
    ctls = []

    root_ik_joint = _duplicate_joint_hierarchy(root_joint, replace=("_JNT", "_ik_JNT"))
    mid_ik_joint = cmds.ls(mid_joint.rpartition("|")[2].replace("_JNT", "_ik_JNT"))[0]
    tip_ik_joint = cmds.ls(tip_joint.rpartition("|")[2].replace("_JNT", "_ik_JNT"))[0]

    root_ik_joint = cmds.parent(root_ik_joint, joint_parent_grp)[0]

    # ik handles
    mid_handle, mid_effector = cmds.ikHandle(
        startJoint=root_ik_joint, endEffector=mid_ik_joint, snapHandleFlagToggle=False, sticky="sticky")
    tip_handle, tip_effector = cmds.ikHandle(
        startJoint=mid_ik_joint, endEffector=tip_ik_joint, snapHandleFlagToggle=False, sticky="sticky")

    mid_ik_ctl = create_ctl(CTL_TYPE.BOX, "{}_mid_IK_CTL".format(name), size=0.03,
                            parent=leg_root_grp, add_hook=True, color=color)
    mid_ik_ctl_hook = cmds.listRelatives(mid_ik_ctl, parent=True, fullPath=True)[0]
    ctls.append(mid_ik_ctl)

    matrix = cmds.xform(mid_ik_joint, query=True, worldSpace=True, matrix=True)
    cmds.xform(mid_ik_ctl_hook, worldSpace=True, matrix=matrix)

    ik_ctl = create_ctl(CTL_TYPE.BOX, "{}_IK_CTL".format(name), size=0.03,
                        parent=leg_root_grp, add_hook=True, color=color)
    ik_ctl_hook = cmds.listRelatives(ik_ctl, parent=True, fullPath=True)[0]
    ctls.append(ik_ctl)

    translation = cmds.xform(tip_ik_joint, query=True, worldSpace=True, translation=True)
    cmds.xform(ik_ctl_hook, worldSpace=True, translation=translation)

    mid_pv_ctl, mid_pv_ctl_hook, mid_pv_up_grp = _create_pole_vector(
        "{}_mid_IK_pv_CTL".format(name), mid_handle, leg_root_grp, [("COG", joint_parent)], color)
    tip_pv_ctl, tip_pv_ctl_hook, tip_pv_up_grp = _create_pole_vector(
        "{}_IK_pv_CTL".format(name), tip_handle, leg_root_grp, [("COG", joint_parent)], color)

    ctls.append(mid_pv_ctl)
    ctls.append(tip_pv_ctl)

    tip_pv_up_offset_grp = cmds.createNode("transform", name="{}_IK_pv_up_offset_GRP".format(name), parent=mid_ik_ctl)
    matrix = cmds.xform(tip_pv_up_grp, query=True, worldSpace=True, matrix=True)
    cmds.xform(tip_pv_up_offset_grp, worldSpace=True, matrix=matrix)

    cmds.orientConstraint(tip_pv_up_offset_grp, tip_pv_up_grp)

    mid_handle = cmds.parent(mid_handle, mid_ik_ctl)[0]
    tip_handle = cmds.parent(tip_handle, ik_ctl)[0]

    # follow setup
    cmds.addAttr(mid_ik_ctl,
                 longName="EXTRA_________", shortName="EXTRA_________", niceName="EXTRA_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.EXTRA_________".format(mid_ik_ctl), lock=True)
    cmds.addAttr(mid_ik_ctl, longName="followParent", keyable=True, attributeType="float",
                 minValue=0.0, maxValue=1.0, defaultValue=0.4)
    cmds.addAttr(mid_ik_ctl, longName="followFoot", keyable=True, attributeType="float",
                 minValue=0.0, maxValue=1.0, defaultValue=0.2)

    _create_point_follow_setup("{}_mid_IK".format(name),
                               blend_attrs=["{}.followParent".format(mid_ik_ctl),
                                            "{}.followFoot".format(mid_ik_ctl)],
                               targets=[joint_parent, ik_ctl],
                               target_names=["Parent", "Foot"],
                               transform=mid_ik_ctl_hook,
                               parent_grp=leg_root_grp)

    # foot
    parent_joint = cmds.ls(cmds.listRelatives(tip_ik_joint, parent=True, fullPath=True), type="joint")[0]

    tip_ctl = create_ctl(CTL_TYPE.CIRCLEX, name="{}_tip_CTL".format(name), size=0.017,
                         parent=parent_joint, add_hook=True, color=color)
    tip_ctl_hook = cmds.listRelatives(tip_ctl, parent=True, fullPath=True)[0]

    matrix = cmds.xform(tip_ik_joint, query=True, worldSpace=True, matrix=True)
    cmds.xform(tip_ctl_hook, worldSpace=True, matrix=matrix)

    foot_ctl = create_ctl(CTL_TYPE.CIRCLEX, name="{}_foot_IK_CTL".format(name), size=0.015,
                          parent=tip_ctl, add_hook=True, color=color)
    foot_ctl_hook = cmds.listRelatives(foot_ctl, parent=True, fullPath=True)[0]

    foot_ik_joint = cmds.ls(cmds.listRelatives(tip_ik_joint, children=True, fullPath=True), type="joint")[0]
    matrix = cmds.xform(foot_ik_joint, query=True, worldSpace=True, matrix=True)
    cmds.xform(foot_ctl_hook, worldSpace=True, matrix=matrix)

    cmds.orientConstraint(ik_ctl, tip_ctl_hook, maintainOffset=True)
    cmds.orientConstraint(tip_ctl, tip_ik_joint)
    cmds.orientConstraint(foot_ctl, foot_ik_joint)

    ctls.append(tip_ctl)
    ctls.append(foot_ctl)

    # clean
    lock_and_hide_keyable_attrs(ik_ctl_hook)
    lock_and_hide_keyable_attrs(mid_ik_ctl_hook)
    lock_and_hide_keyable_attrs(mid_handle)
    lock_and_hide_keyable_attrs(tip_handle)
    lock_and_hide_keyable_attrs(foot_ctl_hook)

    lock_and_hide_attrs(foot_ctl, ["t", "tx", "ty", "tz", "s", "sx", "sy", "sz"])
    cmds.setAttr("{}.v".format(foot_ctl), keyable=False, channelBox=False)

    lock_and_hide_attrs(mid_ik_ctl, ["r", "rx", "ry", "rz", "s", "sx", "sy", "sz"])
    cmds.setAttr("{}.v".format(mid_ik_ctl), keyable=False, channelBox=False)

    lock_and_hide_attrs(ik_ctl, ["s", "sx", "sy", "sz"])
    cmds.setAttr("{}.v".format(ik_ctl), keyable=False, channelBox=False)

    cmds.setAttr("{}.overrideEnabled".format(mid_handle), True)
    cmds.connectAttr(show_rig_plug, "{}.overrideVisibility".format(mid_handle))
    cmds.setAttr("{}.overrideEnabled".format(tip_handle), True)
    cmds.connectAttr(show_rig_plug, "{}.overrideVisibility".format(tip_handle))

    for joint in cmds.ls(root_ik_joint, dag=True, type="joint"):
        cmds.connectAttr(draw_style_plug, "{}.drawStyle".format(joint))

    return root_ik_joint, ctls


def _create_tick_leg_setup(name, clean_grp, main_ctl, parent_ctl, joint_parent, pv_parent, root_joint, mid_joint, tip_joint, single_leg_ik, color):
    ctls = []

    # root setup
    clean_grp = cmds.createNode("transform", name="{}_clean_GRP".format(name), parent=clean_grp)
    lock_and_hide_keyable_attrs(clean_grp)

    parent_trs = cmds.listRelatives(root_joint, parent=True, fullPath=True)
    leg_root_grp = cmds.createNode("transform", name="{}_tick_leg_setup_GRP".format(name), parent=clean_grp)
    matrix = cmds.xform(parent_trs, query=True, worldSpace=True, matrix=True)
    cmds.xform(leg_root_grp, worldSpace=True, matrix=matrix)

    cmds.parentConstraint(parent_ctl, leg_root_grp)
    cmds.scaleConstraint(parent_ctl, leg_root_grp)

    joint_parent_grp = cmds.createNode("transform", name="{}_joints_GRP".format(name), parent=leg_root_grp)
    matrix = cmds.xform(joint_parent, query=True, worldSpace=True, matrix=True)
    cmds.xform(joint_parent_grp, worldSpace=True, matrix=matrix)

    cmds.parentConstraint(joint_parent, joint_parent_grp)

    lock_and_hide_keyable_attrs(leg_root_grp)
    lock_and_hide_keyable_attrs(joint_parent_grp)

    # -------------------------------------------------------------------------
    #                                 IK
    # -------------------------------------------------------------------------
    blend = cmds.listConnections("{}.showRig".format(main_ctl), source=False, destination=True, type="blendTwoAttr")[0]

    if single_leg_ik:
        root_ik_joint, ik_ctls = _create_single_leg_ik(
            name, root_joint, mid_joint, tip_joint, joint_parent, joint_parent_grp,
            leg_root_grp, pv_parent, "{}.showRig".format(main_ctl), "{}.output".format(blend), color)
    else:
        root_ik_joint, ik_ctls = _create_double_leg_ik(
            name, root_joint, mid_joint, tip_joint, joint_parent, joint_parent_grp,
            leg_root_grp, "{}.showRig".format(main_ctl), "{}.output".format(blend), color)

    ctls.extend(ik_ctls)

    # -------------------------------------------------------------------------
    #                                 FK
    # -------------------------------------------------------------------------
    root_fk_joint = _duplicate_joint_hierarchy(root_joint, replace=("_JNT", "_fk_JNT"))
    root_fk_joint = cmds.parent(root_fk_joint, joint_parent_grp)[0]
    fk_ctls = []

    mult = 1.0
    if root_fk_joint.startswith("R_"):
        mult = -1.0

    for joint in cmds.ls(root_fk_joint, dag=True, type="joint"):
        ctls.append(joint)

        create_on_transform(CTL_TYPE.CYLINDERX, joint, size=1.0, color=color)
        fk_ctls.append(joint)

        pivot = cmds.xform(joint, query=True, worldSpace=True, translation=True)

        children = cmds.ls(cmds.listRelatives(joint, children=True, fullPath=True) or [], type="joint")
        if children:
            length = cmds.getAttr("{}.tx".format(children[0]))
            cmds.scale(length * 0.85, 0.015, 0.015, "{}.cv[*]".format(joint), pivot=pivot, relative=True)
        else:
            cmds.scale(0.015 * mult, 0.01, 0.01, "{}.cv[*]".format(joint), pivot=pivot, relative=True)

        lock_and_hide_attrs(joint, ["s", "sx", "sy", "sz"])
        cmds.setAttr("{}.v".format(joint), keyable=False, channelBox=False)
        cmds.setAttr("{}.jointOrient".format(joint), keyable=False, channelBox=False)
        cmds.setAttr("{}.jointOrientX".format(joint), keyable=False, channelBox=False)
        cmds.setAttr("{}.jointOrientY".format(joint), keyable=False, channelBox=False)
        cmds.setAttr("{}.jointOrientZ".format(joint), keyable=False, channelBox=False)
        cmds.setAttr("{}.displayLocalAxis".format(joint), keyable=False, channelBox=False)
        cmds.setAttr("{}.radius".format(joint), keyable=False, channelBox=False)

    # some manual adjustments
    joint = cmds.ls(root_fk_joint, dag=True, type="joint")[-2]
    selection = ["{}.cv[17:27]".format(joint), "{}.cv[45:55]".format(joint), "{}.cv[73:83]".format(joint), "{}.cv[101:111]".format(joint), "{}.cv[129:139]".format(joint)]
    bb = cmds.exactWorldBoundingBox(selection)
    center = [(bb[0] + bb[3]) / 2.0, (bb[1] + bb[4]) / 2.0, (bb[2] + bb[5]) / 2.0]
    cmds.scale(0.7, 0.7, 0.7, selection, pivot=center, relative=True)

    selection = ["{}.cv[0]".format(joint),
                 "{}.cv[16]".format(joint),
                 "{}.cv[28]".format(joint),
                 "{}.cv[44]".format(joint),
                 "{}.cv[56]".format(joint),
                 "{}.cv[72]".format(joint),
                 "{}.cv[84]".format(joint),
                 "{}.cv[100]".format(joint),
                 "{}.cv[112]".format(joint),
                 "{}.cv[128]".format(joint)]
    bb = cmds.exactWorldBoundingBox(selection)
    center = [(bb[0] + bb[3]) / 2.0, (bb[1] + bb[4]) / 2.0, (bb[2] + bb[5]) / 2.0]
    cmds.scale(0.68, 0.68, 0.68, selection, pivot=center, relative=True)

    # -------------------------------------------------------------------------
    #                                SKIN
    # -------------------------------------------------------------------------
    leg_ctl = create_ctl(CTL_TYPE.DIAMOND, name="{}_leg_CTL".format(name), size=0.005, parent=leg_root_grp, add_hook=True, color=color)
    leg_ctl_hook = cmds.listRelatives(leg_ctl, parent=True, fullPath=True)[0]
    ctls.append(leg_ctl)

    tip_pos = cmds.xform(tip_joint, query=True, worldSpace=True, translation=True)
    if name.startswith("L_"):
        tip_pos = [tip_pos[0] + 0.03, tip_pos[1], tip_pos[2]]
    else:
        tip_pos = [tip_pos[0] - 0.03, tip_pos[1], tip_pos[2]]
    cmds.xform(leg_ctl_hook, worldSpace=True, translation=tip_pos)

    cmds.parentConstraint(tip_joint, leg_ctl_hook, maintainOffset=True)

    # attributes
    cmds.addAttr(leg_ctl,
                 longName="EXTRA_________", shortName="EXTRA_________", niceName="EXTRA_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.EXTRA_________".format(leg_ctl), lock=True)
    cmds.addAttr(leg_ctl, longName="blendIK", attributeType="float", keyable=True, defaultValue=1.0, minValue=0.0, maxValue=1.0)

    cmds.addAttr(leg_ctl,
                 longName="VISIBILITY_________", shortName="VISIBILITY_________", niceName="VISIBILITY_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.VISIBILITY_________".format(leg_ctl), lock=True)
    cmds.addAttr(leg_ctl, longName="showIKFK", attributeType="enum", enumName="BOTH:IK:FK", keyable=True, defaultValue=1)

    _blend_joint_chain(root_joint, root_fk_joint, root_ik_joint, "{}.blendIK".format(leg_ctl))

    mult = cmds.createNode("multDoubleLinear", name="{}_ikfkDisplay_MDL".format(name))
    cmds.setAttr("{}.i2".format(mult), 0.5)
    cmds.setAttr("{}.i2".format(mult), lock=True)
    cmds.connectAttr("{}.showIKFK".format(leg_ctl), "{}.i1".format(mult))

    reverse = cmds.createNode("reverse", name="{}_ikfkDisplay_REV".format(name))
    cmds.connectAttr("{}.showIKFK".format(leg_ctl), "{}.inputX".format(reverse))
    cmds.connectAttr("{}.output".format(mult), "{}.inputY".format(reverse))

    for ctl in fk_ctls:
        for shape in cmds.listRelatives(ctl, shapes=True, fullPath=True) or []:
            cmds.setAttr("{}.overrideEnabled".format(shape), True)
            cmds.connectAttr("{}.outputX".format(reverse), "{}.overrideVisibility".format(shape))

    lock_and_hide_keyable_attrs(leg_ctl_hook)
    lock_and_hide_transform_attrs(leg_ctl)

    # -------------------------------------------------------------------------
    # link to main
    # -------------------------------------------------------------------------
    for joint in cmds.ls(root_fk_joint, dag=True, type="joint"):
        cmds.connectAttr("{}.output".format(blend), "{}.drawStyle".format(joint))

    return ctls


def _setup_multi_parents(parent_type, name, constraint_node, attr, parent_names, parent_nodes, clean_grp):
    constraint_matrix = cmds.xform(constraint_node, query=True, worldSpace=True, matrix=True)

    orient_grps = []
    for parent_name, node in zip(parent_names, parent_nodes):
        orient_hook_grp = cmds.createNode("transform", name="{}_{}_orient_HOOK".format(name, parent_name), parent=clean_grp)
        orient_grp = cmds.createNode("transform", name="{}_{}_orient_GRP".format(name, parent_name), parent=orient_hook_grp)
        orient_grps.append(orient_grp)

        matrix = cmds.xform(node, query=True, worldSpace=True, matrix=True)
        cmds.xform(orient_hook_grp, worldSpace=True, matrix=matrix)

        cmds.xform(orient_grp, worldSpace=True, matrix=constraint_matrix)

        if parent_type == "orient":
            cmds.orientConstraint(node, orient_hook_grp)
        elif parent_type == "point":
            cmds.pointConstraint(node, orient_hook_grp)
        elif parent_type == "parent":
            cmds.parentConstraint(node, orient_hook_grp)
        else:
            raise ValueError("parent type {} is not supported".format(parent_type))

    if parent_type == "orient":
        constraint = cmds.orientConstraint(orient_grps + [constraint_node])[0]
    elif parent_type == "point":
        constraint = cmds.pointConstraint(orient_grps + [constraint_node])[0]
    elif parent_type == "parent":
        constraint = cmds.parentConstraint(orient_grps + [constraint_node])[0]

    for i, parent_name in enumerate(parent_names):
        choice_node = cmds.createNode("choice", name="{}_orient_{}_CHC".format(name, parent_name))
        cmds.connectAttr(attr, "{}.selector".format(choice_node))

        for n in range(len(parent_names)):
            value = 0
            if n == i:
                value = 1
            cmds.setAttr("{}.input[{}]".format(choice_node, n), value)

        in_plug = cmds.listConnections(
            "{}.target[{}].targetWeight".format(constraint, i),
            source=True, destination=False, plugs=True)[0]
        cmds.connectAttr("{}.output".format(choice_node), in_plug)


def _create_simple_follow_fk_setup(name, joint, ctl_type, orient_parents, parent_ctl, clean_grp, main_ctl):
    setup_clean_grp = cmds.createNode("transform", name="{}_clean_GRP".format(name), parent=clean_grp)

    setup_root_grp = cmds.createNode("transform", name="{}_root_GRP".format(name), parent=setup_clean_grp)

    cmds.parentConstraint(parent_ctl, setup_root_grp, maintainOffset=True)
    cmds.scaleConstraint(parent_ctl, setup_root_grp, maintainOffset=True)

    ctl = create_ctl(ctl_type, name="{}_CTL".format(name), size=0.015, parent=setup_root_grp, add_hook=True, color=CENTER_COLOR)
    ctl_hook = cmds.listRelatives(ctl, parent=True, fullPath=True)[0]

    matrix = cmds.xform(joint, query=True, worldSpace=True, matrix=True)
    cmds.xform(ctl_hook, worldSpace=True, matrix=matrix)

    # orient setup
    parent_names = []
    parent_nodes = []
    for parent_name, parent_node in orient_parents:
        parent_names.append(parent_name)
        parent_nodes.append(parent_node)

    cmds.addAttr(ctl,
                 longName="EXTRA_________", shortName="EXTRA_________", niceName="EXTRA_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.EXTRA_________".format(ctl), lock=True)
    cmds.addAttr(ctl, longName="orientFollow", keyable=True, attributeType="enum", enumName=":".join(parent_names))

    _setup_multi_parents("orient", name, ctl_hook, "{}.orientFollow".format(ctl), parent_names, parent_nodes, setup_clean_grp)

    # drive joint
    cmds.parentConstraint(ctl, joint)
    cmds.scaleConstraint(ctl, joint)

    lock_and_hide_keyable_attrs(setup_clean_grp)
    lock_and_hide_keyable_attrs(ctl_hook)
    cmds.setAttr("{}.visibility".format(ctl), channelBox=False, keyable=False)

    return ctl


def _create_neck_setup(joint, orient_parents, parent_ctl, clean_grp, main_ctl):
    ctl = _create_simple_follow_fk_setup("NECK", joint, CTL_TYPE.CURVEDARROWX, orient_parents, parent_ctl, clean_grp, main_ctl)

    # adjust controller
    pivot = cmds.xform(joint, query=True, worldSpace=True, translation=True)
    cmds.scale(-1.0, -1.0, -1.0, "{}.cv[*]".format(ctl), pivot=pivot)
    cmds.move(0.0, -0.03, 0.0, "{}.cv[*]".format(ctl), relative=True)

    return ctl


def _create_head_setup(joint, orient_parents, parent_ctl, clean_grp, main_ctl):
    ctl = _create_simple_follow_fk_setup("HEAD", joint, CTL_TYPE.CURVEDARROWCROSS, orient_parents, parent_ctl, clean_grp, main_ctl)

    # adjust controller
    pivot = cmds.xform(joint, query=True, worldSpace=True, translation=True)
    cmds.scale(2, 2, 2, "{}.cv[*]".format(ctl), pivot=pivot)
    cmds.rotate(41.0, 0.0, 0.0, "{}.cv[*]".format(ctl), pivot=pivot)
    cmds.move(0.0, 0.02, 0.01, "{}.cv[*]".format(ctl), relative=True)

    return ctl


def _create_eye_setup(root_joint, color):
    ctls = []

    joints = cmds.ls(root_joint, dag=True, type="joint")
    for joint in joints:
        ctls.append(joint)

        create_on_transform(CTL_TYPE.DIAMOND, joint, size=0.003, color=color)

        lock_and_hide_attrs(joint, ["t", "tx", "ty", "tz"])

        cmds.setAttr("{}.v".format(joint), keyable=False, channelBox=False)
        cmds.setAttr("{}.displayLocalAxis".format(joint), keyable=False, channelBox=False)
        cmds.setAttr("{}.jointOrient".format(joint), keyable=False, channelBox=False)
        cmds.setAttr("{}.jointOrientX".format(joint), keyable=False, channelBox=False)
        cmds.setAttr("{}.jointOrientY".format(joint), keyable=False, channelBox=False)
        cmds.setAttr("{}.jointOrientZ".format(joint), keyable=False, channelBox=False)

    # adjustements
    cmds.move(0.0, 0.01, 0.005, "{}.cv[*]".format(joints[0]), relative=True)

    return ctls


def _create_palp_setup(joint, color):
    create_on_transform(CTL_TYPE.CYLINDERX, joint, size=0.004, color=color)

    lock_and_hide_attrs(joint, ["t", "tx", "ty", "tz"])

    cmds.setAttr("{}.v".format(joint), keyable=False, channelBox=False)
    cmds.setAttr("{}.displayLocalAxis".format(joint), keyable=False, channelBox=False)
    cmds.setAttr("{}.jointOrient".format(joint), keyable=False, channelBox=False)
    cmds.setAttr("{}.jointOrientX".format(joint), keyable=False, channelBox=False)
    cmds.setAttr("{}.jointOrientY".format(joint), keyable=False, channelBox=False)
    cmds.setAttr("{}.jointOrientZ".format(joint), keyable=False, channelBox=False)

    # adjustements
    selection1 = ["{}.cv[4:12]".format(joint),
                  "{}.cv[32:40]".format(joint),
                  "{}.cv[60:68]".format(joint),
                  "{}.cv[88:96]".format(joint),
                  "{}.cv[116:124]".format(joint)]
    bb = cmds.exactWorldBoundingBox(selection1)
    selection1_center = [(bb[0] + bb[3]) / 2.0, (bb[1] + bb[4]) / 2.0, (bb[2] + bb[5]) / 2.0]

    mult = 1.0
    if joint.startswith("R_"):
        mult = -1.0

    cmds.rotate(0.0, -90.0 * mult, 0.2, "{}.cv[*]".format(joint), pivot=selection1_center)
    cmds.scale(1.0, 1.0, 18.0, "{}.cv[*]".format(joint), pivot=selection1_center)

    selection2 = ["{}.cv[2]".format(joint),
                  "{}.cv[14]".format(joint),
                  "{}.cv[30]".format(joint),
                  "{}.cv[42]".format(joint),
                  "{}.cv[58]".format(joint),
                  "{}.cv[70]".format(joint),
                  "{}.cv[86]".format(joint),
                  "{}.cv[98]".format(joint),
                  "{}.cv[114]".format(joint),
                  "{}.cv[126]".format(joint)]

    cmds.scale(2.75, 1.0, 1.0, selection2, pivot=selection1_center)
    cmds.move(-0.005 * mult, 0.0, 0.0, selection2, relative=True, objectSpace=True)

    selection3 = ["{}.cv[1]".format(joint),
                  "{}.cv[15]".format(joint),
                  "{}.cv[29]".format(joint),
                  "{}.cv[43]".format(joint),
                  "{}.cv[57]".format(joint),
                  "{}.cv[71]".format(joint),
                  "{}.cv[85]".format(joint),
                  "{}.cv[99]".format(joint),
                  "{}.cv[113]".format(joint),
                  "{}.cv[127]".format(joint)]

    cmds.scale(2.7, 1.0, 1.0, selection3, pivot=selection1_center)
    cmds.move(0.007 * mult, -0.00025 * mult, 0.0, selection3, relative=True, objectSpace=True)

    selection4 = ["{}.cv[0]".format(joint),
                  "{}.cv[16]".format(joint),
                  "{}.cv[28]".format(joint),
                  "{}.cv[44]".format(joint),
                  "{}.cv[56]".format(joint),
                  "{}.cv[72]".format(joint),
                  "{}.cv[84]".format(joint),
                  "{}.cv[100]".format(joint),
                  "{}.cv[112]".format(joint),
                  "{}.cv[128]".format(joint)]

    cmds.scale(2.3, 1.0, 1.0, selection4, pivot=selection1_center)
    cmds.move(0.007 * mult, -0.001 * mult, 0.0, selection4, relative=True, objectSpace=True)

    return [joint]


def _create_fk_chain_setup(name, root_joint, clean_grp, size, color):
    setup_clean_grp = cmds.createNode("transform", name="{}_clean_GRP".format(name), parent=clean_grp)
    root_grp = cmds.createNode("transform", name="{}_root_GRP".format(name), parent=setup_clean_grp)

    parent = cmds.listRelatives(root_joint, parent=True, fullPath=True)[0]

    cmds.parentConstraint(parent, root_grp)
    cmds.scaleConstraint(parent, root_grp)

    ctls = []
    for joint in cmds.ls(root_joint, dag=True, type="joint"):
        ctl = create_ctl(CTL_TYPE.CIRCLEZ, name=joint.replace("_JNT", "_CTL"), size=size, parent=parent, add_hook=True, color=color)
        ctls.append(ctl)
        ctl_hook = cmds.listRelatives(ctl, parent=True, fullPath=True)[0]

        matrix = cmds.xform(joint, query=True, worldSpace=True, matrix=True)
        cmds.xform(ctl_hook, worldSpace=True, matrix=matrix)

        cmds.parentConstraint(ctl, joint)
        cmds.scaleConstraint(ctl, joint)

        lock_and_hide_keyable_attrs(ctl_hook)

        parent = ctl

    return ctls


def _create_jaw_setup(root_joint, clean_grp):
    ctls = _create_fk_chain_setup("JAW", root_joint, clean_grp, 0.008, CENTER_COLOR)

    # manual adjustments
    selection = "{}.cv[*]".format(ctls[0])
    bb = cmds.exactWorldBoundingBox(selection)
    center = [(bb[0] + bb[3]) / 2.0, (bb[1] + bb[4]) / 2.0, (bb[2] + bb[5]) / 2.0]

    cmds.scale(2.0, 1.8, 1.0, selection, pivot=center)

    return ctls


def _remove_inverse_scale(root_joint):
    children = cmds.ls(cmds.listRelatives(root_joint, children=True, fullPath=True), type="joint")
    for child in children:
        if cmds.isConnected("{}.scale".format(root_joint), "{}.inverseScale".format(child)):
            cmds.disconnectAttr("{}.scale".format(root_joint), "{}.inverseScale".format(child))
        _remove_inverse_scale(child)


def _lock_joints(root_joint):
    if not cmds.listRelatives(root_joint, shapes=True):
        lock_and_hide_keyable_attrs(root_joint)
    cmds.setAttr("{}.radius".format(root_joint), keyable=False, channelBox=False)

    children = cmds.ls(cmds.listRelatives(root_joint, children=True, fullPath=True), type="joint")
    for child in children:
        _lock_joints(child)


# def _link_skin_joints_visibility(root_joint, main_ctl):
#     blend = cmds.listConnections("{}.showSkinJoints".format(main_ctl), source=False, destination=True, type="blendTwoAttr")[0]
#     for joint in cmds.ls(root_joint, dag=True, type="joint"):
#         cmds.connectAttr("{}.output".format(blend), "{}.drawStyle".format(joint))


# def _link_visibility(attr, ctls):
#     for ctl in ctls:
#         for shape in cmds.listRelatives(ctl, shapes=True, fullPath=True) or []:
#             cmds.setAttr("{}.overrideEnabled".format(shape), True)
#
#             plug = cmds.listConnections("{}.overrideVisibility".format(shape), source=True, destination=False, plugs=True)
#
#             if not plug:
#                 cmds.connectAttr(attr, "{}.overrideVisibility".format(shape))
#             else:
#                 mult = cmds.createNode("multDoubleLinear", name="{}_display_MDL".format(ctl))
#                 cmds.connectAttr(plug[0], "{}.input1".format(mult))
#                 cmds.connectAttr(attr, "{}.input2".format(mult))
#                 cmds.connectAttr("{}.output".format(mult), "{}.overrideVisibility".format(shape), force=True)


def rig_tick(single_leg_ik=True):
    rig_grp = cmds.ls("|rig_GRP")[0]
    root_joint = cmds.ls("C_root_JNT")[0]
    neck_joint = cmds.ls("C_neck_01_JNT")[0]
    head_joint = cmds.ls("C_head_01_JNT")[0]
    L_eye_joint = cmds.ls("L_eye_01_JNT")[0]
    R_eye_joint = cmds.ls("R_eye_01_JNT")[0]
    L_palp_joint = cmds.ls("L_palp_01_JNT")[0]
    R_palp_joint = cmds.ls("R_palp_01_JNT")[0]
    jaw_joint = cmds.ls("C_jaw_01_JNT")[0]

    ctls = []

    rig_root_grp, clean_grp = _create_groups(rig_grp)
    main_ctl, main_gimbal_ctl = _create_main_setup(rig_root_grp)
    cog_ctl, cog_gimbal_ctl = _create_cog_setup(main_gimbal_ctl, root_joint)

    ctls.append(main_ctl)
    ctls.append(main_gimbal_ctl)
    ctls.append(cog_ctl)
    ctls.append(cog_gimbal_ctl)

    for side in ["L", "R"]:
        for i in range(4):
            color = LEFT_COLOR
            if side == "R":
                color = RIGHT_COLOR

            name = "{}_leg{}".format(side, i + 1)
            joint_name = "{}_leg{}_{{}}_JNT".format(side, i + 1)

            leg_tip_joint = cmds.ls(joint_name.format("05"))[0]
            if single_leg_ik:
                leg_tip_joint = cmds.ls(joint_name.format("06"))[0]

            leg_root_joint = cmds.ls(joint_name.format("01"))[0]
            if single_leg_ik:
                leg_root_joint = cmds.ls(joint_name.format("02"))[0]

            parent_ctl = cog_gimbal_ctl
            if single_leg_ik:
                parent_ctl = cmds.ls(joint_name.format("01"))[0]

                parent_name = parent_ctl.rpartition("|")[2].replace("_JNT", "_CTL")

                parent_fk_ctl = create_ctl(CTL_TYPE.CIRCLEX, parent_name, size=0.02, parent=cog_gimbal_ctl, add_hook=True, color=color)
                parent_fk_hook = cmds.listRelatives(parent_fk_ctl, parent=True, fullPath=True)[0]

                matrix = cmds.xform(parent_ctl, query=True, worldSpace=True, matrix=True)
                cmds.xform(parent_fk_hook, worldSpace=True, matrix=matrix)

                cmds.parentConstraint(parent_fk_ctl, parent_ctl)
                # cmds.scaleConstraint(parent_fk_ctl, parent_ctl)

                lock_and_hide_keyable_attrs(parent_fk_hook)
                lock_and_hide_attrs(parent_fk_ctl, ["s", "sx", "sy", "sz"])
                cmds.setAttr("{}.visibility".format(parent_fk_ctl), channelBox=False, keyable=False)

            ctls.extend(
                _create_tick_leg_setup(
                    name, clean_grp, main_ctl, main_gimbal_ctl, parent_ctl, cog_gimbal_ctl,
                    leg_root_joint,
                    cmds.ls(joint_name.format("03"))[0],
                    leg_tip_joint,
                    single_leg_ik, color))

    neck_ctl = _create_neck_setup(neck_joint, [("MAIN", main_gimbal_ctl), ("COG", cog_gimbal_ctl)], cog_gimbal_ctl, clean_grp, main_ctl)
    head_ctl = _create_head_setup(head_joint, [("MAIN", main_gimbal_ctl), ("COG", cog_gimbal_ctl), ("NECK", neck_ctl)], neck_ctl, clean_grp, main_ctl)

    ctls.append(neck_ctl)
    ctls.append(head_ctl)

    ctls.extend(_create_eye_setup(L_eye_joint, LEFT_COLOR))
    ctls.extend(_create_eye_setup(R_eye_joint, RIGHT_COLOR))

    ctls.extend(_create_palp_setup(L_palp_joint, LEFT_COLOR))
    ctls.extend(_create_palp_setup(R_palp_joint, RIGHT_COLOR))

    ctls.extend(_create_jaw_setup(jaw_joint, clean_grp))

    _remove_inverse_scale(root_joint)
    _lock_joints(root_joint)
    # _link_skin_joints_visibility(root_joint, main_ctl)

    for ctl in ctls:
        cmds.sets(ctl, add="CTRL_SET")

    # _link_visibility("{}.showControllers".format(main_ctl), ctls)

    # display layer
    cmds.select(ctls)
    display_layer = cmds.createDisplayLayer(noRecurse=True, name='CTRLS')
    cmds.setAttr("{}.hideOnPlayback".format(display_layer), True)
    cmds.setAttr("MODEL.displayType", 2)

    # set ihi to 0
    for node in cmds.ls():
        cmds.setAttr("{}.ihi".format(node), 0)

    # check for bogy group
    bogy_grp = cmds.ls("null1")
    if bogy_grp:
        cmds.delete(bogy_grp)

    cmds.select(main_ctl)
