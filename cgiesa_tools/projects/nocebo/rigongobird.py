# stdlib modules
from __future__ import absolute_import

# third party modules
from maya import cmds
from maya.api import OpenMaya as om2

# tool modules
from cgiesa_tools.maya.rigging.controller import create_ctl
from cgiesa_tools.maya.rigging.controller import create_on_transform
from cgiesa_tools.maya.rigging.controller import CTL_TYPE
from cgiesa_tools.maya.rigging.node import lock_and_hide_keyable_attrs
from cgiesa_tools.maya.rigging.node import lock_and_hide_attrs
from cgiesa_tools.maya.rigging.transform import lock_and_hide_transform_attrs


CENTER_COLOR = 17
LEFT_COLOR = 6
RIGHT_COLOR = 13
GIMBAL_COLOR = 26


def _create_groups(parent):
    rig_setup_grp = cmds.createNode("transform", name="rig_setup_GRP", parent=parent)
    clean_grp = cmds.createNode("transform", name="clean_GRP", parent=parent)

    lock_and_hide_keyable_attrs(rig_setup_grp)
    lock_and_hide_keyable_attrs(clean_grp)

    return rig_setup_grp, clean_grp


def _create_main_setup(parent):
    main_hook_offset_grp = cmds.createNode("transform", name="MAIN_HOOK_OFFSET", parent=parent)
    main_hook_grp = cmds.createNode("transform", name="MAIN_HOOK", parent=main_hook_offset_grp)
    main_ctl = create_ctl(CTL_TYPE.ARROWSQUARE, "MAIN_CTL", size=7.5, parent=main_hook_grp, color=CENTER_COLOR)
    main_gimbal_ctl = create_ctl(CTL_TYPE.CIRCLE, "MAIN_GIMBAL_CTL", size=4.0, parent=main_ctl, color=GIMBAL_COLOR)
    main_gimbal_ctl_shape = cmds.listRelatives(main_gimbal_ctl, shapes=True, fullPath=True)[0]

    lock_and_hide_transform_attrs(main_hook_offset_grp)

    # set the controller shapes
    cmds.scale(1.4, 1.4, 1.4,
               "{}.cv[1]".format(main_gimbal_ctl_shape),
               "{}.cv[3]".format(main_gimbal_ctl_shape),
               "{}.cv[5]".format(main_gimbal_ctl_shape),
               "{}.cv[7]".format(main_gimbal_ctl_shape),
               pivot=(0, 0, 0))

    # attributes
    cmds.addAttr(main_ctl,
                 longName="EXTRA_________", shortName="EXTRA_________", niceName="EXTRA_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.EXTRA_________".format(main_ctl), lock=True)
    cmds.addAttr(main_ctl, longName="globalScale", attributeType="float", keyable=True, defaultValue=1.0, minValue=0.01)

    cmds.connectAttr("{}.globalScale".format(main_ctl), "{}.sx".format(main_ctl))
    cmds.connectAttr("{}.globalScale".format(main_ctl), "{}.sy".format(main_ctl))
    cmds.connectAttr("{}.globalScale".format(main_ctl), "{}.sz".format(main_ctl))

    cmds.addAttr(main_ctl,
                 longName="VISIBILITY_________", shortName="VISIBILITY_________", niceName="VISIBILITY_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.VISIBILITY_________".format(main_ctl), lock=True)
    cmds.addAttr(main_ctl, longName="showGimbal", attributeType="bool", keyable=True, defaultValue=False)
    cmds.addAttr(main_ctl, longName="showControllers", attributeType="bool", keyable=True, defaultValue=True)
    cmds.addAttr(main_ctl, longName="showRig", attributeType="bool", keyable=True, defaultValue=False)
    cmds.addAttr(main_ctl, longName="showSkinJoints", attributeType="bool", keyable=True, defaultValue=False)
    cmds.connectAttr("{}.showGimbal".format(main_ctl), "{}.overrideVisibility".format(main_gimbal_ctl_shape))

    cmds.setAttr("{}.visibility".format(main_ctl), keyable=False)
    cmds.setAttr("{}.visibility".format(main_gimbal_ctl), keyable=False)

    lock_and_hide_attrs(main_ctl, ["s", "sx", "sy", "sz"])
    lock_and_hide_attrs(main_gimbal_ctl, ["s", "sx", "sy", "sz"])

    # add blendTwoAttr for joint draw style
    blend = cmds.createNode("blendTwoAttr", name="MAIN_jointDrawStyle_BTA")
    cmds.setAttr("{}.input[0]".format(blend), 2.0)
    cmds.setAttr("{}.input[1]".format(blend), 0.0)
    cmds.setAttr("{}.input".format(blend), lock=True)
    cmds.connectAttr("{}.showRig".format(main_ctl), "{}.attributesBlender".format(blend))

    blend = cmds.createNode("blendTwoAttr", name="MAIN_skinJointDrawStyle_BTA")
    cmds.setAttr("{}.input[0]".format(blend), 2.0)
    cmds.setAttr("{}.input[1]".format(blend), 0.0)
    cmds.setAttr("{}.input".format(blend), lock=True)
    cmds.connectAttr("{}.showSkinJoints".format(main_ctl), "{}.attributesBlender".format(blend))

    return main_ctl, main_gimbal_ctl


def _create_cog_setup(parent, joint):
    cog_ctl = create_ctl(CTL_TYPE.CIRCLEARROWS, "COG_CTL", size=5.0, parent=parent, add_hook=True, color=CENTER_COLOR)
    cog_gimbal_ctl = create_ctl(CTL_TYPE.CIRCLE, "COG_GIMBAL_CTL", size=2.5, parent=cog_ctl, color=GIMBAL_COLOR)
    cog_hook = cmds.listRelatives(cog_ctl, parent=True, fullPath=True)[0]

    cog_gimbal_ctl_shape = cmds.listRelatives(cog_gimbal_ctl, shapes=True, fullPath=True)[0]

    # position the controller
    translation = cmds.xform(joint, query=True, worldSpace=True, translation=True)
    cmds.xform(cog_hook, worldSpace=True, translation=translation)

    # attributes
    lock_and_hide_transform_attrs(cog_hook)

    cmds.addAttr(cog_ctl,
                 longName="VISIBILITY_________", shortName="VISIBILITY_________", niceName="VISIBILITY_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.VISIBILITY_________".format(cog_ctl), lock=True)
    cmds.addAttr(cog_ctl, longName="showGimbal", attributeType="bool", keyable=True, defaultValue=False)
    cmds.connectAttr("{}.showGimbal".format(cog_ctl), "{}.overrideVisibility".format(cog_gimbal_ctl_shape))

    cmds.setAttr("{}.visibility".format(cog_ctl), keyable=False)
    cmds.setAttr("{}.visibility".format(cog_gimbal_ctl), keyable=False)

    lock_and_hide_attrs(cog_ctl, ["s", "sx", "sy", "sz"])
    lock_and_hide_attrs(cog_gimbal_ctl, ["s", "sx", "sy", "sz"])

    # link to joint
    # cmds.parentConstraint(cog_gimbal_ctl, joint)
    # cmds.scaleConstraint(cog_gimbal_ctl, joint)

    return cog_ctl, cog_gimbal_ctl


def _setup_multi_parents(parent_type, name, constraint_node, attr, parent_names, parent_nodes, clean_grp):
    constraint_matrix = cmds.xform(constraint_node, query=True, worldSpace=True, matrix=True)

    orient_grps = []
    for parent_name, node in zip(parent_names, parent_nodes):
        orient_hook_grp = cmds.createNode("transform", name="{}_{}_orient_HOOK".format(name, parent_name), parent=clean_grp)
        orient_grp = cmds.createNode("transform", name="{}_{}_orient_GRP".format(name, parent_name), parent=orient_hook_grp)
        orient_grps.append(orient_grp)

        matrix = cmds.xform(node, query=True, worldSpace=True, matrix=True)
        cmds.xform(orient_hook_grp, worldSpace=True, matrix=matrix)

        cmds.xform(orient_grp, worldSpace=True, matrix=constraint_matrix)

        if parent_type == "orient":
            cmds.orientConstraint(node, orient_hook_grp)
        elif parent_type == "point":
            cmds.pointConstraint(node, orient_hook_grp)
        elif parent_type == "parent":
            cmds.parentConstraint(node, orient_hook_grp)
        else:
            raise ValueError("parent type {} is not supported".format(parent_type))

    if parent_type == "orient":
        constraint = cmds.orientConstraint(orient_grps + [constraint_node])[0]
    elif parent_type == "point":
        constraint = cmds.pointConstraint(orient_grps + [constraint_node])[0]
    elif parent_type == "parent":
        constraint = cmds.parentConstraint(orient_grps + [constraint_node])[0]

    for i, parent_name in enumerate(parent_names):
        choice_node = cmds.createNode("choice", name="{}_orient_{}_CHC".format(name, parent_name))
        cmds.connectAttr(attr, "{}.selector".format(choice_node))

        for n in range(len(parent_names)):
            value = 0
            if n == i:
                value = 1
            cmds.setAttr("{}.input[{}]".format(choice_node, n), value)

        in_plug = cmds.listConnections(
            "{}.target[{}].targetWeight".format(constraint, i),
            source=True, destination=False, plugs=True)[0]
        cmds.connectAttr("{}.output".format(choice_node), in_plug)


def _create_simple_follow_fk_setup(name, joint, ctl_type, orient_parents, parent_ctl, clean_grp, main_ctl):
    setup_clean_grp = cmds.createNode("transform", name="{}_clean_GRP".format(name), parent=clean_grp)

    setup_root_grp = cmds.createNode("transform", name="{}_root_GRP".format(name), parent=setup_clean_grp)

    cmds.parentConstraint(parent_ctl, setup_root_grp, maintainOffset=True)
    cmds.scaleConstraint(parent_ctl, setup_root_grp, maintainOffset=True)

    ctl = create_ctl(ctl_type, name="{}_CTL".format(name), size=0.4, parent=setup_root_grp, add_hook=True, color=CENTER_COLOR)
    ctl_hook = cmds.listRelatives(ctl, parent=True, fullPath=True)[0]

    matrix = cmds.xform(joint, query=True, worldSpace=True, matrix=True)
    cmds.xform(ctl_hook, worldSpace=True, matrix=matrix)

    # orient setup
    parent_names = []
    parent_nodes = []
    for i, (k, v) in enumerate(orient_parents):
        parent_names.append(k)
        parent_nodes.append(v)

    cmds.addAttr(ctl,
                 longName="EXTRA_________", shortName="EXTRA_________", niceName="EXTRA_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.EXTRA_________".format(ctl), lock=True)
    cmds.addAttr(ctl, longName="orientFollow", keyable=True, attributeType="enum", enumName=":".join(parent_names))

    orient_grps = []
    for parent_name, node in zip(parent_names, parent_nodes):
        orient_hook_grp = cmds.createNode("transform", name="{}_{}_orient_HOOK".format(name, parent_name), parent=setup_clean_grp)
        orient_grp = cmds.createNode("transform", name="{}_{}_orient_GRP".format(name, parent_name), parent=orient_hook_grp)
        orient_grps.append(orient_grp)

        node_matrix = cmds.xform(node, query=True, worldSpace=True, matrix=True)
        cmds.xform(orient_hook_grp, worldSpace=True, matrix=node_matrix)

        cmds.xform(orient_grp, worldSpace=True, matrix=matrix)
        cmds.orientConstraint(node, orient_hook_grp)

        lock_and_hide_keyable_attrs(orient_hook_grp)
        lock_and_hide_keyable_attrs(orient_grp)

    constraint = cmds.orientConstraint(orient_grps + [ctl_hook])[0]

    for i, parent_name in enumerate(parent_names):
        choice_node = cmds.createNode("choice", name="{}_orient_{}_CHC".format(name, parent_name))
        cmds.connectAttr("{}.orientFollow".format(ctl), "{}.selector".format(choice_node))

        for n in range(len(parent_names)):
            value = 0
            if n == i:
                value = 1
            cmds.setAttr("{}.input[{}]".format(choice_node, n), value)

        in_plug = cmds.listConnections("{}.target[{}].targetWeight".format(constraint, i), source=True, destination=False, plugs=True)[0]
        cmds.connectAttr("{}.output".format(choice_node), in_plug)

    # drive joint
    cmds.parentConstraint(ctl, joint)
    cmds.scaleConstraint(ctl, joint)

    lock_and_hide_keyable_attrs(setup_clean_grp)
    lock_and_hide_keyable_attrs(setup_root_grp)
    lock_and_hide_keyable_attrs(ctl_hook)
    cmds.setAttr("{}.v".format(ctl))

    return ctl


def _create_neck_setup(first_joint, last_joint, parent_ctl, orient_parents, clean_grp, main_ctl):
    setup_clean_grp = cmds.createNode("transform", name="{}_clean_GRP".format("NECK"), parent=clean_grp)
    root_grp = cmds.createNode("transform", name="{}_root_GRP".format("NECK"), parent=setup_clean_grp)

    parent = root_grp

    cmds.pointConstraint(parent_ctl, root_grp)
    cmds.scaleConstraint(parent_ctl, root_grp)

    # get all joints
    all_joints = cmds.ls(first_joint, dag=True, type="joint")
    joints = []
    for joint in all_joints:
        joints.append(joint)
        if cmds.ls(joint, long=True) == cmds.ls(last_joint, long=True):
            break

    # revert order
    joints.reverse()

    ctls = []
    for joint in joints:
        ctl = create_ctl(CTL_TYPE.CIRCLEX, name=joint.replace("_JNT", "_CTL"), size=1.0, parent=parent, add_hook=True, color=CENTER_COLOR)
        ctls.append(ctl)
        ctl_hook = cmds.listRelatives(ctl, parent=True, fullPath=True)[0]

        matrix = cmds.xform(joint, query=True, worldSpace=True, matrix=True)
        cmds.xform(ctl_hook, worldSpace=True, matrix=matrix)

        cmds.parentConstraint(ctl, joint)
        cmds.scaleConstraint(ctl, joint)

        parent = ctl

        lock_and_hide_keyable_attrs(ctl_hook)

    # adjust last controller
    cmds.rotate(0.0, 90.0, 0.0, "{}.cv[*]".format(ctls[-1]), objectCenterPivot=True)

    # follow setup
    ctl = ctls[0]
    ctl_hook = cmds.listRelatives(ctl, parent=True, fullPath=True)[0]

    parent_names = [p[0] for p in orient_parents]
    parent_nodes = [p[1] for p in orient_parents]

    cmds.addAttr(ctl,
                 longName="EXTRA_________", shortName="EXTRA_________", niceName="EXTRA_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.EXTRA_________".format(ctl), lock=True)
    cmds.addAttr(ctl, longName="orientFollow", keyable=True, attributeType="enum", enumName=":".join(parent_names))

    _setup_multi_parents("orient", "NECK", root_grp, "{}.orientFollow".format(ctl), parent_names, parent_nodes, setup_clean_grp)

    cmds.setAttr("{}.orientFollow".format(ctl), 2)

    # clean up
    lock_and_hide_keyable_attrs(setup_clean_grp)
    lock_and_hide_keyable_attrs(root_grp)

    return ctls


def _create_head_setup(joint, upper_head_joint, orient_parents, parent_ctl, clean_grp, main_ctl):
    ctl = _create_simple_follow_fk_setup("HEAD", joint, CTL_TYPE.CURVEDARROWCROSS, orient_parents, parent_ctl, clean_grp, main_ctl)

    # set default follow to COG
    cmds.setAttr("{}.orientFollow".format(ctl), 1)

    # adjust controller
    pivot = cmds.xform(joint, query=True, worldSpace=True, translation=True)
    cmds.scale(2.8, 2.8, 2.8, "{}.cv[*]".format(ctl), pivot=pivot)
    cmds.rotate(0.0, 0.0, 20.0, "{}.cv[*]".format(ctl), pivot=pivot)
    cmds.move(0.0, 0.3, 0.0, "{}.cv[*]".format(ctl), relative=True)

    # upper head controller
    upper_ctl = create_ctl(CTL_TYPE.CURVEDARROWZ, name=upper_head_joint.replace("_JNT", "_CTL"), size=0.35, parent=ctl, add_hook=True, color=CENTER_COLOR)
    upper_ctl_hook = cmds.listRelatives(upper_ctl, parent=True, fullPath=True)[0]

    matrix = cmds.xform(upper_head_joint, query=True, worldSpace=True, matrix=True)
    cmds.xform(upper_ctl_hook, worldSpace=True, matrix=matrix)

    cmds.parentConstraint(upper_ctl, upper_head_joint)
    cmds.scaleConstraint(upper_ctl, upper_head_joint)

    # adjust controller
    cmds.move(0.75, 1.0, 0.0, "{}.cv[*]".format(upper_ctl), relative=True, objectSpace=True)

    lock_and_hide_keyable_attrs(upper_ctl_hook)

    return [ctl, upper_ctl]


def _create_fk_chain_setup(name, root_joint, last_joint, clean_grp, size, color):
    setup_clean_grp = cmds.createNode("transform", name="{}_clean_GRP".format(name), parent=clean_grp)
    root_grp = cmds.createNode("transform", name="{}_root_GRP".format(name), parent=setup_clean_grp)

    parent = cmds.listRelatives(root_joint, parent=True, fullPath=True)[0]

    cmds.parentConstraint(parent, root_grp)
    cmds.scaleConstraint(parent, root_grp)

    ctls = []
    for joint in cmds.ls(root_joint, dag=True, type="joint"):
        ctl = create_ctl(CTL_TYPE.CIRCLEX, name=joint.replace("_JNT", "_CTL"), size=size, parent=parent, add_hook=True, color=color)
        ctls.append(ctl)
        ctl_hook = cmds.listRelatives(ctl, parent=True, fullPath=True)[0]

        matrix = cmds.xform(joint, query=True, worldSpace=True, matrix=True)
        cmds.xform(ctl_hook, worldSpace=True, matrix=matrix)

        cmds.parentConstraint(ctl, joint)
        cmds.scaleConstraint(ctl, joint)

        lock_and_hide_keyable_attrs(ctl_hook)

        parent = ctl

        if cmds.ls(joint, long=True) == cmds.ls(last_joint, long=True):
            break

    lock_and_hide_keyable_attrs(setup_clean_grp)
    lock_and_hide_keyable_attrs(root_grp)

    return ctls


def _create_jaw_setup(root_joint, clean_grp):
    setup_clean_grp = cmds.createNode("transform", name="JAW_clean_GRP", parent=clean_grp)
    root_grp = cmds.createNode("transform", name="JAW_root_GRP", parent=setup_clean_grp)

    parent = cmds.listRelatives(root_joint, parent=True, fullPath=True)[0]

    cmds.parentConstraint(parent, root_grp)
    cmds.scaleConstraint(parent, root_grp)

    ctl = create_ctl(CTL_TYPE.CURVEDARROWZ, name=root_joint.replace("_JNT", "_CTL"), size=0.4, parent=root_grp, add_hook=True, color=CENTER_COLOR)
    ctl_hook = cmds.listRelatives(ctl, parent=True, fullPath=True)[0]

    matrix = cmds.xform(root_joint, query=True, worldSpace=True, matrix=True)
    cmds.xform(ctl_hook, worldSpace=True, matrix=matrix)

    cmds.parentConstraint(ctl, root_joint)
    cmds.scaleConstraint(ctl, root_joint)

    # adjust controller
    pivot = cmds.xform(root_joint, query=True, worldSpace=True, translation=True)
    cmds.rotate(0.0, 0.0, 180.0, "{}.cv[*]".format(ctl), pivot=pivot)
    cmds.move(0.85, -0.5, 0.0, "{}.cv[*]".format(ctl), relative=True, objectSpace=True)

    lock_and_hide_keyable_attrs(setup_clean_grp)
    lock_and_hide_keyable_attrs(root_grp)
    lock_and_hide_keyable_attrs(ctl_hook)

    return [ctl]


def _create_tongue_setup(root_joint, clean_grp):
    ctls = _create_fk_chain_setup("TONGUE", root_joint, None, clean_grp, 0.16, CENTER_COLOR)

    # manual adjustments
    pivot = cmds.xform(ctls[0], query=True, worldSpace=True, translation=True)
    cmds.scale(1.0, 1.0, 1.4, "{}.cv[*]".format(ctls[0]), pivot=pivot)
    pivot = cmds.xform(ctls[1], query=True, worldSpace=True, translation=True)
    cmds.scale(1.0, 0.75, 1.0, "{}.cv[*]".format(ctls[1]), pivot=pivot)
    pivot = cmds.xform(ctls[2], query=True, worldSpace=True, translation=True)
    cmds.scale(0.9, 0.675, 0.9, "{}.cv[*]".format(ctls[2]), pivot=pivot)

    return ctls


def _create_eye_setup(left_eye_joint, right_eye_joint, parents, parent_ctl, clean_grp):
    ctls = []

    parent_names = []
    parent_nodes = []
    for k, v in parents:
        parent_names.append(k)
        parent_nodes.append(v)

    setup_clean_grp = cmds.createNode("transform", name="EYES_clean_GRP", parent=clean_grp)
    root_grp = cmds.createNode("transform", name="EYES_root_GRP", parent=setup_clean_grp)

    cmds.parentConstraint(parent_ctl, root_grp)
    cmds.scaleConstraint(parent_ctl, root_grp)

    # setup eye ctl
    for joint, color in zip([left_eye_joint, right_eye_joint], [LEFT_COLOR, RIGHT_COLOR]):
        mult = 1.0
        if joint.startswith("R_"):
            mult = -1.0

        # aim setup
        aim_grp = cmds.createNode("transform", name=joint.replace("_JNT", "_aim_GRP"), parent=root_grp)
        aim_ctl = create_ctl(CTL_TYPE.DIAMOND, name=joint.replace("_JNT", "_aim_CTL"), size=0.25, parent=root_grp, add_hook=True, color=color)
        aim_ctl_hook = cmds.listRelatives(aim_ctl, parent=True, fullPath=True)[0]
        ctls.append(aim_ctl)

        matrix = cmds.xform(joint, query=True, worldSpace=True, matrix=True)
        cmds.xform(aim_grp, worldSpace=True, matrix=matrix)

        aim_ctl_hook = cmds.parent(aim_ctl_hook, aim_grp)[0]
        cmds.setAttr("{}.tx".format(aim_ctl_hook), 5.0 * mult)
        aim_ctl_hook = cmds.parent(aim_ctl_hook, root_grp)[0]
        aim_ctl_hook_matrix = cmds.xform(aim_ctl_hook, query=True, worldSpace=True, matrix=True)

        # aim parent setup
        cmds.addAttr(aim_ctl,
                     longName="EXTRA_________", shortName="EXTRA_________", niceName="EXTRA_________",
                     keyable=True, attributeType="enum", enumName="-------------------")
        cmds.setAttr("{}.EXTRA_________".format(aim_ctl), lock=True)
        cmds.addAttr(aim_ctl, longName="parent", keyable=True, attributeType="enum", enumName=":".join(parent_names))

        parent_grps = []
        for parent_name, node in zip(parent_names, parent_nodes):
            parent_hook_grp = cmds.createNode("transform", name=joint.replace("_JNT", "_{}_parent_HOOK".format(parent_name)), parent=setup_clean_grp)
            parent_grp = cmds.createNode("transform", name=joint.replace("_JNT", "_{}_parent_GRP".format(parent_name)), parent=parent_hook_grp)
            parent_grps.append(parent_grp)

            node_matrix = cmds.xform(node, query=True, worldSpace=True, matrix=True)
            cmds.xform(parent_hook_grp, worldSpace=True, matrix=node_matrix)

            cmds.xform(parent_grp, worldSpace=True, matrix=aim_ctl_hook_matrix)
            cmds.parentConstraint(node, parent_hook_grp)
            cmds.scaleConstraint(node, parent_hook_grp)

            lock_and_hide_keyable_attrs(parent_hook_grp)
            lock_and_hide_keyable_attrs(parent_grp)

        constraint = cmds.parentConstraint(parent_grps + [aim_ctl_hook])[0]
        scale_constraint = cmds.scaleConstraint(parent_grps + [aim_ctl_hook])[0]

        for i, parent_name in enumerate(parent_names):
            choice_node = cmds.createNode("choice", name=joint.replace("_JNT", "_{}_parent_CHC".format(parent_name)))
            cmds.connectAttr("{}.parent".format(aim_ctl), "{}.selector".format(choice_node))

            for n in range(len(parent_names)):
                value = 0
                if n == i:
                    value = 1
                cmds.setAttr("{}.input[{}]".format(choice_node, n), value)

            in_plug = cmds.listConnections("{}.target[{}].targetWeight".format(constraint, i), source=True, destination=False, plugs=True)[0]
            cmds.connectAttr("{}.output".format(choice_node), in_plug)
            in_plug = cmds.listConnections("{}.target[{}].targetWeight".format(scale_constraint, i), source=True, destination=False, plugs=True)[0]
            cmds.connectAttr("{}.output".format(choice_node), in_plug)

        # find the up vector
        root_grp_matrix = cmds.xform(root_grp, query=True, worldSpace=True, matrix=True)
        root_y_axis = om2.MVector([root_grp_matrix[4], root_grp_matrix[5], root_grp_matrix[6]])

        aim_grp_matrix = om2.MMatrix(cmds.xform(aim_grp, query=True, worldSpace=True, matrix=True))
        aim_inv_matrix = aim_grp_matrix.inverse()
        aim_up_vector = root_y_axis * aim_inv_matrix

        const = cmds.aimConstraint(aim_ctl, aim_grp,
                                   aimVector=(1.0, 0.0, 0.0),
                                   upVector=(aim_up_vector.x, aim_up_vector.y, aim_up_vector.z),
                                   worldUpObject=root_grp,
                                   worldUpType="objectrotation",
                                   worldUpVector=(0.0, 1.0, 0.0))[0]

        # eye ctl
        ctl = create_ctl(CTL_TYPE.CURVEDARROWCROSS, name=joint.replace("_JNT", "_CTL"), size=0.25, parent=aim_grp, add_hook=True, color=color)
        ctl_hook = cmds.listRelatives(ctl, parent=True, fullPath=True)[0]
        ctls.append(ctl)

        matrix = cmds.xform(joint, query=True, worldSpace=True, matrix=True)
        cmds.xform(ctl_hook, worldSpace=True, matrix=matrix)

        cmds.orientConstraint(ctl, joint)

        lock_and_hide_attrs(ctl, ["t", "tx", "ty", "tz", "s", "sx", "sy", "sz"])
        cmds.setAttr("{}.v".format(ctl), keyable=False, channelBox=False)

        lock_and_hide_attrs(aim_ctl, ["r", "rx", "ry", "rz", "s", "sx", "sy", "sz"])
        cmds.setAttr("{}.v".format(aim_ctl), keyable=False, channelBox=False)

        lock_and_hide_keyable_attrs(ctl_hook)
        lock_and_hide_keyable_attrs(aim_ctl_hook)

        # adjust controller
        pivot = cmds.xform(joint, query=True, worldSpace=True, translation=True)
        cmds.rotate(0.0, 0.0, -90.0 * mult, "{}.cv[*]".format(ctl), pivot=pivot, objectSpace=True)
        cmds.move(0.2 * mult, 0.0, 0.0, "{}.cv[*]".format(ctl), relative=True, objectSpace=True)

        # aim attributes
        cmds.addAttr(ctl,
                     longName="EXTRA_________", shortName="EXTRA_________", niceName="EXTRA_________",
                     keyable=True, attributeType="enum", enumName="-------------------")
        cmds.setAttr("{}.EXTRA_________".format(ctl), lock=True)
        cmds.addAttr(ctl, longName="aim", keyable=True, attributeType="float", minValue=0.0, maxValue=1.0, defaultValue=1.0)

        blend = cmds.createNode("pairBlend", name=joint.replace("_JNT", "_aim_PB"))

        cmds.setAttr("{}.inRotateX1".format(blend), cmds.getAttr("{}.rx".format(aim_grp)))
        cmds.setAttr("{}.inRotateY1".format(blend), cmds.getAttr("{}.ry".format(aim_grp)))
        cmds.setAttr("{}.inRotateZ1".format(blend), cmds.getAttr("{}.rz".format(aim_grp)))
        cmds.setAttr("{}.inRotateX1".format(blend), lock=True)
        cmds.setAttr("{}.inRotateY1".format(blend), lock=True)
        cmds.setAttr("{}.inRotateZ1".format(blend), lock=True)

        cmds.connectAttr("{}.constraintRotateX".format(const), "{}.inRotateX2".format(blend))
        cmds.connectAttr("{}.constraintRotateY".format(const), "{}.inRotateY2".format(blend))
        cmds.connectAttr("{}.constraintRotateZ".format(const), "{}.inRotateZ2".format(blend))

        cmds.connectAttr("{}.outRotateX".format(blend), "{}.rx".format(aim_grp), force=True)
        cmds.connectAttr("{}.outRotateY".format(blend), "{}.ry".format(aim_grp), force=True)
        cmds.connectAttr("{}.outRotateZ".format(blend), "{}.rz".format(aim_grp), force=True)

        cmds.connectAttr("{}.aim".format(ctl), "{}.weight".format(blend))
        cmds.setAttr("{}.rotInterpolation".format(blend), 1)

        lock_and_hide_keyable_attrs(aim_grp)

    lock_and_hide_keyable_attrs(setup_clean_grp)
    lock_and_hide_keyable_attrs(root_grp)

    return ctls


def _create_membrane_setup(joint, clean_grp, color):
    joint_name = joint.rpartition("|")[2]
    clean_grp_name = joint_name.replace("_JNT", "_clean_GRP")
    root_grp_name = joint_name.replace("_JNT", "_root_GRP")
    ctl_name = joint_name.replace("_JNT", "_CTL")

    mult = 1.0
    if joint_name.startswith("R_"):
        mult = -1.0

    setup_clean_grp = cmds.createNode("transform", name=clean_grp_name, parent=clean_grp)
    root_grp = cmds.createNode("transform", name=root_grp_name, parent=setup_clean_grp)

    parent = cmds.listRelatives(joint, parent=True, fullPath=True)[0]
    matrix = cmds.xform(joint, query=True, worldSpace=True, matrix=True)
    pivot = (matrix[12], matrix[13], matrix[14])
    cmds.xform(root_grp, worldSpace=True, matrix=matrix)

    cmds.parentConstraint(parent, root_grp, maintainOffset=True)
    cmds.scaleConstraint(parent, root_grp)

    ctl = create_ctl(CTL_TYPE.CURVEDARROWX, ctl_name, size=0.15, parent=root_grp, add_hook=True, color=color)
    hook = cmds.listRelatives(ctl, parent=True, fullPath=True)[0]

    cmds.setAttr("{}.rotateOrder".format(ctl), 5)

    cmds.addAttr(ctl,
                 longName="EXTRA_________", shortName="EXTRA_________", niceName="EXTRA_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.EXTRA_________".format(ctl), lock=True)
    cmds.addAttr(ctl, longName="close", keyable=True, attributeType="float", minValue=0.0, maxValue=10.0, defaultValue=0.0)
    cmds.addAttr(ctl, longName="turn", keyable=True, attributeType="float", minValue=-10.0, maxValue=10.0, defaultValue=0.0)

    mult1 = cmds.createNode("multDoubleLinear")
    cmds.setAttr("{}.input2".format(mult1), 12.5)
    cmds.setAttr("{}.input2".format(mult1), lock=True)
    mult2 = cmds.createNode("multDoubleLinear")
    cmds.setAttr("{}.input2".format(mult2), 4.5)
    cmds.setAttr("{}.input2".format(mult2), lock=True)

    cmds.connectAttr("{}.close".format(ctl), "{}.input1".format(mult1))
    cmds.connectAttr("{}.turn".format(ctl), "{}.input1".format(mult2))
    cmds.connectAttr("{}.output".format(mult1), "{}.rotateY".format(ctl))
    cmds.connectAttr("{}.output".format(mult2), "{}.rotateX".format(ctl))

    cmds.orientConstraint(ctl, joint)

    # adjust ctl shape
    cmds.rotate(0.0, 0.0, -90.0 * mult, "{}.cv[*]".format(ctl), pivot=pivot)
    cmds.rotate(0.0, 45.0, 0.0, "{}.cv[*]".format(ctl), pivot=pivot)
    cmds.move(0.25 * mult, 0.0, 0.1, "{}.cv[*]".format(ctl), relative=True)

    lock_and_hide_keyable_attrs(setup_clean_grp)
    lock_and_hide_keyable_attrs(root_grp)
    lock_and_hide_keyable_attrs(hook)
    lock_and_hide_transform_attrs(ctl, hide_visibility=True)

    return ctl


def _add_eyelid_setup(lower_eyelid_loc, upper_eyelid_loc, eye_ctl, clean_grp, color):
    eyelid_name = "L_eyelids"
    mult = 1.0

    if lower_eyelid_loc.startswith("R_"):
        eyelid_name = "R_eyelids"
        mult = -1.0

    setup_clean_grp = cmds.createNode("transform", name="{}_clean_GRP".format(eyelid_name), parent=clean_grp)
    root_grp = cmds.createNode("transform", name="{}_root_GRP".format(eyelid_name), parent=setup_clean_grp)

    parent = cmds.listRelatives(lower_eyelid_loc, parent=True, fullPath=True)[0]
    matrix = cmds.xform(lower_eyelid_loc, query=True, worldSpace=True, matrix=True)
    cmds.xform(root_grp, worldSpace=True, matrix=matrix)

    cmds.parentConstraint(parent, root_grp, maintainOffset=True)
    cmds.scaleConstraint(parent, root_grp, maintainOffset=True)

    # create groups to control locators
    offset_grp = cmds.createNode("transform", name="{}_offset_GRP".format(eyelid_name), parent=root_grp)
    upper_eyelid_grp = cmds.createNode("transform", name="{}_upper_constraint_GRP".format(eyelid_name), parent=offset_grp)
    lower_eyelid_grp = cmds.createNode("transform", name="{}_lower_constraint_GRP".format(eyelid_name), parent=offset_grp)

    cmds.xform(offset_grp, worldSpace=True, matrix=matrix)

    cmds.orientConstraint(upper_eyelid_grp, upper_eyelid_loc)
    cmds.orientConstraint(lower_eyelid_grp, lower_eyelid_loc)

    # add new attributes
    cmds.addAttr(eye_ctl,
                 longName="EYELIDS_________", shortName="EYELIDS_________", niceName="EYELIDS_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.EYELIDS_________".format(eye_ctl), lock=True)
    cmds.addAttr(eye_ctl, longName="upperBlink", keyable=True, attributeType="float", minValue=-0.2, maxValue=1.0, defaultValue=0.0)
    cmds.addAttr(eye_ctl, longName="lowerBlink", keyable=True, attributeType="float", minValue=-0.2, maxValue=1.0, defaultValue=0.0)
    cmds.addAttr(eye_ctl, longName="offsetBlink", keyable=True, attributeType="float", minValue=-1.0, maxValue=1.0, defaultValue=0.0)

    # create the connections
    offset_mult = cmds.createNode("multDoubleLinear", name="{}_offset_MDL".format(eyelid_name))
    upper_mult = cmds.createNode("multDoubleLinear", name="{}_upper_MDL".format(eyelid_name))
    lower_mult = cmds.createNode("multDoubleLinear", name="{}_lower_MDL".format(eyelid_name))

    cmds.setAttr("{}.input2".format(offset_mult), 60.0 * mult)
    cmds.setAttr("{}.input2".format(offset_mult), lock=True)
    cmds.setAttr("{}.input2".format(upper_mult), -60.0 * mult)
    cmds.setAttr("{}.input2".format(upper_mult), lock=True)
    cmds.setAttr("{}.input2".format(lower_mult), 23.0 * mult)
    cmds.setAttr("{}.input2".format(lower_mult), lock=True)

    cmds.connectAttr("{}.offsetBlink".format(eye_ctl), "{}.input1".format(offset_mult))
    cmds.connectAttr("{}.upperBlink".format(eye_ctl), "{}.input1".format(upper_mult))
    cmds.connectAttr("{}.lowerBlink".format(eye_ctl), "{}.input1".format(lower_mult))

    cmds.connectAttr("{}.output".format(offset_mult), "{}.rotateZ".format(offset_grp))
    cmds.connectAttr("{}.output".format(upper_mult), "{}.rotateZ".format(upper_eyelid_grp))
    cmds.connectAttr("{}.output".format(lower_mult), "{}.rotateZ".format(lower_eyelid_grp))


def _remove_inverse_scale(root_joint):
    children = cmds.ls(cmds.listRelatives(root_joint, children=True, fullPath=True), type="joint")
    cmds.setAttr("{}.segmentScaleCompensate".format(root_joint), False)

    for child in children:
        if cmds.isConnected("{}.scale".format(root_joint), "{}.inverseScale".format(child)):
            cmds.disconnectAttr("{}.scale".format(root_joint), "{}.inverseScale".format(child))
        _remove_inverse_scale(child)


def _add_eyelid_bulging_control(main_ctl):
    cmds.addAttr(main_ctl,
                 longName="PERFORMANCE_________", shortName="PERFORMANCE_________", niceName="PERFORMANCE_________",
                 keyable=True, attributeType="enum", enumName="-------------------")
    cmds.setAttr("{}.PERFORMANCE_________".format(main_ctl), lock=True)
    cmds.addAttr(main_ctl, longName="bulgingEyelid", keyable=True, attributeType="bool", defaultValue=True)

    reverse = cmds.createNode("reverse", name="bulgingEyelid_RVS")
    cmds.connectAttr("{}.bulgingEyelid".format(main_ctl), "{}.inputX".format(reverse))

    nodes = [
        "L_eyelidBulgingHead_WRE",
        "R_eyelidBulgingHead_WRE",
        "L_eyelidBulging_WRE",
        "R_eyelidBulging_WRE"
    ]

    for node in nodes:
        cmds.connectAttr("{}.outputX".format(reverse), "{}.nodeState".format(node))


def _lock_joints(root_joint):
    if not cmds.listRelatives(root_joint, shapes=True):
        lock_and_hide_keyable_attrs(root_joint)
    cmds.setAttr("{}.radius".format(root_joint), keyable=False, channelBox=False)

    children = cmds.ls(cmds.listRelatives(root_joint, children=True, fullPath=True), type="joint")
    for child in children:
        _lock_joints(child)


# def _link_skin_joints_visibility(root_joint, main_ctl):
#     blend = cmds.listConnections("{}.showSkinJoints".format(main_ctl), source=False, destination=True, type="blendTwoAttr")[0]
#     for joint in cmds.ls(root_joint, dag=True, type="joint"):
#         cmds.connectAttr("{}.output".format(blend), "{}.drawStyle".format(joint))


# def _link_visibility(attr, ctls):
#     for ctl in ctls:
#         for shape in cmds.listRelatives(ctl, shapes=True, fullPath=True) or []:
#             cmds.setAttr("{}.overrideEnabled".format(shape), True)
#
#             plug = cmds.listConnections("{}.overrideVisibility".format(shape), source=True, destination=False, plugs=True)
#
#             if not plug:
#                 cmds.connectAttr(attr, "{}.overrideVisibility".format(shape))
#             else:
#                 mult = cmds.createNode("multDoubleLinear", name="{}_display_MDL".format(ctl))
#                 cmds.connectAttr(plug[0], "{}.input1".format(mult))
#                 cmds.connectAttr(attr, "{}.input2".format(mult))
#                 cmds.connectAttr("{}.output".format(mult), "{}.overrideVisibility".format(shape), force=True)


def rig_ongobird():
    rig_grp = cmds.ls("|rig_GRP")[0]
    root_joint = cmds.ls("C_root_JNT")[0]
    neck_joint = cmds.ls("C_neck_01_JNT")[0]
    last_neck_joint = cmds.ls("C_neck_04_JNT")[0]
    head_joint = cmds.ls("C_head_01_JNT")[0]
    upper_head_joint = cmds.ls("C_upperHead_01_JNT")[0]
    L_eye_joint = cmds.ls("L_eye_01_JNT")[0]
    R_eye_joint = cmds.ls("R_eye_01_JNT")[0]
    jaw_joint = cmds.ls("C_jaw_01_JNT")[0]
    tongue_joint = cmds.ls("C_tongue_01_JNT")[0]

    L_membrane_joint = cmds.ls("L_eyeMembrane_JNT")[0]
    R_membrane_joint = cmds.ls("R_eyeMembrane_JNT")[0]
    L_upperEyelid_loc = cmds.ls("L_upperEyelid_LOC")[0]
    L_lowerEyelid_loc = cmds.ls("L_lowerEyelid_LOC")[0]
    R_upperEyelid_loc = cmds.ls("R_upperEyelid_LOC")[0]
    R_lowerEyelid_loc = cmds.ls("R_lowerEyelid_LOC")[0]

    ctls = []

    rig_root_grp, clean_grp = _create_groups(rig_grp)
    main_ctl, main_gimbal_ctl = _create_main_setup(rig_root_grp)
    cog_ctl, cog_gimbal_ctl = _create_cog_setup(main_gimbal_ctl, root_joint)

    ctls.append(main_ctl)
    ctls.append(main_gimbal_ctl)
    ctls.append(cog_ctl)
    ctls.append(cog_gimbal_ctl)

    head_ctls = _create_head_setup(head_joint, upper_head_joint, [("MAIN", main_gimbal_ctl), ("COG", cog_gimbal_ctl)], cog_gimbal_ctl, clean_grp, main_ctl)
    neck_ctls = _create_neck_setup(root_joint, last_neck_joint, head_ctls[0], [("MAIN", main_gimbal_ctl), ("COG", cog_gimbal_ctl), ("HEAD", head_ctls[0])], clean_grp, main_ctl)

    ctls.extend(neck_ctls)
    ctls.extend(head_ctls)

    eyes_ctls = _create_eye_setup(L_eye_joint, R_eye_joint, [("MAIN", main_gimbal_ctl), ("COG", cog_gimbal_ctl), ("HEAD", head_ctls[-1])], head_ctls[-1], clean_grp)
    L_eye_ctl = eyes_ctls[1]
    R_eye_ctl = eyes_ctls[3]
    ctls.extend(eyes_ctls)

    membrane_ctl = _create_membrane_setup(L_membrane_joint, clean_grp, LEFT_COLOR)
    ctls.append(membrane_ctl)
    membrane_ctl = _create_membrane_setup(R_membrane_joint, clean_grp, RIGHT_COLOR)
    ctls.append(membrane_ctl)

    _add_eyelid_setup(L_lowerEyelid_loc, L_upperEyelid_loc, L_eye_ctl, clean_grp, LEFT_COLOR)
    _add_eyelid_setup(R_lowerEyelid_loc, R_upperEyelid_loc, R_eye_ctl, clean_grp, RIGHT_COLOR)

    ctls.extend(_create_jaw_setup(jaw_joint, clean_grp))
    ctls.extend(_create_tongue_setup(tongue_joint, clean_grp))

    _remove_inverse_scale(root_joint)
    _lock_joints(root_joint)
    # _link_skin_joints_visibility(root_joint, main_ctl)

    for ctl in ctls:
        cmds.sets(ctl, add="CTRL_SET")

    # _link_visibility("{}.showControllers".format(main_ctl), ctls)

    _add_eyelid_bulging_control(main_ctl)

    # display layer
    cmds.select(ctls)
    display_layer = cmds.createDisplayLayer(noRecurse=True, name='CTRLS')
    cmds.setAttr("{}.hideOnPlayback".format(display_layer), True)
    cmds.setAttr("MODEL.displayType", 2)

    # set ihi to 0
    for node in cmds.ls():
        cmds.setAttr("{}.ihi".format(node), 0)
    for node in cmds.ls(type="transform"):
        cmds.setAttr("{}.displayLocalAxis".format(node), 0)

    # check for bogy group
    bogy_grp = cmds.ls("null1")
    if bogy_grp:
        cmds.delete(bogy_grp)

    cmds.select(main_ctl)
