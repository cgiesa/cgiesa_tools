# -*- coding: utf-8 -*-

from cgiesa_tools.maya.rigging.rivet import create


def create_rivets_on_boxes():
    '''Used the boxes simulated in Houdini and attaches rivets on each of them.
    You should go to the last frame of the simulation, execute this script and
    bake the translations and rotations of the rivet. You'll get error messages
    for the frames where the boxes do not exist yet. That is normal.
    '''
    edge_str = 'OUT.e[{:d}]'
    for i in range(17):
        edges = [i * 12 + 2, i * 12 + 6]
        edges = [edge_str.format(e) for e in edges]
        create(edges[0], edges[1])
