# -*- coding: utf-8 -*-

import logging

import maya.cmds as cmds

from cgiesa_tools.general.enum import enum
from cgiesa_tools.maya.rigging.rivet import create as create_rivet
from cgiesa_tools.maya.context_managers import isolate_model_views
from cgiesa_tools.maya.decorators import need_plugin

ALEMBIC_MESH_NAME = 'OUT'
PARCEL_RIGGING_FILE = ('/uvfx/Projects/Un_cadeau_du_ciel/Assets/3D/Props/'
                       'Parcel/Parcel_Rigging_Master.mb')
PARCEL_NAMESPACE = 'Parcel_Rigging_Master'
PARCEL_MOD_GRP = PARCEL_NAMESPACE + ':Boxes_Modeling:Props_Boxes_modGeo_GRP'
PARCEL_ROOT_GRP = PARCEL_NAMESPACE + ':Props_Boxes_Rigging_GRP'

SCALE = 100  # This is the scene scale difference between Maya and Houdini
FIRST_FRAME = 993
LAST_FRAME = 1300
TOLERANCE = 1e-6

OFFSET_250 = -115
OFFSET_300 = -196

COLLIDER_DIM = enum(DEPTH=34.7, HEIGHT=20.1, WIDTH=21.6)
COLLIDER_OFFSET = [0.0, 10.0, 0.0]


@need_plugin('SOuP')
def prepare_alembic_for_maya():
    '''
    This concerns the parcels from shots 090_0200, 090_0250 and 090_0300.

    Once you generated the alembic from Houdini, there is a small cleanup to do
    after having imported the alembic file into Maya. Execute this script to
    make sure that everything is handled as it is supposed to be.
    '''
    logging.info('Start parcels alembic clean-up...')

    with isolate_model_views():
        # Set the correct frame range for all three shots.
        cmds.playbackOptions(edit=True, minTime=FIRST_FRAME)
        cmds.playbackOptions(edit=True, maxTime=LAST_FRAME)
        cmds.playbackOptions(edit=True, animationStartTime=FIRST_FRAME)
        cmds.playbackOptions(edit=True, animationEndTime=LAST_FRAME)
        cmds.currentTime(LAST_FRAME, edit=True)

        cmds.setAttr(ALEMBIC_MESH_NAME + '.s',  SCALE, SCALE, SCALE)

        shell_num = cmds.polyEvaluate(ALEMBIC_MESH_NAME, shell=True)

        # Create rivets.
        logging.info(' - Create rivets...')
        edge_str = ALEMBIC_MESH_NAME + '.e[{:d}]'
        rivet_list = []
        for i in range(shell_num):
            edge_nums = [i * 12 + 2, i * 12 + 6]
            edges = [edge_str.format(e) for e in edge_nums]
            rivet_list.append(create_rivet(edges[0], edges[1]))

        # Bake rivets transforms.
        logging.info(' - Bake rivets transform...')
        trs_list = ['t', 'r']
        axises = ['x', 'y', 'z']
        attr_list = [r + '.' + t + a for r in rivet_list for t in trs_list
                     for a in axises]
        cmds.bakeResults(attr_list, time=(FIRST_FRAME, LAST_FRAME),
                         simulation=True)

        # Clean rivet setup.
        logging.info(' - Clean rivet setup...')
        delete_nodes = []
        for rivet in rivet_list:
            aim = cmds.listConnections(rivet, type='aimConstraint')[0]
            posi = cmds.listConnections(rivet, type='pointOnSurfaceInfo')[0]
            loft = cmds.listConnections(posi, type='loft')[0]
            edges = cmds.listConnections(loft, type='polyEdgeToCurve')
            delete_nodes.append(aim)
            delete_nodes.append(posi)
            delete_nodes.append(loft)
            delete_nodes.extend(edges)
        if delete_nodes:
            cmds.delete(delete_nodes)

        rivet_grp = cmds.createNode('transform', name='rivet_grp',
                                    skipSelect=True)
        cmds.parent(rivet_list, rivet_grp)

        # Find first frame and remove everything before.
        logging.info(' - Remove obsolete keys before the simulation and add '
                     'scale...')
        for rivet in rivet_list:
            curves = [cmds.listConnections(rivet + '.' + t + a)[0]
                      for t in trs_list for a in axises]
            first_change_index = LAST_FRAME - FIRST_FRAME
            for crv in curves:
                last_value = cmds.keyframe(
                    crv, query=True, index=(0, 0), valueChange=True)[0]
                for i in range(1, LAST_FRAME - FIRST_FRAME + 1):
                    value = cmds.keyframe(
                        crv, query=True, index=(i, i), valueChange=True)[0]
                    if abs(value - last_value) > TOLERANCE:
                        if i < first_change_index:
                            first_change_index = i
                        break
                    last_value = value

            # We delete all keys until one index before the first change
            # happens. This way, we keep the first key (without change).
            for crv in curves:
                cmds.cutKey(crv, index=(0, first_change_index - 1))

            first_frame = FIRST_FRAME + i
            cmds.setKeyframe(rivet + '.sx', time=first_frame - 1, value=0.0)
            cmds.setKeyframe(rivet + '.sy', time=first_frame - 1, value=0.0)
            cmds.setKeyframe(rivet + '.sz', time=first_frame - 1, value=0.0)
            cmds.setKeyframe(rivet + '.sx', time=first_frame, value=1.0)
            cmds.setKeyframe(rivet + '.sy', time=first_frame, value=1.0)
            cmds.setKeyframe(rivet + '.sz', time=first_frame, value=1.0)

        # Apply euler filter.
        logging.info(' - Apply Euler filter...')
        for rivet in rivet_list:
            curves = []
            for a in axises:
                curve = cmds.listConnections(rivet + '.r' + a)
                if curve:
                    curves.append(curve[0])
            if curves:
                cmds.filterCurve(curves)

        # Load parcel rigging.
        logging.info(' - Setup the instancer...')
        cmds.file(PARCEL_RIGGING_FILE, reference=True, type='mayaBinary',
                  ignoreVersion=True, groupLocator=True,
                  mergeNamespacesOnClash=False,
                  namespace=PARCEL_NAMESPACE, options='v=0;')

        t_to_arr = cmds.createNode('transformsToArrays',
                                   name='parcels_trsToArrays', skipSelect=True)

        attrs_list = [
            ('rotateOrder', 'inRotateOrder'),
            ('worldMatrix[0]', 'inMatrix')]

        for i, rivet in enumerate(rivet_list):
            t_to_arr_plug = t_to_arr + '.inTransforms[{}]'.format(i)
            for attrs in attrs_list:
                cmds.connectAttr(
                    rivet + '.' + attrs[0],
                    t_to_arr_plug + '.' + attrs[1])

        pos_array = cmds.createNode(
            'arrayToDynArrays', name='parcels_pos_array', skipSelect=True)
        rot_array = cmds.createNode(
            'arrayToDynArrays', name='parcels_rot_array', skipSelect=True)
        scale_array = cmds.createNode(
            'arrayToDynArrays', name='parcels_scale_array', skipSelect=True)

        cmds.setAttr(pos_array + '.name', 'position', type='string')
        cmds.setAttr(rot_array + '.name', 'rotation', type='string')
        cmds.setAttr(scale_array + '.name', 'scale', type='string')

        cmds.connectAttr(t_to_arr + '.outPositionPP', pos_array + '.inArray')
        cmds.connectAttr(t_to_arr + '.outRotationPP', rot_array + '.inArray')
        cmds.connectAttr(t_to_arr + '.outScalePP', scale_array + '.inArray')

        cmds.connectAttr(pos_array + '.outDynamicArrays',
                         rot_array + '.inDynamicArrays')
        cmds.connectAttr(rot_array + '.outDynamicArrays',
                         scale_array + '.inDynamicArrays')

        instancer = cmds.createNode(
            'instancer', name='parcels_instancer', skipSelect=True)
        cmds.connectAttr(scale_array + '.outDynamicArrays',
                         instancer + '.inputPoints')
        cmds.connectAttr(PARCEL_MOD_GRP + '.matrix',
                         instancer + '.inputHierarchy[0]')

        cmds.setAttr(PARCEL_MOD_GRP + '.v', False)
        cmds.setAttr(ALEMBIC_MESH_NAME + '.v', False)

        # Final clean up.
        logging.info(' - Final clean-up...')
        main_grp = cmds.createNode('transform', name='main_grp',
                                   skipSelect=True)
        cmds.parent(ALEMBIC_MESH_NAME, rivet_grp, instancer, PARCEL_ROOT_GRP,
                    main_grp)

        # We create cube colliders for the cloth simulation.
        logging.info(' - We add cube colliders for cloth simulation...')
        collider = cmds.polyCube(constructionHistory=False, createUVs=True,
                                 depth=COLLIDER_DIM.DEPTH,
                                 height=COLLIDER_DIM.HEIGHT,
                                 width=COLLIDER_DIM.WIDTH)[0]
        cmds.setAttr(collider + '.translate',
                     COLLIDER_OFFSET[0],
                     COLLIDER_OFFSET[1],
                     COLLIDER_OFFSET[2])
        cmds.xform(collider, rotatePivot=(0, 0, 0), worldSpace=True)
        cmds.makeIdentity(collider, apply=True)
        cmds.delete(cmds.cluster(collider))

        for rivet in rivet_list:
            new_collider = cmds.duplicate(collider)
            cmds.parent(new_collider, rivet, relative=True)

        cmds.delete(collider)
        cmds.setAttr(rivet_grp + '.visibility', False)

    logging.info('Parcels alembic clean-up finished! Wish you a nice day! :)')


def offset_parcels_animation(by):
    '''
    When preparing the Fx scenes for shots 090_0250 and 090_0300, you need to
    offset the time for the parcels. This script handles this.

    Usage:

    from cgiesa_tools.projects.un_cadeau_du_ciel.parcels import \
      offset_parcels_animation, OFFSET_250, OFFSET_300

    # To run for the shot 090_0250.
    offset_parcels_animation(OFFSET_250)

    # To run for the shot 090_0300.
    offset_parcels_animation(OFFSET_300)
    '''
    time_warp = cmds.createNode('timeWarp')
    cmds.setAttr(time_warp + '.origFrames',
                 (2, FIRST_FRAME, LAST_FRAME), type='doubleArray')
    cmds.setAttr(time_warp + '.endFrames',
                 (2, FIRST_FRAME + by, LAST_FRAME + by), type='doubleArray')
    cmds.connectAttr('time1.outTime', time_warp + '.input')

    for each in cmds.ls(
            type=['animCurveTA', 'animCurveTL', 'animCurveTT', 'animCurveTU']):
        # All keyframes from the parcels come with the same namespace. Just to
        # avoid offsets on other keyframes.
        if not each.startswith('Fx_'):
            continue
        if not cmds.isConnected(time_warp + '.output', each + '.input'):
            cmds.connectAttr(time_warp + '.output', each + '.input')
