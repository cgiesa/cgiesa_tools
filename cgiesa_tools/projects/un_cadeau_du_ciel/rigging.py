import maya.cmds as cmds

from cgiesa_tools.maya.rigging.transform import add_hook
from cgiesa_tools.maya.rigging.node import lock_and_hide_all_attrs
from cgiesa_tools.maya.rigging.controller import create_ctl, CTL_TYPE


def create_bend_rig(start_loc, end_loc, geo, name=None):
    '''
    Creates a simple bend rig with three controllers.
    '''
    # Check input.
    start_loc = cmds.ls(start_loc, type='transform')
    end_loc = cmds.ls(end_loc, type='transform')

    if not start_loc or not end_loc:
        cmds.error('Please provide a valid start and end locator for your '
                   'rig.')
    start_loc = start_loc[0]
    end_loc = end_loc[0]

    geo = cmds.ls(geo, dag=True, type='geometry', noIntermediate=True)

    if not geo:
        cmds.error('Please provide a valid geometry.')
    geo = geo[0]

    # Build controllers.
    if not name:
        name = 'bendSetup'
    root = cmds.createNode('transform', name=name + '_root_grp')
    clean = cmds.createNode('transform', name=name + '_clean_grp', parent=root)
    lock_and_hide_all_attrs(root)
    lock_and_hide_all_attrs(clean)

    pos1 = cmds.xform(start_loc, query=True, rotatePivot=True, worldSpace=True)
    pos2 = cmds.xform(end_loc, query=True, rotatePivot=True, worldSpace=True)

    ctl1 = create_ctl(CTL_TYPE.CIRCLE, name + '_topCtl')
    ctl2 = create_ctl(CTL_TYPE.CIRCLE, name + '_midCtl')
    ctl3 = create_ctl(CTL_TYPE.CIRCLE, name + '_bottomCtl')
    ctl1 = cmds.parent(ctl1, root)[0]

    cmds.setAttr(ctl1 + '.translate', pos1[0], pos1[1], pos1[2])
    cmds.setAttr(ctl2 + '.translate',
                 (pos1[0] + pos2[0]) * 0.5,
                 (pos1[1] + pos2[1]) * 0.5,
                 (pos1[2] + pos2[2]) * 0.5,)
    cmds.setAttr(ctl3 + '.translate', pos2[0], pos2[1], pos2[2])

    cmds.delete(
        cmds.aimConstraint(
            ctl3, ctl1, aimVector=(0.0, -1.0, 0.0), upVector=(0.0, 0.0, 1.0),
            worldUpVector=(0.0, 0.0, 1.0)))

    rotation = cmds.getAttr(ctl1 + '.rotate')[0]
    cmds.setAttr(ctl2 + '.rotate', rotation[0], rotation[1], rotation[2])
    cmds.setAttr(ctl3 + '.rotate', rotation[0], rotation[1], rotation[2])

    ctl2 = cmds.parent(ctl2, ctl1)[0]
    ctl3 = cmds.parent(ctl3, ctl1)[0]

    add_hook(ctl1, lock_attrs_on_hook=True)
    add_hook(ctl2, lock_attrs_on_hook=True)
    add_hook(ctl3, lock_attrs_on_hook=True)

    # Create locators that will drive the curve.
    pos_list = []
    num_pnts = 5
    for i in range(num_pnts + 1):
        # We skip the second point. Not needed.
        if i == 1:
            continue
        blend = float(i) / float(num_pnts)
        pos_list.append((
            (pos1[0] * (1 - blend)) + (pos2[0] * blend),
            (pos1[1] * (1 - blend)) + (pos2[1] * blend),
            (pos1[2] * (1 - blend)) + (pos2[2] * blend)))

    locs = []
    for i, pos in enumerate(pos_list):
        loc = cmds.createNode('locator')
        cmds.setAttr(loc + '.visibility', False)
        loc_trs = cmds.listRelatives(loc, parent=True)[0]
        cmds.setAttr(loc_trs + '.translate', pos[0], pos[1], pos[2])
        loc_trs = cmds.rename(loc_trs, name + '_crvPos{}_loc'.format(i))
        locs.append(loc_trs)

    locs[0] = cmds.parent(locs[0], ctl1)[0]
    locs[1] = cmds.parent(locs[1], ctl2)[0]
    locs[2] = cmds.parent(locs[2], ctl2)[0]
    locs[3] = cmds.parent(locs[3], ctl3)[0]
    locs[4] = cmds.parent(locs[4], ctl3)[0]
    for loc in locs:
        lock_and_hide_all_attrs(loc)
        cmds.setAttr(loc + '.ihi', 0)

    # Create curve.
    curve = cmds.curve(degree=1, point=pos_list)
    curve = cmds.rename(curve, name + '_linearCurve')
    curve_shape = cmds.listRelatives(curve, shapes=True)[0]
    cmds.setAttr(curve_shape + '.intermediateObject', True)
    cmds.setAttr(curve_shape + '.ihi', 0)

    for i, loc in enumerate(locs):
        loc = cmds.listRelatives(loc, shapes=True)[0]
        cmds.connectAttr(loc + '.worldPosition',
                         curve_shape + '.controlPoints[{}]'.format(i))

    rbld = cmds.createNode('rebuildCurve', name=name + '_rebuildCurve')
    wire_crv = cmds.createNode('nurbsCurve')
    base_crv = cmds.createNode('nurbsCurve')

    cmds.setAttr(rbld + '.ihi', 0)
    cmds.setAttr(wire_crv + '.ihi', 0)
    cmds.setAttr(base_crv + '.ihi', 0)
    cmds.setAttr(wire_crv + '.intermediateObject', True)
    cmds.setAttr(base_crv + '.intermediateObject', True)

    wire_crv_trs = cmds.listRelatives(wire_crv, parent=True)[0]
    base_crv_trs = cmds.listRelatives(base_crv, parent=True)[0]

    cmds.connectAttr(curve_shape + '.worldSpace', rbld + '.inputCurve')
    cmds.setAttr(rbld + '.spans', 3)
    cmds.connectAttr(rbld + '.outputCurve', wire_crv + '.create')
    cmds.connectAttr(rbld + '.outputCurve', base_crv + '.create')
    cmds.getAttr(base_crv + '.worldSpace', type=True)
    cmds.disconnectAttr(rbld + '.outputCurve', base_crv + '.create')

    curve = cmds.parent(curve, clean)[0]
    wire_crv_trs = cmds.parent(wire_crv_trs, clean)[0]
    base_crv_trs = cmds.parent(base_crv_trs, ctl1)[0]

    lock_and_hide_all_attrs(curve)
    lock_and_hide_all_attrs(wire_crv_trs)
    lock_and_hide_all_attrs(base_crv_trs)

    # Deform the geometry.
    wire = cmds.deformer(geo, type='wire', before=True, name=name + '_wire')[0]
    cmds.setAttr(wire + '.ihi', 0)

    cmds.connectAttr(wire_crv + '.worldSpace', wire + '.deformedWire[0]')
    cmds.connectAttr(base_crv + '.worldSpace', wire + '.baseWire[0]')
    cmds.setAttr(wire + '.freezeGeometry', True)
    # 20 is an arbitrary value but should be big enough in most cases. If the
    # value is too small, the wire effect might be wrong.
    cmds.setAttr(wire + '.dropoffDistance[0]', 20)

    wire_crv_trs = cmds.rename(wire_crv_trs, name + '_wireDeformCurve')
    base_crv_trs = cmds.rename(base_crv_trs, name + '_wireBaseCurve')
