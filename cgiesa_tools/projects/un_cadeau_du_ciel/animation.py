# -*- coding: utf-8 -*-

import logging

import maya.cmds as cmds
import pymel.core as pm


def switch_anim_sheep_to_fur():
    '''
    This will look for all sheep rigging references and switch them to fur.
    Several keys will be removed since they'll get reanimated after animation.
    All display layers get removed.
    '''
    logging.info('Switch all sheep rigs to fur assets. Starting procedure...')

    RIGGING_FILENAME = 'Sheep_Rigging_Master.mb'
    FUR_FILENAME = 'Sheep_Fur_Master.mb'

    ref_nodes = []

    for node in pm.ls(type='reference'):
        # Ignore shared reference nodes and ref nodes that are in reference
        # themselves. Those are second level of reference.
        if node.name().count('sharedReferenceNode'):
            continue
        if cmds.referenceQuery(node.name(), isNodeReferenced=True):
            continue
        ref_nodes.append(node)

    for node in ref_nodes:
        filename = cmds.referenceQuery(node.name(), filename=True,
                                       withoutCopyNumber=True)
        if not filename.count(RIGGING_FILENAME):
            continue

        # Replace the rigging files with the fur files.
        fur_filename = filename.replace(RIGGING_FILENAME, FUR_FILENAME)
        logging.info('Switch {} to {}...'.format(node.name(), fur_filename))
        cmds.file(fur_filename, loadReference=node.name(), type='mayaBinary',
                  options='v=0')

        # Find the attributes where are keyframes should be removed.
        attr_list = ['InstanceLength', 'InstanceThickness', 'DisplayHairs',
                     'DisplayCombDir']
        namespace = cmds.referenceQuery(node.name(), namespace=True)
        to_delete = []
        for attr in attr_list:
            plugs = cmds.ls(namespace + ':*.' + attr)
            for plug in plugs:
                animCurve = cmds.listConnections(
                    plug, source=True, destination=False, type='animCurve')
                if animCurve:
                    to_delete.append(animCurve)

    if to_delete:
        logging.info('Delete anim curves: {}'.format(to_delete))
        cmds.delete(to_delete)

    # Remove all display layers.
    display_layers = cmds.ls(type='displayLayer')
    for layer in display_layers:
        if cmds.referenceQuery(layer, isNodeReferenced=True):
            continue
        try:
            cmds.delete(layer)
            logging.info('Delete display layer {}'.format(layer))
        except RuntimeError:
            pass

    logging.info('Finished switch from rig to fur asset.')
