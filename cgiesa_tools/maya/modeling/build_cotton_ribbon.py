from math import sqrt, pow

import maya.cmds as cmds

from cgiesa_tools.general.enum import enum

DIRECTION = enum('U', 'V')
UV_TEST_OFFSET = 0.001


def _get_span_points(surface, along_dir, turn_dir, width, direction):
    if direction == DIRECTION.U:
        uv_vals = (along_dir, turn_dir)
        uv_test_fwd_vals = (min(along_dir + UV_TEST_OFFSET, 1.0), turn_dir)
        uv_test_bck_vals = (max(along_dir - UV_TEST_OFFSET, 0.0), turn_dir)
    elif direction == DIRECTION.V:
        uv_vals = (turn_dir, along_dir)
        uv_test_fwd_vals = (turn_dir, min(along_dir + UV_TEST_OFFSET, 1.0))
        uv_test_bck_vals = (turn_dir, max(along_dir - UV_TEST_OFFSET, 0.0))

    point = cmds.pointOnSurface(
        surface, u=uv_vals[0], v=uv_vals[1], position=True,
        turnOnPercentage=True)
    tst_fwd_point = cmds.pointOnSurface(
        surface, u=uv_test_fwd_vals[0], v=uv_test_fwd_vals[1], position=True,
        turnOnPercentage=True)
    tst_bck_point = cmds.pointOnSurface(
        surface, u=uv_test_bck_vals[0], v=uv_test_bck_vals[1], position=True,
        turnOnPercentage=True)

    fwd_dist = sqrt(
        pow(tst_fwd_point[0] - point[0], 2) +
        pow(tst_fwd_point[1] - point[1], 2) +
        pow(tst_fwd_point[2] - point[2], 2))
    bck_dist = sqrt(
        pow(tst_bck_point[0] - point[0], 2) +
        pow(tst_bck_point[1] - point[1], 2) +
        pow(tst_bck_point[2] - point[2], 2))

    if fwd_dist > 0:
        fwd_offset = width * UV_TEST_OFFSET / fwd_dist
    else:
        fwd_offset = 0
    if bck_dist > 0:
        bck_offset = width * UV_TEST_OFFSET / bck_dist
    else:
        bck_offset = 0

    if direction == DIRECTION.U:
        uv_fwd_vals = (min(along_dir + fwd_offset, 1.0), turn_dir)
        uv_bck_vals = (max(along_dir - bck_offset, 0.0), turn_dir)
    elif direction == DIRECTION.V:
        uv_fwd_vals = (turn_dir, min(along_dir + fwd_offset, 1.0))
        uv_bck_vals = (turn_dir, max(along_dir - bck_offset, 0.0))

    if fwd_offset != 0.0:
        fwd_point = cmds.pointOnSurface(
            surface, u=uv_fwd_vals[0], v=uv_fwd_vals[1], position=True,
            turnOnPercentage=True)
    else:
        fwd_point = point

    if bck_offset != 0.0:
        bck_point = cmds.pointOnSurface(
            surface, u=uv_bck_vals[0], v=uv_bck_vals[1], position=True,
            turnOnPercentage=True)
    else:
        bck_point = point

    return [bck_point, point, fwd_point]


def build_cotton_ribbon(inner_surface, outer_surface, num_of_turns,
                        density_per_turn, width, forth_and_back=1,
                        direction=DIRECTION.U, reverse=False):
    '''
    Given a inner_surface and outer_surface, this script will generate a ribbon
    between those two that will go along the volume from the inner to the outer
    side.

    Both surfaces should be laid out the same way in terms of u and v direction
    to make sure that the ribbon goes the right way.

    num_of_turns will define the number of turns that the ribbon will make from
    the start to the end of the volume.

    forth_and_back will multiply those turns back and forth. By default, this is
    set to 1, so just one go from the bottom to the top.

    TODO:
        - add random
        - allow the inverse from the outside to the inside
    '''
    inner_surface = cmds.ls(inner_surface, type='nurbsSurface', dag=True,
                            noIntermediate=True)
    if not inner_surface:
        cmds.error('The given inner_surface is no valid nurbsSurface.')
    inner_surface = inner_surface[0]

    outer_surface = cmds.ls(outer_surface, type='nurbsSurface', dag=True,
                            noIntermediate=True)
    if not outer_surface:
        cmds.error('The given outer_surface is no valid nurbsSurface.')
    outer_surface = outer_surface[0]

    point1_list = []
    point2_list = []
    point3_list = []

    current_pnt_count = 1
    total_points = num_of_turns * density_per_turn * forth_and_back
    point_per_forth_and_back = num_of_turns * density_per_turn - 1
    width *= 0.5
    
    for i in range(forth_and_back):
        forth_and_back_count = 0
        for j in range(num_of_turns):
            for k in range(density_per_turn):
                turn_dir = float(k) / float(density_per_turn)
                along_dir = (float(forth_and_back_count) /
                             float(point_per_forth_and_back))
                forth_and_back_count += 1

                if i % 2:
                    along_dir = 1 - along_dir
                if reverse:
                    along_dir = 1 - along_dir

                blend = float(current_pnt_count) / float(total_points)
                inv_blend = 1 - blend
                current_pnt_count += 1

                inner_bck_pnt, inner_pnt, inner_fwd_pnt = _get_span_points(
                    inner_surface, along_dir, turn_dir, width, direction)
                outer_bck_pnt, outer_pnt, outer_fwd_pnt = _get_span_points(
                    outer_surface, along_dir, turn_dir, width, direction)
                
                bck_pnt = [inner_bck_pnt[0] * inv_blend + outer_bck_pnt[0] * blend,
                           inner_bck_pnt[1] * inv_blend + outer_bck_pnt[1] * blend,
                           inner_bck_pnt[2] * inv_blend + outer_bck_pnt[2] * blend]
                pnt = [inner_pnt[0] * inv_blend + outer_pnt[0] * blend,
                       inner_pnt[1] * inv_blend + outer_pnt[1] * blend,
                       inner_pnt[2] * inv_blend + outer_pnt[2] * blend]
                fwd_pnt = [inner_fwd_pnt[0] * inv_blend + outer_fwd_pnt[0] * blend,
                           inner_fwd_pnt[1] * inv_blend + outer_fwd_pnt[1] * blend,
                           inner_fwd_pnt[2] * inv_blend + outer_fwd_pnt[2] * blend]
                
                point1_list.append(bck_pnt)
                point2_list.append(pnt)
                point3_list.append(fwd_pnt)

    # Create curve
    degree = 3
    knot_vector = range(total_points + degree - 1)
    curves = []
    curves.append(cmds.curve(point=point1_list, knot=knot_vector, degree=degree))
    curves.append(cmds.curve(point=point2_list, knot=knot_vector, degree=degree))
    curves.append(cmds.curve(point=point3_list, knot=knot_vector, degree=degree))
    
    cmds.loft(curves, constructionHistory=False)
    cmds.delete(curves)

