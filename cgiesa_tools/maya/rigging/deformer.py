# -*- coding: utf-8 -*-

from math import sqrt

import maya.cmds as cmds

from node import lock_and_hide_keyable_attrs
from controller import create_on_transform, CTL_TYPE
from exceptions import DeformerConfigurationError
from cgiesa_tools.maya.exceptions import SelectionError


def create_offsettable_cluster(transform, cluster):
    '''
    This will take the given transform and link it to the cluster that way so
    that it can be used as a cluster handle. The advantage is that the
    transform works relatively to its parent which allows to have an
    offsettable cluster. Ideally, you make sure that the transform's local
    transformation is zero to avoid deformation effects when calling this
    method.
    '''
    cmds.connectAttr(transform + '.worldMatrix[0]',
                     cluster + '.matrix', force=True)
    cmds.connectAttr(transform + '.parentInverseMatrix[0]',
                     cluster + '.bindPreMatrix', force=True)
    cmds.connectAttr(transform + '.parentMatrix[0]',
                     cluster + '.clusterXforms.preMatrix', force=True)
    cmds.connectAttr(transform + '.matrix',
                     cluster + '.clusterXforms.weightedMatrix', force=True)

    # We remove any existing cluster handle.
    plug = cmds.listConnections(cluster + '.clusterXforms', plugs=True)
    if plug:
        cmds.disconnectAttr(plug[0], cluster + '.clusterXforms')
        cmds.delete(cmds.listRelatives(plug[0].split('.')[0], parent=True))


def create_offsettable_cluster_from_selection():
    '''
    A little wrapper for the create_offsettable_cluster method which takes
    the current selection.

    First select a transform, then the cluster handle to make this work.
    '''
    sel = cmds.ls(selection=True)
    trs = cmds.ls(sel[0], type='transform')
    if not trs:
        raise SelectionError('Please select first a transform.')
    trs = trs[0]

    cls = cmds.ls(sel[1], type='cluster')
    if not cls:
        cls = cmds.listConnections(sel[1], type='cluster')
        if not cls:
            raise SelectionError('Please select a cluster or cluster handle as '
                                 'second object.')
    cls = cls[0]

    create_offsettable_cluster(trs, cls)


def create_offsettable_softmod(geometry):
    '''
    This method will add a softmod deformer to the given geometry and replace
    it with an offsettable softmod setup.
    '''
    pass


def create_moving_deformer(geos, name=None):
    '''
    Adds a moving deformer to the given geometries. The moving deformer is a
    mixture of cluster and lattice that allows to have a offsettable sliding
    cluster effect. It can also easily be attached to the deformed mesh.
    '''
    RESOLUTION = 12
    MAX_DIST = (1.0 / (RESOLUTION - 1)) * ((RESOLUTION - 1) * 0.5 - 1.0)
    CTL_COLOR = 25
    OFFSET_COLOR = 4

    geos = cmds.ls(geos, dag=True, type='geometry', noIntermediate=True)
    if not geos:
        cmds.error('Provided geometry/ies is/are not valid.')

    if name is None:
        name = 'movingDeformer'

    root = cmds.createNode('transform', name=name + '_root_grp')
    offset_ctl = cmds.createNode('transform', name=name + '_offset_ctl',
                                 parent=root)
    ctl = cmds.createNode('transform', name=name + '_ctl', parent=offset_ctl)

    create_on_transform(CTL_TYPE.BALL, offset_ctl, size=0.5)
    create_on_transform(CTL_TYPE.ARROWBALL, ctl, size=0.45)

    for shape in cmds.listRelatives(ctl, shapes=True):
        cmds.setAttr(shape + '.overrideEnabled', True)
        cmds.setAttr(shape + '.overrideColor', CTL_COLOR)
    for shape in cmds.listRelatives(offset_ctl, shapes=True):
        cmds.setAttr(shape + '.overrideEnabled', True)
        cmds.setAttr(shape + '.overrideColor', OFFSET_COLOR)

    ffd, lattice, lattice_base = cmds.lattice(
        divisions=(RESOLUTION, RESOLUTION, RESOLUTION),
        ignoreSelected=True, name=name + '_lattice',
        position=(0, 0, 0), scale=(1, 1, 1))

    cmds.lattice(ffd, edit=True, geometry=geos, before=True)
    lattice = cmds.parent(lattice, offset_ctl)[0]
    lattice_base = cmds.parent(lattice_base, offset_ctl)[0]

    cmds.setAttr(ffd + '.local', True)
    cmds.setAttr(ffd + '.localInfluenceS', RESOLUTION)
    cmds.setAttr(ffd + '.localInfluenceT', RESOLUTION)
    cmds.setAttr(ffd + '.localInfluenceU', RESOLUTION)

    softmod, softmod_handle = cmds.softMod(ignoreSelected=True,
                                           name=name + '_softmod')

    cmds.softMod(softmod, edit=True, geometry=lattice, before=True)

    # Put weight to 0 for points that are close to the border of the lattice.
    # REMARK: We MUST NOT remove those points from the deformation set,
    # otherwise, the soft mod looses the falloff feature (bug Maya?).
    object_set = get_object_set_from_deformer(softmod)
    comp_list = cmds.ls(cmds.sets(object_set, query=True), flatten=True)
    zero_comps = []

    for comp in comp_list:
        pos = cmds.pointPosition(comp, world=True)
        if sqrt(pow(pos[0], 2) + pow(pos[1], 2) + pow(pos[2], 2)) > MAX_DIST:
            zero_comps.append(comp)
    cmds.select(zero_comps, replace=True)
    cmds.percent(softmod, value=0.0)

    # Setup the attributes.
    cmds.addAttr(ctl, longName='falloff', keyable=True, defaultValue=1.0,
                 min=0.0)
    cmds.connectAttr(ctl + '.falloff', softmod + '.falloffRadius')

    cmds.addAttr(ctl, longName='softnessLevel', keyable=True,
                 defaultValue=int(RESOLUTION * 0.5),
                 min=0.0, attributeType='short')
    mult = cmds.createNode('multDoubleLinear')
    cmds.setAttr(mult + '.i2', 2)
    cmds.connectAttr(ctl + '.softnessLevel', mult + '.i1')
    cmds.connectAttr(mult + '.output', ffd + '.localInfluenceS')
    cmds.connectAttr(mult + '.output', ffd + '.localInfluenceT')
    cmds.connectAttr(mult + '.output', ffd + '.localInfluenceU')

    # TODO: This should be in a separate method to be accessible from somewhere
    # else as well.
    cmds.connectAttr(
        ctl + '.worldMatrix[0]', softmod + '.matrix', force=True)
    cmds.connectAttr(
        ctl + '.parentInverseMatrix[0]', softmod + '.bindPreMatrix',
        force=True)
    cmds.connectAttr(
        ctl + '.parentMatrix[0]', softmod + '.softModXforms.preMatrix',
        force=True)
    cmds.connectAttr(
        ctl + '.matrix', softmod + '.softModXforms.weightedMatrix', force=True)
    cmds.setAttr(softmod + '.relative', True)

    # TODO: This should be done more dynamically. For now, we just suppose to
    # always have the same connection. I'm too lazy to check for existing
    # connections.
    handle_shape = cmds.listRelatives(softmod_handle, shapes=True)[0]
    cmds.disconnectAttr(handle_shape + '.softModTransforms [0]',
                        softmod + '.softModXforms')
    cmds.delete(softmod_handle)

    # Clean.
    cmds.setAttr(lattice + '.visibility', False)
    cmds.setAttr(lattice_base + '.visibility', False)
    cmds.setAttr(lattice + '.ihi', 0)
    cmds.setAttr(lattice_base + '.ihi', 0)
    cmds.setAttr(ffd + '.ihi', 0)
    cmds.setAttr(softmod + '.ihi', 0)

    lock_and_hide_keyable_attrs(ffd)
    lock_and_hide_keyable_attrs(lattice)
    lock_and_hide_keyable_attrs(lattice_base)
    lock_and_hide_keyable_attrs(softmod)


def create_moving_deformer_from_selection(name):
    '''
    Wrapper function to use the current selection for the
    create_moving_deformer method.
    '''
    geos = cmds.ls(selection=True, dag=True, type='mesh', noIntermediate=True)
    create_moving_deformer(geos, name)


def prune_deformer_membership(deformer, below=0.0001):
    '''
    Removes all members that have a weight below the given value.
    '''
    object_set = get_object_set_from_deformer(deformer)
    to_remove = []

    comp_list = cmds.ls(cmds.sets(object_set, query=True), flatten=True)
    weight_list = cmds.percent(deformer, comp_list, query=True, value=True)

    for comp, weight in zip(comp_list, weight_list):
        if weight < below:
            to_remove.append(comp)

    if to_remove:
        cmds.sets(to_remove, remove=object_set)


def get_object_set_from_deformer(deformer):
    '''
    This method parses the dag connections to get to the object set which
    should be the most reliable way.
    '''
    groupId = cmds.listConnections(deformer + '.input', type='groupId')
    if not groupId:
        raise DeformerConfigurationError(
            'Cannot get any groupId from deformer {}.'.format(deformer))

    # We take the first group id node. Normally, all group id nodes should be
    # connected to the same set. This should be secure until I find a situation
    # where this will not work.
    sets = cmds.listConnections(groupId[0], type='objectSet', plugs=True)

    # A groupId node might be inside a set itself, that's why we have to check
    # if the connection is done to a "groupNodes" attribute on the object
    # set.
    for each in sets:
        if each.count('groupNodes'):
            return each.split('.')[0]

    raise DeformerConfigurationError(
        'Could not find any valid object set for deformer {}. Check your '
        'configuration.'.format(deformer))
