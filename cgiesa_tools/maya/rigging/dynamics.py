# -*- coding: utf-8 -*-

# Below, we have an expression to compute a spring behavior.
'''
vector $pos = <<locator2.translateX, locator2.translateY, locator2.translateZ>>;
vector $lastPos = <<locator2.lastPosX, locator2.lastPosY, locator2.lastPosZ>>;
vector $lastForce = <<locator2.lastForceX, locator2.lastForceY, locator2.lastForceZ>>;
vector $newPos = $pos;
vector $force = <<0.0, 0.0, 0.0>>;

float $startFrame = locator2.startFrame;
float $stiff = locator2.stiffness;
float $damp = locator2.samping;

if (frame > $startFrame) {
    vector $diff = $pos - $lastPos;
    $force = $lastForce * (1 - $damp) + $stiff * $diff;
    $newPos = $lastPos + $force;
} 

locator1.translateX = $newPos.x;
locator1.translateY = $newPos.y;
locator1.translateZ = $newPos.z;
locator2.lastPosX = $newPos.x;
locator2.lastPosY = $newPos.y;
locator2.lastPosZ = $newPos.z;
locator2.lastForceX = $force.x;
locator2.lastForceY = $force.y;
locator2.lastForceZ = $force.z;
'''
