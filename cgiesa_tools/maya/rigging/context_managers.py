# -*- coding: utf-8 -*-

from contextlib import contextmanager

import maya.cmds as cmds


@contextmanager
def unable_node_evaluation(node):
    current_state = cmds.getAttr(node + '.nodeState')
    cmds.setAttr(node + '.nodeState', 1)

    try:
        yield
    except Exception, e:
        raise e
    finally:
        cmds.setAttr(node + '.nodeState', current_state)
