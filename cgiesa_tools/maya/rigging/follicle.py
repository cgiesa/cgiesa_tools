# -*- coding: utf-8 -*-

import maya.cmds as cmds
import maya.OpenMaya as om
import maya.api.OpenMaya as om2

from cgiesa_tools.maya.exceptions import SelectionError


def create_follicle_on_closest_point(geometry, position):
    '''
    Use the given geometry (nurbs or mesh) and creates a follic at the closest
    position from the given position.
    '''
    mesh = cmds.ls(geometry, dag=True, type='mesh', noIntermediate=True)
    surface = cmds.ls(geometry, dag=True, type='nurbsSurface',
                      noIntermediate=True)

    follicle = cmds.createNode('follicle')
    cmds.connectAttr(geometry + '.worldMatrix[0]',
                     follicle + '.inputWorldMatrix')

    if mesh:
        mesh = mesh[0]
        cmds.setAttr(mesh + '.quadSplit', 0)
        cmds.connectAttr(mesh + '.worldMesh', follicle + '.inputMesh')

        point = om2.MPoint(position[0], position[1], position[2])
        meshfn = om2.MFnMesh(om2.MSelectionList().add(mesh).getDagPath(0))
        u, v, face_id = meshfn.getUVAtPoint(point, om2.MSpace.kWorld)
    elif surface:
        point = om.MPoint(position[0], position[1], position[2])
        surface = surface[0]
        cmds.connectAttr(surface + '.local', follicle + '.inputSurface')

        sel_list = om.MSelectionList()
        sel_list.add(surface)
        nurbs_dag = om.MDagPath()
        sel_list.getDagPath(0, nurbs_dag)
        nurbsfn = om.MFnNurbsSurface(nurbs_dag)

        u_util = om.MScriptUtil()
        u_ptr = u_util.asDoublePtr()
        v_util = om.MScriptUtil()
        v_ptr = v_util.asDoublePtr()

        nurbsfn.closestPoint(point, u_ptr, v_ptr, False, 0.0001,
                             om.MSpace.kWorld)

        u = u_util.getDouble(u_ptr)
        v = v_util.getDouble(v_ptr)

        min_u = cmds.getAttr(surface + '.mnu')
        max_u = cmds.getAttr(surface + '.mxu')
        min_v = cmds.getAttr(surface + '.mnv')
        max_v = cmds.getAttr(surface + '.mxv')

        u = (u - min_u) / (max_u - min_u)
        v = (v - min_v) / (max_v - min_v)

    cmds.setAttr(follicle + '.parameterU', u)
    cmds.setAttr(follicle + '.parameterV', v)

    trs = cmds.listRelatives(follicle, parent=True)[0]
    cmds.connectAttr(follicle + '.outTranslate', trs + '.translate')
    cmds.connectAttr(follicle + '.outRotate', trs + '.rotate')

    return trs


def create_follicle_on_closest_point_from_selection():
    '''
    Wrapper for create_follicle_on_closest_point with current selection as
    input. First select the transform, second the surface or mesh.
    '''
    sel_error_msg = 'Select a transform and a geometry to create a follicle.'
    sel = cmds.ls(selection=True)
    if not sel:
        raise SelectionError(sel_error_msg)
    trs = cmds.ls(sel[0], type='transform')
    if not trs:
        raise SelectionError(sel_error_msg)
    geo = cmds.ls(sel[1], dag=True, type=['mesh', 'nurbsSurface'],
                  noIntermediate=True)
    if not geo:
        raise SelectionError(sel_error_msg)

    pos = cmds.xform(trs[0], query=True, rotatePivot=True, worldSpace=True)

    return create_follicle_on_closest_point(geo[0], pos)
