# -*- coding: utf-8 -*-

from collections import OrderedDict
from contextlib import contextmanager

import maya.api.OpenMaya as om2
import maya.cmds as cmds

from cgiesa_tools.general.math.interpolation import smoothstep, smootherstep, \
    smootheststep, clamp, linear
from cgiesa_tools.general.enum import enum
from cgiesa_tools.maya.exceptions import NodeTypeError
from cgiesa_tools.maya.context_managers import open_undo_chunk
from cgiesa_tools.maya.rigging.exceptions import DeformerConfigurationError
from cgiesa_tools.maya.rigging.deformer import get_object_set_from_deformer
from cgiesa_tools.maya.rigging.context_managers import unable_node_evaluation

INTERPOLATION = enum('LINEAR', 'SMOOTHSTEP', 'SMOOTHERSTEP', 'SMOOTHESTSTEP')


@contextmanager
def pause_normalize_weights(skin_cluster):
    current_state = cmds.getAttr(skin_cluster + '.normalizeWeights')
    cmds.setAttr(skin_cluster + '.normalizeWeights', 0)

    try:
        yield
    except Exception, e:
        raise e
    finally:
        cmds.setAttr(skin_cluster + '.normalizeWeights', current_state)


def reskin(geometry):
    '''
    This will update the bindPreMatrices of all skinClusters that can be found
    on the given geometry.

    TODO:
     - Need a cleaner way to get the skin clusters in the geometry's
       history. This would need more time.
     - Should check the exact matrix index to make this work with instances.
    '''
    history = cmds.listHistory(geometry)
    skin_clusters = cmds.ls(history, type='skinCluster')

    for cls in skin_clusters:
        for index in cmds.getAttr(cls + '.matrix', multiIndices=True):
            src = cmds.listConnections(
                cls + '.matrix[{}]'.format(index), source=True,
                destination=False, shapes=True)
            if not src:
                continue
            src = src[0]

            # TODO: Here we should check the matrix index.
            plug = src + '.worldInverseMatrix[0]'
            xform = cmds.getAttr(plug)
            cmds.setAttr(cls + '.bindPreMatrix[{}]'.format(index), *xform,
                         type='matrix')

            print index, plug, src, xform


def curve_skinning(curve, skin_cluster,
                   interpolation_type=INTERPOLATION.SMOOTHSTEP,
                   direction=0.0):
    '''
    This skinning method is very useful for cylindric objects. Instead of
    basing weights on distance to skin influences, it'll use a curve as
    reference. The idea is that the closest curve parameter will be used. This
    resolves in a very smooth skinning along the curve independently of the
    distance to it. You should be careful about the initial shape. This works
    the best when the shape is straight.

    We get all influence objects of the skin cluster. Then we get the closest
    parameters of all influences, line them up in the right order and blend
    then for each point between those parameters. Any point that is lying
    further than the first or last will get the full percentage of the first or
    last influence object.

    The interpolation can be changed to achieve different results.

    The direction helps to push or pull the current position forward or
    backward. This can help to avoid having too much influence before of after
    the influence object. The value should be between -1 and 1. 0 means
    centered, i.e. we use the actual parameter. Towards -1, we pull the
    parameter backwards to the last position and toward 1, we push.

    TODO:
     - Handle locked influences (should not change)
    '''
    # Check input.
    curve = cmds.ls(curve, dag=True, type='nurbsCurve', noIntermediate=True)
    if not curve:
        raise NodeTypeError('No valid curve is provided.')
    curve = curve[0]

    skin_cluster = cmds.ls(skin_cluster, type='skinCluster')
    if not skin_cluster:
        raise NodeTypeError('No valid skin cluster provided.')
    skin_cluster = skin_cluster[0]

    direction = clamp(direction, -1.0, 1.0)

    # We create a mfnnurbscurve for the closest point querying.
    mfncurve = om2.MFnNurbsCurve(om2.MSelectionList().add(curve).getDagPath(0))
    min_p, max_p = mfncurve.knotDomain

    # Get all components of the skin cluster.
    skin_set = get_object_set_from_deformer(skin_cluster)
    comps = cmds.ls(cmds.sets(skin_set, query=True), flatten=True)

    # Get all influences with their indexes.
    influence_dict = {}
    for index in cmds.getAttr(skin_cluster + '.matrix', multiIndices=True):
        influence = cmds.listConnections(
            skin_cluster + '.matrix[{}]'.format(index), shapes=True)
        if not influence:
            continue

        pivot = om2.MPoint(*(cmds.xform(
            influence, query=True, rotatePivot=True, worldSpace=True)))
        closest, param = mfncurve.closestPoint(pivot, space=om2.MSpace.kWorld)
        influence_dict[param] = index

    if not influence_dict:
        raise DeformerConfigurationError(
            'Could not get any influence objects from skin cluster {}.'.format(
                skin_cluster))

    # We put everything in an ordered dictionnary which should make the weight
    # computation much faster.
    ordered_dict = OrderedDict(sorted(influence_dict.items(),
                                      key=lambda t: t[0]))

    # We will offset the parameters depending on the direction parameter. -1
    # means we shift everything to the next parameter. 1 means the inverse. All
    # other values will be interpolated. For the border parameters, we'll
    # offset by the same amount than the neighbour one. All will be clamped to
    # the min and max parameter of the curve.
    id_list = ordered_dict.values()
    param_list = ordered_dict.keys()

    if direction < 0:
        start = len(ordered_dict) - 1
        end = 0
        step = -1
        direction *= -1
    else:
        start = 0
        end = len(ordered_dict) - 1
        step = 1

    last_diff = 0
    for i in range(start, end, step):
        current_p = param_list[i]
        next_p = param_list[i + step]
        new_p = linear(direction, current_p, next_p)
        last_diff = new_p - current_p
        param_list[i] = clamp(new_p, min_p, max_p)
    param_list[i + step] = clamp(param_list[i + step] + last_diff,
                                 min_p, max_p)
    ordered_dict = OrderedDict(zip(param_list, id_list))

    # We store all weights of each point in a list. So we can apply those
    # weights easily afterwards.
    weight_list = []
    for comp in comps:
        pos = om2.MPoint(*(cmds.pointPosition(comp, world=True)))
        closest, closest_param = mfncurve.closestPoint(
            pos, space=om2.MSpace.kWorld)

        before = None
        after = None
        for param in param_list:
            if param >= closest_param:
                after = param
                break
            before = param

        # Let's compute the linear interpolated value.
        weights = dict([(i, 0) for i in id_list])
        if before is None:
            weights[id_list[0]] = 1.0
        elif after is None:
            weights[id_list[-1]] = 1.0
        else:
            x = (closest_param - before) / (after - before)

            if interpolation_type == INTERPOLATION.SMOOTHSTEP:
                x = smoothstep(x)
            elif interpolation_type == INTERPOLATION.SMOOTHERSTEP:
                x = smootherstep(x)
            elif interpolation_type == INTERPOLATION.SMOOTHESTSTEP:
                x = smootheststep(x)

            weights[ordered_dict[before]] = 1 - x
            weights[ordered_dict[after]] = x
        weight_list.append(weights)

    # We apply the weights.
    plug = skin_cluster + '.weightList[{}].weights[{}]'
    with open_undo_chunk(), unable_node_evaluation(skin_cluster), \
            pause_normalize_weights(skin_cluster):
        [cmds.setAttr(plug.format(i, index), weight)
         for i, weights in enumerate(weight_list)
         for index, weight in weights.iteritems()]


def smooth_skinning(mesh, skin_cluster, ignore_selection=False):
    """Custom smoothing algorithm since the default one of Maya does not work.

    :param mesh: The name of the mesh.
    :type mesh: str

    :param skin_cluster: The name of the skin cluster.
    :type skin_cluster: str

    :param ignore_selection: If False, smoothing will only get applied on selected comps.
    :type ignore_selection: bool
    """
    pass
