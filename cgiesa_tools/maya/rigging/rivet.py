# -*- coding: utf-8 -*-

import maya.cmds as cmds

from cgiesa_tools.maya.exceptions import SelectionError


def create(edge1, edge2, name=None):
    '''
    This will create a rivet setup based on a selection of two edges.

    @param edge1: string, edge 1 for rivet

    @param edge2: string, edge 2 for rivet

    @param name: string, name of the rivet

    @return: string, name of the rivet transform

    TODO: Check if one curve needs a reverse.
    '''
    if not name:
        name = 'rivet1'

#     mesh = edge1.split('|')[-1]
    mesh = edge1.split('.')[0]

    # First the loft that will give us the surface to use to compute position
    # and normal/tangent information.
    loft = cmds.createNode('loft', name='loft_{}'.format(name))
    cmds.setAttr(loft + '.degree', 1)

    # Convert the edges to curves that will be fed into the loft node.
    i = 0
    for edge in [edge1, edge2]:
        vtxs = cmds.polyListComponentConversion(
            edge, fromEdge=True, toVertex=True)
        vtxs = cmds.ls(vtxs, flatten=True)
        vtxs = [v.split('.')[-1] for v in vtxs]

        edge_to_curve = cmds.createNode(
            'polyEdgeToCurve', name='edgeToCurve{}_{}'.format(i + 1, name))
        cmds.connectAttr(mesh + '.outMesh', edge_to_curve + '.inputPolymesh')
        cmds.connectAttr(mesh + '.worldMatrix[0]', edge_to_curve + '.inputMat')

        cmds.setAttr(edge_to_curve + '.inputComponents',
                     len(vtxs), *vtxs, type='componentList')
        cmds.setAttr(edge_to_curve + '.form', 0)  # open form
        cmds.setAttr(edge_to_curve + '.degree', 1)

        cmds.connectAttr(edge_to_curve + '.outputcurve',
                         loft + '.inputCurve[{}]'.format(i))
        i += 1

    # Build the aim and position setup and create a locator.
    rivet = cmds.createNode('transform', name=name)

    posi = cmds.createNode('pointOnSurfaceInfo',
                           name='pointOnSurfaceInfo_{}'.format(name))
    cmds.connectAttr(loft + '.outputSurface', posi + '.inputSurface')
    cmds.setAttr(posi + '.turnOnPercentage', True)
    cmds.setAttr(posi + '.parameterU', 0.5)
    cmds.setAttr(posi + '.parameterV', 0.5)

    cmds.connectAttr(posi + '.result.position', rivet + '.translate')

    aim = cmds.createNode('aimConstraint', name='aim_{}'.format(name))
    cmds.setAttr(aim + '.aimVector', 0, 1, 0)
    cmds.setAttr(aim + '.upVector', 0, 0, 1)

    cmds.connectAttr(posi + '.result.normal',
                     aim + '.target[0].targetTranslate')
    cmds.connectAttr(posi + '.result.tangentU', aim + '.worldUpVector')
    cmds.connectAttr(aim + '.constraintRotate', rivet + '.rotate')

    cmds.parent(aim, rivet)

    return rivet


def create_from_selection(name=None):
    '''Wrapper for create function taking the current selection.'''
    edges = cmds.filterExpand(selectionMask=32, expand=True, fullPath=True)

    if not edges or not len(edges) == 2:
        raise SelectionError('Please select two edges.')

    return create(edges[0], edges[1], name)
