import maya.cmds as cmds


def create_locator_on_selection_center():
    '''
    Take the current selection and creates a locator at its center.
    '''
    bb = cmds.exactWorldBoundingBox()
    center = [
        (bb[3] + bb[0]) * 0.5,
        (bb[4] + bb[1]) * 0.5,
        (bb[5] + bb[2]) * 0.5]
    locator = cmds.spaceLocator()[0]
    cmds.setAttr(locator + '.translate', *center)
