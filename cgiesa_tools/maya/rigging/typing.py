# -*- coding: utf-8 -*-

# This module handles node typing which is mostly used in rigging and
# animation tools to allow to detect nodes easily. There are predefined types
# but it can be any type of string if necessary. Any node can have as many
# types as necessary, there is no limitation of number of types.

import maya.cmds as cmds
from cgiesa_tools.general.enum import enum

TYPES = enum('CTL', 'SKIN', 'IK', 'FK', 'CENTER', 'LEFT', 'RIGHT', 'FRONT',
             'BACK', 'BAKE', 'BODY', 'FACE', 'SPINE', 'LEG', 'FOOT', 'ARM',
             'HAND', 'NECK', 'HEAD')
# CTL: Any controller for animation tools.
# SKIN: All nodes linked to skinning
# IK: Any ik (or ik similar) joint
# FK: Any fk joint
# CENTER: In center position
# LEFT: On the left side
# RIGHT: On the right side
# FRONT: Front limbs for quadrupeds
# BACK: Back limbs for quadrupeds
# BAKE: Anything that needs to be baked on the animation export.
# All following are different body parts for rigging.


def type_node(node, node_type):
    '''
    This method should add the given type to the node. If the type exists
    already, nothing will change. If other types exist, this one will be added.
    '''
    pass


def untype_node(node, node_type):
    '''
    This method should remove the given type to the node. If the type does not
    exist, nothing will change. If other types exist, this one will be added.
    '''
    pass


def has_node_type(node, node_type):
    '''
    Will return True or False if the given node has the given type.
    '''
    pass


def get_nodes_of_types(node_types):
    '''
    Returns all nodes of the given types. If more than one type is defined, all
    types must match.
    '''
    pass
