# -*- coding: utf-8 -*-

import maya.api.OpenMaya as om2


def get_ref_matrix(aim_vec, up_vec):
    '''
    The returns the reference matrix that can be used in rigging modules to
    orient joints into the right axises. First you orient as if x is your aim
    and y is your up axis. Then you multiply it with this reference matrix and
    your aim and up axises will correspond to what has been provided to this
    method.

    @param aim_vec: om2.MVector, must be normalized

    @param up_vec: om2.MVector, must be normalized

    @return om2.MMatrix
    '''
    sec_up_vec = aim_vec ^ up_vec
    sec_up_vec.normalize()
    up_vec = sec_up_vec ^ aim_vec

    ref_matrix = om2.MMatrix(
        [aim_vec.x, aim_vec.y, aim_vec.z, 0.0,
         up_vec.x, up_vec.y, up_vec.z, 0.0,
         sec_up_vec.x, sec_up_vec.y, sec_up_vec.z, 0.0,
         0.0, 0.0, 0.0, 1.0])
    return ref_matrix.inverse()
