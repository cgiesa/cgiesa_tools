# -*- coding: utf-8 -*-

from cgiesa_tools.general.enum import enum

SIDE = enum('LEFT', 'RIGHT', 'CENTER')

SIDE_PREFIX = {
    SIDE.LEFT: 'L',
    SIDE.RIGHT: 'R'}

CTL_COLOR = {
    SIDE.LEFT: 6,
    SIDE.RIGHT: 13,
    SIDE.CENTER: 17
    }

GLOBAL_SCALE_ATTR = 'globalScale'
SELECTABLE_GEO_ATTR = 'selectableGeometry'
