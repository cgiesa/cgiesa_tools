# -*- coding: utf-8 -*-

import maya.cmds as cmds


def hide_selection_in_cb():
    '''
    Sets the ihi attribute to 0 on selected objects.
    '''
    for each in cmds.ls(selection=True):
        cmds.setAttr(each + '.ihi', 0)


def get_keyable_attributes(node):
    '''
    Returns all keyable attributes of the given node.
    '''
    attrs = cmds.listAttr(node, keyable=True, multi=True)
    if not attrs:
        return []
    return attrs


def lock_and_hide_keyable_attrs(node):
    '''
    Locks and hide all keyable attributes of the given node.
    '''
    lock_and_hide_attrs(node, get_keyable_attributes(node))


def lock_and_hide_attrs(node, attrs):
    '''
    This is the core function that locks and hides all given attributes.
    '''
    for attr in attrs:
        plug = node + '.' + attr
        cmds.setAttr(plug, lock=True)
        cmds.setAttr(plug, keyable=False)


def unlock_and_show_attrs(node, attrs):
    '''
    This is the core function that locks and hides all given attributes.
    '''
    for attr in attrs:
        plug = node + '.' + attr
        cmds.setAttr(plug, lock=False)
        cmds.setAttr(plug, keyable=True)
        cmds.setAttr(plug, channelBox=True)


def copy_connections(frm, to, new):
    '''
    Copies all connections between "frm" and "to" and links "new".
    '''
    in_conn = cmds.listConnections(frm, source=True, destination=False,
                                   plugs=True, shapes=True, connections=True)
    out_conn = cmds.listConnections(frm, source=False, destination=True,
                                    plugs=True, shapes=True, connections=True)

    if not out_conn:
        out_conn = []
    if not in_conn:
        in_conn = []

    to = cmds.ls(to, long=True)

    for i in range(0, len(out_conn), 2):
        src_plug = out_conn[i]
        dst_plug = out_conn[i + 1]

        dst_node = cmds.ls(dst_plug.split('.')[0], long=True)
        if dst_node != to:
            continue

        src_plug = '.'.join(src_plug.split('.')[1:])
        src_plug = new + '.' + src_plug
        cmds.connectAttr(src_plug, dst_plug, force=True)
        print src_plug + ' --> ' + dst_plug

    for i in range(0, len(in_conn), 2):
        src_plug = in_conn[i + 1]
        dst_plug = in_conn[i]

        src_node = cmds.ls(src_plug.split('.')[0], long=True)
        if src_node != to:
            continue

        dst_plug = '.'.join(dst_plug.split('.')[1:])
        dst_plug = new + '.' + dst_plug
        cmds.connectAttr(src_plug, dst_plug, force=True)

        print src_plug + ' --> ' + dst_plug
