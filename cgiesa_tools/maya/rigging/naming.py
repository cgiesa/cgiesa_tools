# -*- coding: utf-8 -*-

import maya.cmds as cmds
import maya.api.OpenMaya as om2


def _apply_naming_function(nodes, func, *args):
    '''
    This is the core function to apply all naming changes.
    '''
    # Use the api to get the nodes. This allows working code with clashing
    # node names in a hierarchy.
    node_list = om2.MSelectionList()
    node_name_funcs = []
    node_names = []
    for node_name in nodes:
        node_list.add(node_name)
        try:
            dag_path = node_list.getDagPath(0)
            node_name_funcs.append(dag_path.fullPathName)
            depend_node = om2.MFnDependencyNode(dag_path.node())
            node_names.append(depend_node.name())
        except TypeError:
            depend_node = om2.MFnDependencyNode(node_list.getDependNode(0))
            node_name_funcs.append(depend_node.absoluteName)
            node_names.append(depend_node.name())
        node_list.clear()

    for node_name_func, node_name in zip(node_name_funcs, node_names):
        func(node_name_func(), node_name, *args)


def add_prefix(prefix, node_names):
    def func(node, node_name, prefix):
        cmds.rename(node, prefix + node_name)
    _apply_naming_function(node_names, func, prefix)


def add_suffix(suffix, node_names):
    def func(node, node_name, suffix):
        cmds.rename(node, node_name + suffix)
    _apply_naming_function(node_names, func, suffix)


def replace(search, replace, node_names):
    def func(node, node_name, search, replace):
        cmds.rename(node, node_name.replace(search, replace))
    _apply_naming_function(node_names, func, search, replace)


def multi_name(name, node_names, start=1, by=1, digits=2):
    '''
    When having multiple objects selected, you can provide a name and it will
    add and underscore and the number in order of the given nodes. The
    enumeration can be controlled via start, by and digits.
    '''
    class IncreaseValue(object):

        def __init__(self, val):
            self._val = val

        def get_val(self):
            return self._val

        def increase_by(self, by):
            self._val += by

    def func(node, node_name, name, val, by, digits):
        cmds.rename(node, ('{{}}_{{:0{}d}}'.format(digits)).format(
            name, val.get_val()))
        val.increase_by(by)
    _apply_naming_function(node_names, func, name, IncreaseValue(start),
                           by, digits)
