# -*- coding: utf-8 -*-

import maya.cmds as cmds

from node import lock_and_hide_keyable_attrs, lock_and_hide_attrs, \
  unlock_and_show_attrs

TRANSFORM_ATTRS = ['t', 'r', 's']
AXISES = ['x', 'y', 'z']


def add_hook_on_selection():
    '''
    Takes the current selection and applies the add_hook method.
    '''
    for each in cmds.ls(selection=True, transforms=True):
        add_hook(each)


def add_hook(transform, lock_attrs_on_hook=False):
    '''
    Adds a hook transform on top of the given one.
    '''
    transform = cmds.ls(transform, transforms=True, long=True)
    if not transform:
        cmds.error('No valid transform provided.')
    transform = transform[0]

    for attr in TRANSFORM_ATTRS:
        plug = transform + '.' + attr
        if cmds.getAttr(plug, lock=True):
            cmds.warning('Attribute {} is locked. Result of the hook setup is '
                         'unpredictable.'.format(plug))
            for axis in AXISES:
                plug = transform + '.' + attr + axis
                if cmds.getAttr(plug, lock=True):
                    cmds.warning('Attribute {} is locked. Result of the hook '
                                 'setup is unpredictable.'.format(plug))

    parent = cmds.listRelatives(transform, parent=True, fullPath=True)
    if parent:
        parent = parent[0]

    xform = cmds.xform(transform, query=True, worldSpace=True, matrix=True)

    name = transform.split('|')[-1] + '_hook'

    if not parent:
        hook = cmds.createNode('transform', name=name)
    else:
        hook = cmds.createNode('transform', name=name, parent=parent)

    cmds.xform(hook, worldSpace=True, matrix=xform)
    hook = cmds.ls(hook, long=True)[0]

    # Put the hook at the right spot.
    if parent:
        children = cmds.listRelatives(parent, type='transform', fullPath=True)
    else:
        children = cmds.ls(assemblies=True, long=True)

    index1 = children.index(transform)
    index2 = children.index(hook)

    # In some cases (not clear exactly when), Maya throws a RuntimeError when
    # reordering at scene root level. A warning is thrown and the hook will
    # be placed where it has been created. This should not make any further
    # problems. This is just esthetic.
    try:
        cmds.reorder(hook, relative=index1 - index2 + 1)
    except RuntimeError:
        cmds.warning('Could not reorder the hook due to unkown reasons. This '
                     'does NOT mean that the hook setup is broken.')

    # TODO: We might need to set everything to 0. In some cases, we have 360 or
    # 180 values on rotation and/or jointOrient.
    cmds.parent(transform, hook)

    if lock_attrs_on_hook:
        lock_and_hide_keyable_attrs(hook)

    return hook


def unlock_and_show_transform_attrs(node):
    '''
    Unlocks and makes transform attributes keyable.
    '''
    attrs = [n + a for n in TRANSFORM_ATTRS for a in AXISES]
    unlock_and_show_attrs(node, attrs)


def lock_and_hide_transform_attrs(node, attrs=None, hide_visibility=True):
    '''
    Locks and hides all transform attributes. You can provide attributes like
    translate, rotate or scale and it will lock the compound and all axises.
    '''
    if attrs is None:
        attrs = TRANSFORM_ATTRS[:]
    attrs.extend([n + a for n in attrs for a in AXISES])

    lock_and_hide_attrs(node, attrs)

    # This is a little hack, I admit. But it makes the process just easier.
    # Visibility is not a transform attribute, but most of the time, when we
    # lock and hide transform attrs, we want to hide the visibility as well.
    if hide_visibility:
        cmds.setAttr(node + '.visibility', keyable=False, channelBox=False)


def lock_and_hide_trs_attrs_on_selection():
    '''
    Little wrapper for lock_and_hide_transform_attrs that take the current
    selection as input. For joints, there is a little short cut. It will lock
    and hide the "radius" attribute as well.
    '''
    sel = cmds.ls(selection=True)
    if not sel:
        return
    jnts = cmds.ls(sel, type='joint')
    if jnts:
        for jnt in jnts:
            lock_and_hide_transform_attrs(jnt)
            lock_and_hide_attrs(jnt, 'radius')
    trss = cmds.ls(sel, exactType='transform')
    if trss:
        for trs in trss:
            lock_and_hide_transform_attrs(trs)


def unlock_and_show_trs_attrs_on_selection():
    '''
    Wrapper for unlock_and_show_transform_attrs that take the current selection
    as input.
    '''
    sel = cmds.ls(selection=True, type='transform')
    if not sel:
        return
    for node in sel:
        unlock_and_show_transform_attrs(node)
