# -*- coding: utf-8 -*-

import maya.cmds as cmds
from cgiesa_tools.maya.rigging.controller import create_ctl, CTL_TYPE
from cgiesa_tools.maya.rigging.node import lock_and_hide_keyable_attrs
from cgiesa_tools.maya.rigging.config import GLOBAL_SCALE_ATTR, \
  SELECTABLE_GEO_ATTR, CTL_COLOR, SIDE


def create(name, model_group=None):
    '''
    This build the root rig which is the basis of all rigs. Simple props can
    be setupped only with this. It creates the basic groups, a main and
    secondary controller. If a model group is provided, the selectableGeo
    attribute gets setupped directly.
    '''
    rig_group = cmds.createNode('transform', name=name)
    root = cmds.createNode('transform', name='root', parent=rig_group)
    main = create_ctl(CTL_TYPE.CIRCLE, 'main', parent=root)
    secondary = create_ctl(CTL_TYPE.CIRCLEARROWS, 'secondary', parent=main)

    ctl_shapes = []
    ctl_shapes.extend(cmds.listRelatives(main, shapes=True))
    ctl_shapes.extend(cmds.listRelatives(secondary, shapes=True))

    lock_and_hide_keyable_attrs(rig_group)
    lock_and_hide_keyable_attrs(root)
    cmds.setAttr(root + '.ihi', 0)
    cmds.setAttr(main + '.ihi', 0)
    cmds.setAttr(secondary + '.ihi', 0)

    cmds.addAttr(main, longName=GLOBAL_SCALE_ATTR, min=0.001, defaultValue=1.0,
                 keyable=True)
    cmds.addAttr(main, longName=SELECTABLE_GEO_ATTR, min=0, max=1,
                 attributeType='short', keyable=True)

    cmds.connectAttr(main + '.' + GLOBAL_SCALE_ATTR, main + '.scaleX')
    cmds.connectAttr(main + '.' + GLOBAL_SCALE_ATTR, main + '.scaleY')
    cmds.connectAttr(main + '.' + GLOBAL_SCALE_ATTR, main + '.scaleZ')

    reverse = cmds.createNode('reverse', name=SELECTABLE_GEO_ATTR + '_reverse')
    mult = cmds.createNode('multDoubleLinear',
                           name=SELECTABLE_GEO_ATTR + '_mult')
    cmds.setAttr(reverse + '.ihi', 0)
    cmds.setAttr(mult + '.ihi', 0)

    cmds.connectAttr(main + '.' + SELECTABLE_GEO_ATTR, reverse + '.inputX')
    cmds.connectAttr(reverse + '.outputX', mult + '.input1')
    cmds.setAttr(mult + '.input2', 2)

    if model_group is not None:
        cmds.setAttr(model_group + '.overrideEnabled', True)
        cmds.connectAttr(mult + '.output',
                         model_group + '.overrideDisplayType')
        model_group = cmds.parent(model_group, rig_group)[0]

    for shape in ctl_shapes:
        cmds.setAttr(shape + '.overrideEnabled', True)
        cmds.setAttr(shape + '.overrideColor', CTL_COLOR[SIDE.CENTER])

    cmds.setAttr(main + '.s', lock=True, keyable=False)
    cmds.setAttr(main + '.scaleX', lock=True, keyable=False)
    cmds.setAttr(main + '.scaleY', lock=True, keyable=False)
    cmds.setAttr(main + '.scaleZ', lock=True, keyable=False)
    cmds.setAttr(main + '.visibility', keyable=False, channelBox=False)

    cmds.setAttr(secondary + '.s', lock=True, keyable=False)
    cmds.setAttr(secondary + '.scaleX', lock=True, keyable=False)
    cmds.setAttr(secondary + '.scaleY', lock=True, keyable=False)
    cmds.setAttr(secondary + '.scaleZ', lock=True, keyable=False)
    cmds.setAttr(secondary + '.visibility', keyable=False, channelBox=False)

    lock_and_hide_keyable_attrs(reverse)
    lock_and_hide_keyable_attrs(mult)
