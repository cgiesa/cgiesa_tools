# -*- coding: utf-8 -*-

import maya.cmds as cmds
import maya.api.OpenMaya as om2

from cgiesa_tools.maya.rigging.config import SIDE, CTL_COLOR, SIDE_PREFIX
from cgiesa_tools.general.enum import enum
from cgiesa_tools.maya.rigging.controller import create_ctl, CTL_TYPE
from cgiesa_tools.maya.rigging.transform import lock_and_hide_transform_attrs
from cgiesa_tools.maya.rigging.node import lock_and_hide_keyable_attrs
from cgiesa_tools.maya.rigging.maths import get_ref_matrix

TYPE = enum('ARM', 'LEG')

NAMES = {
    TYPE.ARM: {
        'main': 'arm',
        'upper': 'shoulder',
        'lower': 'elbow',
        'tip': 'hand'},
    TYPE.LEG: {
        'main': 'leg',
        'upper': 'hip',
        'lower': 'knee',
        'tip': 'foot'}
    }

EXPR = '''// This expression has been generated automatically by the limb rig
// procedure.

// The value 1.1 is just eyeballed and avoids having the round
// groups going to infinity.
float $cos_angle = cos(
    smoothstep(0.0, 1.1, deg_to_rad({angle_between}.angle * 0.5)) * 1.1);

{upper_round_grp}.translateX = ({upper_half_distance} / $cos_angle) * {aim_x};
{upper_round_grp}.translateY = ({upper_half_distance} / $cos_angle) * {aim_y};
{upper_round_grp}.translateZ = ({upper_half_distance} / $cos_angle) * {aim_z};
{lower_round_grp}.translateX = ({lower_half_distance} / $cos_angle) * {aim_x};
{lower_round_grp}.translateY = ({lower_half_distance} / $cos_angle) * {aim_y};
{lower_round_grp}.translateZ = ({lower_half_distance} / $cos_angle) * {aim_z};
'''


def _setup_offset_controller(main, name, limb_grp, round_grp, pos):
    '''
    This creates the offset controller for either the upper or lower limb which
    is blendable between a round and straight setup.
    '''
    ctl = create_ctl(CTL_TYPE.ARROWBALL, name=name + '_offset_ctl',
                     parent=limb_grp, add_hook=True)
    hook = cmds.listRelatives(ctl, parent=True)[0]

    cmds.setAttr(hook + '.translate', pos.x, pos.y, pos.z)

    # Constraint to the round setup.
    const = cmds.pointConstraint(round_grp, hook, name=hook + '_pc')[0]
    cmds.setAttr(const + '.ihi', 0)

    # Create the blend.
    blend = cmds.createNode('pairBlend', name=name + '_round_blend',
                            skipSelect=True)
    cmds.setAttr(blend + '.ihi', 0)

    cmds.setAttr(blend + '.inTranslate1', pos.x, pos.y, pos.z)
    cmds.setAttr(blend + '.inTranslate1', lock=True)

    cmds.connectAttr(const + '.constraintTranslate', blend + '.inTranslate2')
    cmds.connectAttr(main + '.roundness', blend + '.weight')

    cmds.disconnectAttr(const + '.constraintTranslateX', hook + '.translateX')
    cmds.disconnectAttr(const + '.constraintTranslateY', hook + '.translateY')
    cmds.disconnectAttr(const + '.constraintTranslateZ', hook + '.translateZ')
    cmds.connectAttr(blend + '.outTranslate', hook + '.translate')

    lock_and_hide_keyable_attrs(const)
    lock_and_hide_keyable_attrs(blend)
    lock_and_hide_transform_attrs(hook)
    lock_and_hide_transform_attrs(ctl, ['r', 's'])
    cmds.setAttr(hook + '.visibility', keyable=False)
    cmds.setAttr(ctl + '.visibility', keyable=False)

    cmds.connectAttr(main + '.showExtraCtls', ctl + '.visibility')

    return ctl


def _create_border_handle(name, parent, aim, up, aim_object):
    '''
    For the bezier curve setup, this method creates the handles at the borders.
    '''
    handle_hook = cmds.createNode(
        'transform', name=name + '_bezier_handle_hook', parent=parent,
        skipSelect=True)
    handle = cmds.createNode(
        'transform', name=name + '_bezier_handle', parent=handle_hook,
        skipSelect=True)

    const = cmds.aimConstraint(
        aim_object, handle_hook, name=handle_hook + '_ac',
        aim=[aim.x, aim.y, aim.z], upVector=[up.x, up.y, up.z],
        worldUpVector=[up.x, up.y, up.z],
        worldUpType='objectrotation', worldUpObject=parent)[0]
    cmds.setAttr(const + '.ihi', 0)

    lock_and_hide_keyable_attrs(const)
    lock_and_hide_keyable_attrs(handle_hook)

    return handle


def _setup_handle_global_scale(main, name, grp, offset_ctl, handle_grp, aim,
                               reverse=False):
    '''
    The handles have to scale with the global scale. This methods makes sure
    that this works for the border handles.
    '''
    distance = cmds.createNode(
        'distanceBetween', name=name + '_bezier_globalScale_dist',
        skipSelect=True)
    cmds.setAttr(distance + '.ihi', 0)

    cmds.connectAttr(grp + '.worldMatrix[0]', distance + '.inMatrix1')
    cmds.connectAttr(offset_ctl + '.worldMatrix[0]', distance + '.inMatrix2')

    divide_global = cmds.createNode(
        'multiplyDivide', name=name + '_bezier_globalScale_divide',
        skipSelect=True)
    cmds.setAttr(divide_global + '.ihi', 0)
    cmds.setAttr(divide_global + '.operation', 2)  # Divide

    cmds.connectAttr(distance + '.distance', divide_global + '.input1X')
    cmds.connectAttr(main + '.globalScale', divide_global + '.input2X')

    handle_global_mult = cmds.createNode(
        'multDoubleLinear', name=name + '_bezier_globalScale_mult',
        skipSelect=True)
    cmds.setAttr(handle_global_mult + '.ihi', 0)

    if reverse:
        cmds.setAttr(handle_global_mult + '.input2', -0.5)
    else:
        cmds.setAttr(handle_global_mult + '.input2', 0.5)

    cmds.connectAttr(divide_global + '.outputX',
                     handle_global_mult + '.input1')

    handle_aim_mult = cmds.createNode(
        'multiplyDivide', skipSelect=True, name=name + '_bezier_aim_mult')
    cmds.setAttr(handle_aim_mult + '.ihi', 0)
    cmds.setAttr(handle_aim_mult + '.input2X', aim.x)
    cmds.setAttr(handle_aim_mult + '.input2Y', aim.y)
    cmds.setAttr(handle_aim_mult + '.input2Z', aim.z)

    cmds.connectAttr(handle_global_mult + '.output',
                     handle_aim_mult + '.input1X')
    cmds.connectAttr(handle_global_mult + '.output',
                     handle_aim_mult + '.input1Y')
    cmds.connectAttr(handle_global_mult + '.output',
                     handle_aim_mult + '.input1Z')

    cmds.connectAttr(handle_aim_mult + '.output',
                     handle_grp + '.translate')

    lock_and_hide_keyable_attrs(distance)
    lock_and_hide_keyable_attrs(divide_global)
    lock_and_hide_keyable_attrs(handle_global_mult)


def _setup_lower_limb_handles(name, round_ctl, main, aim, up, aim_ctl, parent,
                              reverse):
    handle_hook = cmds.createNode(
        'transform', name=name + '_handle_hook', parent=parent,
        skipSelect=True)

    const = cmds.aimConstraint(
        aim_ctl, handle_hook, name=handle_hook + '_ac',
        aim=[aim.x, aim.y, aim.z], upVector=[up.x, up.y, up.z],
        worldUpVector=[up.x, up.y, up.z],
        worldUpType='objectrotation', worldUpObject=parent)[0]
    cmds.setAttr(const + '.ihi', 0)

    blend = cmds.createNode('pairBlend', name=const + '_blend',
                            skipSelect=True)
    cmds.setAttr(blend + '.ihi', 0)
    cmds.setAttr(blend + '.rotInterpolation', 1)  # Quaternion
    cmds.setAttr(blend + '.inRotate2', 0, 0, 0)

    cmds.connectAttr(const + '.constraintRotate', blend + '.inRotate1')
    cmds.connectAttr(round_ctl + '.roundness', blend + '.weight')

    cmds.disconnectAttr(const + '.constraintRotateX', handle_hook + '.rotateX')
    cmds.disconnectAttr(const + '.constraintRotateY', handle_hook + '.rotateY')
    cmds.disconnectAttr(const + '.constraintRotateZ', handle_hook + '.rotateZ')
    cmds.connectAttr(blend + '.outRotate', handle_hook + '.rotate')

    lock_and_hide_keyable_attrs(const)
    lock_and_hide_keyable_attrs(blend)

    handle = cmds.createNode(
        'transform', name=name + '_handle', parent=handle_hook,
        skipSelect=True)

    _setup_handle_global_scale(main, name, handle_hook, aim_ctl, handle, aim,
                               reverse=reverse)

    lock_and_hide_transform_attrs(handle)
    lock_and_hide_transform_attrs(handle_hook)

    return handle


def create(upper_pos, lower_pos, lower_tip_pos, limb_type, side,
           number_of_between_joints=3, round_setup=True,
           aim_vector=[1.0, 0.0, 0.0], up_vector=[0.0, 1.0, 0.0],
           world_up_vector=[0.0, 1.0, 0.0]):
    '''
    This will create the entire rig. We only need the three positions for the
    limb. All axises will be computed automatically. Just make sure that the
    three positions are not on one line which would make it impossible to
    compute an up-vector.

    @param upper_pos: list of 3 floats, pos of the upper limb member

    @param lower_pos: list of 3 floats, pos of the middle limb member

    @param lower_tip_pos: list of 3 floats, pos of the tip limb member

    @param limb_type: use TYPE.ARM or TYPE.LEG

    @param side: SIDE.LEFT or SIDE.RIGHT

    @param number_of_between_joints: int, for round and twist setup, this
      defines the number of additional joints for the upper and lower limb that
      will get placed between the parent and child joint

    @param round_setup: bool, apply or not the round setup

    @param aim_vector: list of 3 floats, the joint's aim vector that points to
      the child joint

    @param up_vector: list of 3 floats, the joint's up vector that points to
      the world up vector

    @param world_up_vector: list of 3 floats, the vector that is used to align
      the up vector of the joints

    TODO:
    - requires matrixNodes plugin
    - Round setup is temporary (perhaps there is a way to make this with less
      nodes)
    - Check if the limb is straight
    - Twist setup should run without iks (too slow)
    - Add inbetween joints option
    - Add ik option
    - Remove expression
    - Add clavicle setup for arm
    '''
    # We cannot have a round setup without between joints.
    if not number_of_between_joints:
        round_setup = False

    # Prepare the vectors and matrices for the correct orientation.
    aim_vector = om2.MVector(*aim_vector)
    up_vector = om2.MVector(*up_vector)
    world_up_vector = om2.MVector(*world_up_vector)

    aim_vector.normalize()
    up_vector.normalize()
    world_up_vector.normalize()

    if side == SIDE.RIGHT:
        side_mult = -1.0
        aim_vector *= -1
        up_vector *= -1
    elif side == SIDE.LEFT:
        side_mult = 1.0
    else:
        cmds.error('The limb rig exists only for left or right side.')

    aim_vector.normalize()
    up_vector.normalize()

    ref_matrix_inv = get_ref_matrix(aim_vector, up_vector)

    # We start computing the axises from the plane defined by the three limb
    # members.
    pos1 = om2.MVector(*upper_pos)
    pos2 = om2.MVector(*lower_pos)
    pos3 = om2.MVector(*lower_tip_pos)

    upper_half_dist = (pos2 - pos1).length() * 0.5
    lower_half_dist = (pos3 - pos2).length() * 0.5

    aim1 = pos2 - pos1
    aim2 = pos3 - pos2
    aim1.normalize()
    aim2.normalize()

    plane_up = aim1 ^ aim2
    plane_up.normalize()
    # Check if the plane_up does align with the world up, otherwise make it
    # negative.
    if plane_up * world_up_vector < 0.0:
        plane_up *= -1

    up1 = aim1 ^ plane_up
    up2 = aim2 ^ plane_up
    up1.normalize()
    up2.normalize()

    mat1 = om2.MMatrix([
        aim1.x, aim1.y, aim1.z, 0.0,
        plane_up.x, plane_up.y, plane_up.z, 0.0,
        up1.x, up1.y, up1.z, 0.0,
        pos1.x, pos1.y, pos1.z, 1.0])

    mat2 = om2.MMatrix([
        aim2.x, aim2.y, aim2.z, 0.0,
        plane_up.x, plane_up.y, plane_up.z, 0.0,
        up2.x, up2.y, up2.z, 0.0,
        pos2.x, pos2.y, pos2.z, 1.0])

    mat3 = om2.MMatrix([
        aim2.x, aim2.y, aim2.z, 0.0,
        plane_up.x, plane_up.y, plane_up.z, 0.0,
        up2.x, up2.y, up2.z, 0.0,
        pos3.x, pos3.y, pos3.z, 1.0])

    mat1 = ref_matrix_inv * mat1
    mat2 = ref_matrix_inv * mat2
    mat3 = ref_matrix_inv * mat3

    # -------------------------------------------------------------------------
    # Create all groups and controllers.
    # -------------------------------------------------------------------------
    # Create main groups.
    name = '{}_{}'.format(SIDE_PREFIX[side], NAMES[limb_type]['main'])
    root = cmds.createNode('transform', name=name + '_root_grp',
                           skipSelect=True)
    main = cmds.createNode('transform', name=name + '_main_grp', parent=root,
                           skipSelect=True)
    clean = cmds.createNode('transform', name=name + '_clean_grp', parent=root,
                            skipSelect=True)

    ctl_shapes = []

    cmds.addAttr(main, longName='globalScale', min=0.001, defaultValue=1.0,
                 keyable=True)

    cmds.connectAttr(main + '.globalScale', main + '.scaleX')
    cmds.connectAttr(main + '.globalScale', main + '.scaleY')
    cmds.connectAttr(main + '.globalScale', main + '.scaleZ')

    lock_and_hide_transform_attrs(root)
    lock_and_hide_transform_attrs(main)
    lock_and_hide_transform_attrs(clean)

    # Create fk controllers.
    upper_name = '{}_{}'.format(SIDE_PREFIX[side], NAMES[limb_type]['upper'])
    lower_name = '{}_{}'.format(SIDE_PREFIX[side], NAMES[limb_type]['lower'])
    tip_name = '{}_{}'.format(SIDE_PREFIX[side], NAMES[limb_type]['tip'])

    upper_ctl = create_ctl(CTL_TYPE.CIRCLEX, upper_name + '_fk_ctl',
                           parent=main, add_hook=True)
    lower_ctl = create_ctl(CTL_TYPE.CIRCLEX, lower_name + '_fk_ctl',
                           parent=upper_ctl, add_hook=True)
    tip_ctl = create_ctl(CTL_TYPE.CIRCLEX, tip_name + '_fk_ctl',
                         parent=lower_ctl, add_hook=True)

    upper_ctl_hook = cmds.listRelatives(upper_ctl, parent=True, path=True)[0]
    lower_ctl_hook = cmds.listRelatives(lower_ctl, parent=True, path=True)[0]
    tip_ctl_hook = cmds.listRelatives(tip_ctl, parent=True, path=True)[0]

    cmds.xform(upper_ctl_hook, worldSpace=True, matrix=[n for n in mat1])
    cmds.xform(lower_ctl_hook, worldSpace=True, matrix=[n for n in mat2])
    cmds.xform(tip_ctl_hook, worldSpace=True, matrix=[n for n in mat3])

    lock_and_hide_transform_attrs(lower_ctl_hook)
    lock_and_hide_transform_attrs(tip_ctl_hook)

    ctl_shapes.extend(cmds.listRelatives(upper_ctl, shapes=True))
    ctl_shapes.extend(cmds.listRelatives(lower_ctl, shapes=True))
    ctl_shapes.extend(cmds.listRelatives(tip_ctl, shapes=True))

    # Create the limb groups for round setup.
    upper_grp = cmds.createNode('transform', name=upper_name + '_rig_grp',
                                parent=main, skipSelect=True)
    lower_grp = cmds.createNode('transform', name=lower_name + '_rig_grp',
                                parent=upper_grp, skipSelect=True)
    tip_grp = cmds.createNode('transform', name=tip_name + '_rig_grp',
                              parent=lower_grp, skipSelect=True)
    limb_ctl = create_ctl(CTL_TYPE.STARY, name=name + '_param_ctl',
                          parent=tip_grp)

    ctl_shapes.extend(cmds.listRelatives(limb_ctl, shapes=True))

    cmds.xform(upper_grp, worldSpace=True, matrix=[n for n in mat1])
    cmds.xform(lower_grp, worldSpace=True, matrix=[n for n in mat2])
    cmds.xform(tip_grp, worldSpace=True, matrix=[n for n in mat3])

    lock_and_hide_keyable_attrs(limb_ctl)
    if round_setup:
        cmds.addAttr(limb_ctl, longName='roundness', min=0, max=1,
                     keyable=True)
    cmds.addAttr(limb_ctl, longName='showExtraCtls', attributeType='short',
                 min=0, max=1, keyable=True)

    upper_matrix = cmds.createNode(
        'decomposeMatrix', name=upper_name + '_matrix', skipSelect=True)
    lower_matrix = cmds.createNode(
        'decomposeMatrix', name=lower_name + '_matrix', skipSelect=True)
    tip_matrix = cmds.createNode(
        'decomposeMatrix', name=tip_name + '_matrix', skipSelect=True)

    cmds.setAttr(upper_matrix + '.ihi', 0)
    cmds.setAttr(lower_matrix + '.ihi', 0)
    cmds.setAttr(tip_matrix + '.ihi', 0)

    cmds.connectAttr(upper_grp + '.worldMatrix[0]',
                     upper_matrix + '.inputMatrix')
    cmds.connectAttr(lower_grp + '.worldMatrix[0]',
                     lower_matrix + '.inputMatrix')
    cmds.connectAttr(tip_grp + '.worldMatrix[0]',
                     tip_matrix + '.inputMatrix')

    upper_up_vector = None
    lower_up_vector = None
    tip_up_vector = None
    if number_of_between_joints:
        upper_up_vector = cmds.createNode(
            'vectorProduct', name=upper_name + '_upVector', skipSelect=True)
        lower_up_vector = cmds.createNode(
            'vectorProduct', name=lower_name + '_upVector', skipSelect=True)
        tip_up_vector = cmds.createNode(
            'vectorProduct', name=tip_name + '_upVector', skipSelect=True)

        cmds.setAttr(upper_up_vector + '.ihi', 0)
        cmds.setAttr(lower_up_vector + '.ihi', 0)
        cmds.setAttr(tip_up_vector + '.ihi', 0)

        cmds.setAttr(upper_up_vector + '.operation', 3)  # vector-matrix-mult
        cmds.setAttr(lower_up_vector + '.operation', 3)  # vector-matrix-mult
        cmds.setAttr(tip_up_vector + '.operation', 3)  # vector-matrix-mult

        cmds.setAttr(upper_up_vector + '.input1',
                     up_vector.x, up_vector.y, up_vector.z)
        cmds.setAttr(lower_up_vector + '.input1',
                     up_vector.x, up_vector.y, up_vector.z)
        cmds.setAttr(tip_up_vector + '.input1',
                     up_vector.x, up_vector.y, up_vector.z)

        cmds.connectAttr(upper_grp + '.worldMatrix[0]',
                         upper_up_vector + '.matrix')
        cmds.connectAttr(lower_grp + '.worldMatrix[0]',
                         lower_up_vector + '.matrix')
        cmds.connectAttr(tip_grp + '.worldMatrix[0]',
                         tip_up_vector + '.matrix')

    # For now, just let's create simple constraints between the fk rig and
    # the round setup. Later, we could integrate an ik rig as well.
    const = cmds.parentConstraint(
        upper_ctl, upper_grp, name=upper_ctl + '_pac')[0]
    cmds.setAttr(const + '.ihi', 0)
    lock_and_hide_keyable_attrs(const)

    const = cmds.parentConstraint(
        lower_ctl, lower_grp, name=lower_ctl + '_pac')[0]
    cmds.setAttr(const + '.ihi', 0)
    lock_and_hide_keyable_attrs(const)

    const = cmds.parentConstraint(
        tip_ctl, tip_grp, name=tip_ctl + '_pac')[0]
    cmds.setAttr(const + '.ihi', 0)
    lock_and_hide_keyable_attrs(const)

    lock_and_hide_transform_attrs(upper_grp)
    lock_and_hide_transform_attrs(lower_grp)
    lock_and_hide_transform_attrs(tip_grp)

    # -------------------------------------------------------------------------
    # Setup the bezier curve rig.
    # -------------------------------------------------------------------------
    # Create the lower limb aim group. It is positioned at the lower limb and
    # will aim the vector that goes from the upper limb to the tip. It's like
    # the half vector of the lower limb rotation.

    # We create another tip group which has the same distance to the lower
    # group than the upper to the lower. This is necessary to make the round
    # setup work correctly.
    if round_setup:
        aim_tip_grp = cmds.createNode(
            'transform', name=tip_name + '_bezier_aim_grp', parent=lower_grp,
            skipSelect=True)
        aim_tip_pos = aim_vector * (pos2 - pos1).length()
        cmds.setAttr(aim_tip_grp + '.translate',
                     aim_tip_pos.x, aim_tip_pos.y, aim_tip_pos.z)
        cmds.setAttr(aim_tip_grp + '.ihi', 0)
        lock_and_hide_transform_attrs(aim_tip_grp)

        lower_bezier_grp = cmds.createNode(
            'transform', name=lower_name + '_bezier_grp', parent=main,
            skipSelect=True)
        const = cmds.pointConstraint(lower_grp, lower_bezier_grp,
                                     name=lower_bezier_grp + '_pc')[0]
        cmds.setAttr(const + '.ihi', 0)
        lock_and_hide_keyable_attrs(const)

        const = cmds.createNode('aimConstraint', name=lower_bezier_grp + '_ac',
                                parent=lower_bezier_grp, skipSelect=True)
        cmds.connectAttr(upper_grp + '.translate',
                         const + '.constraintTranslate')
        cmds.connectAttr(upper_grp + '.parentInverseMatrix[0]',
                         const + '.constraintParentInverseMatrix')
        cmds.connectAttr(upper_grp + '.rotatePivot',
                         const + '.constraintRotatePivot')
        cmds.connectAttr(upper_grp + '.rotatePivotTranslate',
                         const + '.constraintRotateTranslate')
        cmds.connectAttr(upper_grp + '.worldMatrix[0]',
                         const + '.worldUpMatrix')

        cmds.connectAttr(aim_tip_grp + '.translate',
                         const + '.target[0].targetTranslate')
        cmds.connectAttr(aim_tip_grp + '.parentMatrix[0]',
                         const + '.target[0].targetParentMatrix')
        cmds.connectAttr(aim_tip_grp + '.rotatePivot',
                         const + '.target[0].targetRotatePivot')
        cmds.connectAttr(aim_tip_grp + '.rotatePivotTranslate',
                         const + '.target[0].targetRotateTranslate')

        cmds.connectAttr(const + '.constraintRotate',
                         lower_bezier_grp + '.rotate')

        cmds.setAttr(const + '.aimVector',
                     aim_vector.x, aim_vector.y, aim_vector.z)
        cmds.setAttr(const + '.upVector',
                     up_vector.x, up_vector.y, up_vector.z)
        cmds.setAttr(const + '.worldUpVector',
                     up_vector.x, up_vector.y, up_vector.z)
        cmds.setAttr(const + '.worldUpType', 2)  # Object rotation up

        cmds.setAttr(const + '.ihi', 0)
        lock_and_hide_keyable_attrs(const)
        lock_and_hide_transform_attrs(lower_bezier_grp)

        # Create the mid limb aim groups. Those transforms are used to make the
        # handles follow the limb bending in rotation and "scale".
        upper_round_grp = cmds.createNode(
            'transform', name=upper_name + '_round_target_grp',
            parent=lower_bezier_grp, skipSelect=True)
        lower_round_grp = cmds.createNode(
            'transform', name=lower_name + '_round_target_grp',
            parent=lower_bezier_grp, skipSelect=True)

        upper_half_pos = aim_vector * upper_half_dist
        cmds.setAttr(upper_round_grp + '.translate',
                     upper_half_pos.x * side_mult,
                     upper_half_pos.y * side_mult,
                     upper_half_pos.z * side_mult)

        lower_half_pos = aim_vector * lower_half_dist
        cmds.setAttr(lower_round_grp + '.translate',
                     lower_half_pos.x * side_mult * -1,
                     lower_half_pos.y * side_mult * -1,
                     lower_half_pos.z * side_mult * -1)

        # Those groups are driven with an expression based on the half angle
        # between the upper and lower limb.

        # We need the vector between upper/lower limb and upper/tip limb.
        subtract_lower = cmds.createNode(
            'plusMinusAverage', name='{}_{}_{}_vec'.format(
                name, NAMES[limb_type]['upper'], NAMES[limb_type]['lower']),
            skipSelect=True)
        subtract_tip = cmds.createNode(
            'plusMinusAverage', name='{}_{}_{}_vec'.format(
                name, NAMES[limb_type]['lower'], NAMES[limb_type]['tip']),
            skipSelect=True)

        cmds.setAttr(subtract_lower + '.ihi', 0)
        cmds.setAttr(subtract_tip + '.ihi', 0)
        cmds.setAttr(subtract_lower + '.operation', 2)  # Subtract
        cmds.setAttr(subtract_tip + '.operation', 2)  # Subtract

        cmds.connectAttr(lower_matrix + '.outputTranslate',
                         subtract_lower + '.input3D[0]')
        cmds.connectAttr(upper_matrix + '.outputTranslate',
                         subtract_lower + '.input3D[1]')
        cmds.connectAttr(tip_matrix + '.outputTranslate',
                         subtract_tip + '.input3D[0]')
        cmds.connectAttr(lower_matrix + '.outputTranslate',
                         subtract_tip + '.input3D[1]')

        lock_and_hide_keyable_attrs(upper_matrix)
        lock_and_hide_keyable_attrs(lower_matrix)
        lock_and_hide_keyable_attrs(tip_matrix)

        angle_between = cmds.createNode(
            'angleBetween', name=name + '_round_half_angle', skipSelect=True)

        cmds.setAttr(angle_between + '.ihi', 0)

        cmds.connectAttr(subtract_lower + '.output3D',
                         angle_between + '.vector1')
        cmds.connectAttr(subtract_tip + '.output3D',
                         angle_between + '.vector2')

        lock_and_hide_keyable_attrs(subtract_lower)
        lock_and_hide_keyable_attrs(subtract_tip)

        expr = cmds.expression(name=name + '_round_expr', string=EXPR.format(
            angle_between=angle_between, upper_round_grp=upper_round_grp,
            lower_round_grp=lower_round_grp,
            upper_half_distance=upper_half_dist * -1,
            lower_half_distance=lower_half_dist,
            aim_x=aim_vector.x, aim_y=aim_vector.y, aim_z=aim_vector.z))

        cmds.setAttr(expr + '.ihi', 0)
        lock_and_hide_keyable_attrs(upper_round_grp)
        lock_and_hide_keyable_attrs(lower_round_grp)

        # Create the corresponding groups that allow to blend to the straight
        # limb.
        upper_offset_ctl = _setup_offset_controller(
            limb_ctl, upper_name, upper_grp, upper_round_grp, upper_half_pos)
        lower_offset_ctl = _setup_offset_controller(
            limb_ctl, lower_name, lower_grp, lower_round_grp, lower_half_pos)

        ctl_shapes.extend(cmds.listRelatives(upper_offset_ctl, shapes=True))
        ctl_shapes.extend(cmds.listRelatives(lower_offset_ctl, shapes=True))

        # ---------------------------------------------------------------------
        # The bezier handle setup for our curve.
        # ---------------------------------------------------------------------
        # We create 7 handles, 3 of them are the limb groups, the rest are the
        # handles next to them that will get setuped now.
        handles = [upper_grp, None, None, lower_grp, None, None, tip_grp]

        # Upper and lower handles.
        upper_handle = _create_border_handle(
            upper_name, upper_grp, aim_vector, up_vector, upper_offset_ctl)
        tip_handle = _create_border_handle(
            tip_name, tip_grp, aim_vector * -1, up_vector, lower_offset_ctl)
        handles[1] = upper_handle
        handles[5] = tip_handle

        # Upper and lower global scale setup.
        _setup_handle_global_scale(
            main, upper_name, upper_grp, upper_offset_ctl, upper_handle,
            aim_vector, reverse=False)
        _setup_handle_global_scale(
            main, lower_name, tip_grp, lower_offset_ctl, tip_handle,
            aim_vector, reverse=True)

        lock_and_hide_transform_attrs(upper_handle)
        lock_and_hide_transform_attrs(tip_handle)

        # The missing two handles in the middle. This has a special setup to
        # handle round and straight situation correctly.
        lower_handle_hook = cmds.createNode(
            'transform', name=lower_name + '_bezier_handle_hook', parent=main,
            skipSelect=True)
        cmds.connectAttr(lower_bezier_grp + '.translate',
                         lower_handle_hook + '.translate')

        const = cmds.createNode(
            'aimConstraint', name=lower_handle_hook + '_ac',
            parent=lower_handle_hook, skipSelect=True)
        cmds.setAttr(const + '.ihi', 0)
        cmds.setAttr(const + '.aimVector',
                     aim_vector.x, aim_vector.y, aim_vector.z)
        cmds.setAttr(const + '.upVector',
                     up_vector.x, up_vector.y, up_vector.z)
        cmds.setAttr(const + '.worldUpVector',
                     up_vector.x, up_vector.y, up_vector.z)
        cmds.setAttr(const + '.worldUpType', 2)  # Object rotation up

        cmds.connectAttr(lower_bezier_grp + '.worldMatrix[0]',
                         const + '.worldUpMatrix')
        cmds.connectAttr(const + '.constraintRotate',
                         lower_handle_hook + '.rotate')

        # Create the aim vector out of the two offset controllers.
        lower_offset_matrix = cmds.createNode(
            'decomposeMatrix', name=lower_offset_ctl + '_matrix',
            skipSelect=True)
        upper_offset_matrix = cmds.createNode(
            'decomposeMatrix', name=upper_offset_ctl + '_matrix',
            skipSelect=True)

        cmds.setAttr(lower_offset_matrix + '.ihi', 0)
        cmds.setAttr(upper_offset_matrix + '.ihi', 0)

        cmds.connectAttr(lower_offset_ctl + '.worldMatrix[0]',
                         lower_offset_matrix + '.inputMatrix')
        cmds.connectAttr(upper_offset_ctl + '.worldMatrix[0]',
                         upper_offset_matrix + '.inputMatrix')

        subtract_offset_pos = cmds.createNode(
            'plusMinusAverage', name=name + '_offset_ctl_subtract',
            skipSelect=True)
        cmds.setAttr(subtract_offset_pos + '.ihi', 0)
        cmds.setAttr(subtract_offset_pos + '.operation', 2)  # Subtract
        cmds.connectAttr(lower_offset_matrix + '.outputTranslate',
                         subtract_offset_pos + '.input3D[0]')
        cmds.connectAttr(upper_offset_matrix + '.outputTranslate',
                         subtract_offset_pos + '.input3D[1]')
        cmds.connectAttr(subtract_offset_pos + '.output3D',
                         const + '.target[0].targetTranslate')

        lock_and_hide_keyable_attrs(const)
        lock_and_hide_keyable_attrs(lower_offset_matrix)
        lock_and_hide_keyable_attrs(upper_offset_matrix)
        lock_and_hide_keyable_attrs(subtract_offset_pos)
        lock_and_hide_transform_attrs(lower_handle_hook)

        # Now, we add the hooks for the upper and lower handle of the lower
        # limb. This will handle the straight setup when round gets deactivated
        # and enables "breaking" the tangent at the lower limb.
        handles[2] = _setup_lower_limb_handles(
            lower_name + '_upper', limb_ctl, main, aim_vector * -1, up_vector,
            upper_offset_ctl, lower_handle_hook, reverse=False)
        handles[4] = _setup_lower_limb_handles(
            lower_name + '_lower', limb_ctl, main, aim_vector, up_vector,
            lower_offset_ctl, lower_handle_hook, reverse=False)

        # ---------------------------------------------------------------------
        # Curve setup
        # ---------------------------------------------------------------------
        # We create a bogus curve, just to be able to connect our handles to
        # its control points.
        curve_trs = cmds.curve(
            degree=3, point=[[0, 0, 0]]*7,
            knot=[0, 0, 0, 1, 1, 1, 2, 2, 2], bezier=True)
        curve_trs = cmds.rename(curve_trs, name + '_round_curve')
        curve = cmds.listRelatives(curve_trs, shapes=True)[0]

        cmds.setAttr(curve_trs + '.visibility', False)
        cmds.setAttr(curve_trs + '.ihi', 0)
        cmds.setAttr(curve + '.ihi', 0)

        for i, handle in enumerate(handles):
            world_matrix = cmds.listConnections(handle, type='decomposeMatrix')
            if not world_matrix:
                world_matrix = cmds.createNode(
                    'decomposeMatrix', name=handle + '_matrix',
                    skipSelect=True)
                cmds.connectAttr(handle + '.worldMatrix[0]',
                                 world_matrix + '.inputMatrix')
                lock_and_hide_keyable_attrs(world_matrix)
            else:
                world_matrix = world_matrix[0]
            cmds.connectAttr(world_matrix + '.outputTranslate',
                             curve + '.controlPoints[{}]'.format(i))

        curve_trs = cmds.parent(curve_trs, clean)[0]
        lock_and_hide_keyable_attrs(curve_trs)

    # -------------------------------------------------------------------------
    # Setup twist groups
    # -------------------------------------------------------------------------
    upper_limb_twist_jnt = cmds.createNode(
        'joint', name=upper_name + '_twist_jnt', parent=upper_grp,
        skipSelect=True)
    upper_limb_twist_tip = cmds.createNode(
        'joint', name=upper_name + '_twistTip_jnt',
        parent=upper_limb_twist_jnt, skipSelect=True)
    cmds.setAttr(upper_limb_twist_jnt + '.translate',
                 aim_vector.x * upper_half_dist * 2,
                 aim_vector.y * upper_half_dist * 2,
                 aim_vector.z * upper_half_dist * 2)
    cmds.setAttr(upper_limb_twist_tip + '.translate',
                 aim_vector.x * upper_half_dist * -2,
                 aim_vector.y * upper_half_dist * -2,
                 aim_vector.z * upper_half_dist * -2)
    upper_twist_ik, upper_twist_ee = cmds.ikHandle(
        startJoint=upper_limb_twist_jnt, endEffector=upper_limb_twist_tip,
        name=upper_name + '_twist_ikHandle', solver='ikSCsolver')
    upper_twist_ee = cmds.rename(
        upper_twist_ee, upper_name + '_twist_effector')

    lower_limb_twist_jnt = cmds.createNode(
        'joint', name=lower_name + '_twist_jnt', parent=lower_grp,
        skipSelect=True)
    lower_limb_twist_tip = cmds.createNode(
        'joint', name=lower_name + '_twistTip_jnt',
        parent=lower_limb_twist_jnt, skipSelect=True)
    cmds.setAttr(lower_limb_twist_jnt + '.translate',
                 aim_vector.x * lower_half_dist * 2,
                 aim_vector.y * lower_half_dist * 2,
                 aim_vector.z * lower_half_dist * 2)
    cmds.setAttr(lower_limb_twist_tip + '.translate',
                 aim_vector.x * -1, aim_vector.y * -1, aim_vector.z * -1)
    lower_twist_ik, lower_twist_ee = cmds.ikHandle(
        startJoint=lower_limb_twist_jnt, endEffector=lower_limb_twist_tip,
        name=lower_name + '_twist_ikHandle', solver='ikSCsolver')
    lower_twist_ee = cmds.rename(
        lower_twist_ee, lower_name + '_twist_effector')

    upper_twist_ik = cmds.parent(upper_twist_ik, main)[0]
    lower_twist_ik = cmds.parent(lower_twist_ik, lower_grp)[0]
    const = cmds.orientConstraint(tip_grp, lower_twist_ik,
                                  name=lower_twist_ik + '_oc')[0]
    cmds.setAttr(const + '.ihi', 0)
    lock_and_hide_keyable_attrs(const)

    cmds.setAttr(upper_twist_ik + '.visibility', False)
    cmds.setAttr(lower_twist_ik + '.visibility', False)
    cmds.setAttr(upper_limb_twist_jnt + '.drawStyle', 2)  # None
    cmds.setAttr(lower_limb_twist_jnt + '.drawStyle', 2)  # None
    cmds.setAttr(upper_limb_twist_tip + '.drawStyle', 2)  # None
    cmds.setAttr(lower_limb_twist_tip + '.drawStyle', 2)  # None

    # -------------------------------------------------------------------------
    # Add skin joints
    # -------------------------------------------------------------------------
    num_of_skin_joints = 3 + 2 * number_of_between_joints
    skin_joint_grp = cmds.createNode('transform', name=name + '_skin_jnt_grp',
                                     parent=clean, skipSelect=True)
    lock_and_hide_transform_attrs(skin_joint_grp)

    for i in range(num_of_skin_joints):
        jnt_name = name + '_skin_{:02d}'.format(i + 1)
        hook = cmds.createNode('transform', name=jnt_name + '_hook',
                               parent=skin_joint_grp, skipSelect=True)
        joint = cmds.createNode('joint', name=jnt_name + '_jnt', parent=hook,
                                skipSelect=True)

        if number_of_between_joints:
            if ((i * 2) + 1) / num_of_skin_joints:
                upper_parent = lower_grp
                lower_parent = tip_grp
                upper_up_vector = lower_up_vector
                lower_up_vector = tip_up_vector
                weight = (float(i - number_of_between_joints - 1) /
                          float(number_of_between_joints + 1))
            else:
                upper_parent = upper_grp
                lower_parent = lower_grp
                upper_up_vector = upper_up_vector
                lower_up_vector = lower_up_vector
                weight = float(i) / float(number_of_between_joints + 1)

            # Aim constraint for the twist.
            const = cmds.createNode('aimConstraint', name=jnt_name + '_ac',
                                    parent=hook, skipSelect=True)
            cmds.setAttr(const + '.aimVector',
                         aim_vector.x, aim_vector.y, aim_vector.z)
            cmds.setAttr(const + '.upVector',
                         up_vector.x, up_vector.y, up_vector.z)
            cmds.setAttr(const + '.worldUpType', 3)  # Vector
            cmds.connectAttr(const + '.constraintRotate',
                             hook + '.rotate')

            blend = cmds.createNode(
                'blendColors', name=jnt_name + '_blendUpVector')
            cmds.connectAttr(lower_up_vector + '.output', blend + '.color1')
            cmds.connectAttr(upper_up_vector + '.output', blend + '.color2')
            cmds.connectAttr(blend + '.output', const + '.worldUpVector')

            cmds.setAttr(blend + '.blender', weight)

            cmds.setAttr(const + '.ihi', 0)
            cmds.setAttr(blend + '.ihi', 0)
            lock_and_hide_keyable_attrs(blend)
            lock_and_hide_keyable_attrs(const)

        if round_setup:
            poci = cmds.createNode('pointOnCurveInfo', name=jnt_name + '_poci',
                                   skipSelect=True)
            cmds.setAttr(poci + '.ihi', 0)
            cmds.setAttr(poci + '.turnOnPercentage', True)
            cmds.setAttr(poci + '.parameter',
                         float(i) / float(num_of_skin_joints - 1))

            cmds.connectAttr(curve + '.worldSpace', poci + '.inputCurve')
            cmds.connectAttr(poci + '.position', hook + '.translate')
            cmds.connectAttr(poci + '.tangent',
                             const + '.target[0].targetTranslate')

            lock_and_hide_keyable_attrs(poci)
            lock_and_hide_keyable_attrs(const)
        else:
            # Without curve, we have to use constraints.
            const = cmds.pointConstraint(upper_parent, lower_parent, hook)
            cmds.pointConstraint(upper_parent, hook, edit=True,
                                 weight=1 - weight)
            cmds.pointConstraint(lower_parent, hook, edit=True,
                                 weight=weight)

        lock_and_hide_keyable_attrs(hook)
        lock_and_hide_keyable_attrs(joint)

    # -------------------------------------------------------------------------
    # Clean up
    # -------------------------------------------------------------------------
    for shape in ctl_shapes:
        cmds.setAttr(shape + '.overrideEnabled', True)
        cmds.setAttr(shape + '.overrideColor', CTL_COLOR[side])
