# -*- coding: utf-8 -*-

import maya.api.OpenMaya as om2
import maya.cmds as cmds

from cgiesa_tools.maya.rigging.config import SIDE, SIDE_PREFIX, CTL_COLOR
from cgiesa_tools.maya.rigging.controller import create_ctl, CTL_TYPE, \
  create_on_transform
from cgiesa_tools.maya.rigging.transform import add_hook, \
  lock_and_hide_transform_attrs
from cgiesa_tools.maya.rigging.node import lock_and_hide_keyable_attrs
from cgiesa_tools.maya.rigging.maths import get_ref_matrix

AXISES = ['X', 'Y', 'Z']
SLIDE_ATTR = 'slide'
SHOW_XTRA_ATTR = 'showExtraCtls'


def _create_ctl(prefix, parent, mat):
    '''
    Little helper function to create the main controllers of the fk-chain-rig.
    '''
    hook = cmds.createNode('joint', name='{}_hook'.format(prefix),
                           skipSelect=True)
    ctl = cmds.createNode(
        'joint', name='{}_ctl'.format(prefix),
        skipSelect=True, parent=hook)

    create_on_transform(CTL_TYPE.CIRCLE, ctl)

    hook_trs = om2.MFnTransform(
        om2.MSelectionList().add(hook).getDagPath(0))
    hook_trs.setTransformation(om2.MTransformationMatrix(mat))

    cmds.parent(hook, parent)
    lock_and_hide_transform_attrs(hook)

    cmds.setAttr(hook + '.radius', keyable=False, channelBox=False)
    cmds.setAttr(ctl + '.radius', keyable=False, channelBox=False)
    cmds.setAttr(ctl + '.visibility', keyable=False, channelBox=False)
    cmds.setAttr(hook + '.drawStyle', 2)  # Don't draw the bone.
    cmds.setAttr(ctl + '.drawStyle', 2)  # Don't draw the bone.

    return ctl


def create(name, side, first_level_pos_list, second_level_ctl_num,
           aim_axis=[1, 0, 0], up_axis=[0, 1, 0], world_up_axis=[0, 1, 0]):
    '''
    Creates a fk chain setup with two levels of controls. The first level is a
    standard fk joint chain with a lower amount of controllers. The second
    level consists of the skin joints.

    Twist and scale is interpolated between the first level controllers. A
    slide attribute allows to make the fk chain grow.

    TODO:
    - add option to have better control of rotation on the last first level
      controller. This can be done by adding another control point on the
      linear curve that gets driven by the matrix of this last controller.
    - add label and side on joints for easier mirror skin
    '''
    # Create first the ref matrix that will be used to orient the correct
    # axises.
    aim_vec = om2.MVector(*aim_axis)
    up_vec = om2.MVector(*up_axis)
    world_up_vec = om2.MVector(*world_up_axis)

    if side == SIDE.RIGHT:
        aim_vec *= -1
        up_vec *= -1

    aim_vec.normalize()
    up_vec.normalize()

    ref_matrix_inv = get_ref_matrix(aim_vec, up_vec)

    # Create main groups.
    ctl_shapes = []
    name = '{}_{}'.format(SIDE_PREFIX[side], name)
    root = cmds.createNode('transform', name=name + '_root_grp',
                           skipSelect=True)
    clean = cmds.createNode('transform', name=name + '_clean_grp', parent=root,
                            skipSelect=True)
    main_ctl = create_ctl(CTL_TYPE.STARY, name + '_main_ctl', parent=root)

    lock_and_hide_transform_attrs(root)
    lock_and_hide_transform_attrs(clean)

    cmds.addAttr(main_ctl, longName=SLIDE_ATTR, min=0, max=1, defaultValue=1,
                 keyable=True)
    cmds.addAttr(main_ctl, longName=SHOW_XTRA_ATTR, attributeType='short',
                 keyable=True, min=0, max=1)
    cmds.setAttr(main_ctl + '.visibility', keyable=False, channelBox=False)

    ctl_shapes.extend(cmds.listRelatives(main_ctl, shapes=True))

    # Compute the matrices of the first level controllers. The second level
    # controllers will adapt accordingly.
    ctls = []
    parent = main_ctl
    for i in range(len(first_level_pos_list) - 1):
        pos = om2.MPoint(*first_level_pos_list[i])
        next_pos = om2.MPoint(first_level_pos_list[i + 1])

        aim = (next_pos - pos).normalize()
        sec_up = aim ^ world_up_vec
        sec_up.normalize()
        up = sec_up ^ aim

        mat = om2.MMatrix(
            [aim.x, aim.y, aim.z, 0.0,
             up.x, up.y, up.z, 0.0,
             sec_up.x, sec_up.y, sec_up.z, 0.0,
             0.0, 0.0, 0.0, 1.0])
        mat = ref_matrix_inv * mat
        mat[12] = pos.x
        mat[13] = pos.y
        mat[14] = pos.z

        # We apply the same transformation to the main controller.
        if parent == main_ctl:
            ctl_trs = om2.MFnTransform(
                om2.MSelectionList().add(main_ctl).getDagPath(0))
            ctl_trs.setTransformation(om2.MTransformationMatrix(mat))

        ctl = _create_ctl('{}_{:02d}'.format(name, i + 1), parent, mat)

        ctls.append(ctl)
        parent = ctl

    # We add the hook now, since the main controller has been moved just
    # before.
    add_hook(main_ctl)

    # The last controller has the same orientation than its parent controller.
    # We just change the position.
    tip_pos = om2.MPoint(*first_level_pos_list[-1])
    mat[12] = tip_pos.x
    mat[13] = tip_pos.y
    mat[14] = tip_pos.z

    ctls.append(_create_ctl('{}_tip'.format(name), parent, mat))

    for ctl in ctls:
        ctl_shapes.extend(cmds.listRelatives(ctl, shapes=True))

    # Create the up vector nodes for each control that will be used for the
    # aim constraint.
    up_vec_nodes = []
    for i, ctl in enumerate(ctls):
        # Create the up vector mult node for each ctl.
        up_vec_prod = cmds.createNode(
            'vectorProduct', name=name + '_ctl_{:02d}_upVec'.format(i + 1),
            skipSelect=True)
        cmds.setAttr(up_vec_prod + '.ihi', 0)
        cmds.setAttr(up_vec_prod + '.operation', 3)  # Vector-matrix-mult
        cmds.setAttr(up_vec_prod + '.input1', up_vec.x, up_vec.y, up_vec.z)

        # To have the correct twist, the first up vector is actually not the
        # first controller, but the main controller.
        if not i:
            cmds.connectAttr(
                main_ctl + '.worldMatrix[0]', up_vec_prod + '.matrix')
        else:
            cmds.connectAttr(ctl + '.worldMatrix[0]', up_vec_prod + '.matrix')

        lock_and_hide_keyable_attrs(up_vec_prod)
        up_vec_nodes.append(up_vec_prod)

    # We create the linear curve that will be used to place the secondary level
    # joints.
    curve = cmds.curve(degree=1, point=[[0, 0, 0]] * len(first_level_pos_list))
    for i, ctl in enumerate(ctls):
        decompose = cmds.createNode('decomposeMatrix', name=ctl + '_worldPos',
                                    skipSelect=True)
        cmds.setAttr(decompose + '.ihi', 0)
        cmds.connectAttr(ctl + '.worldMatrix[0]', decompose + '.inputMatrix')
        cmds.connectAttr(decompose + '.outputTranslate',
                         curve + '.controlPoints[{}]'.format(i))

    curve = cmds.rename(curve, name + '_fk_curve')
    curve = cmds.parent(curve, clean)[0]

    cmds.setAttr(curve + '.visibility', False)
    lock_and_hide_transform_attrs(curve)
    curve_shape = cmds.listRelatives(curve, shapes=True)[0]

    rebuild_crv = cmds.createNode('rebuildCurve', name=name + '_rebuild_crv',
                                  skipSelect=True)
    cmds.setAttr(rebuild_crv + '.spans', len(ctls))

    # This value could be adjusted afterwards, depending of the needs, but
    # should NOT be touched after the skinning since this will move the skin
    # joints.
    cmds.setAttr(rebuild_crv + '.smooth', 4.5)
    cmds.setAttr(rebuild_crv + '.keepRange', 2)  # To number of spans.
    cmds.setAttr(rebuild_crv + '.ihi', 0)
    cmds.connectAttr(curve_shape + '.worldSpace', rebuild_crv + '.inputCurve')

    # Create the skin joints.
    skin_jnt_grp = cmds.createNode('transform', name=name + '_skin_jnt_grp',
                                   parent=clean, skipSelect=True)
    lock_and_hide_transform_attrs(skin_jnt_grp)

    j = 1
    for i in range(second_level_ctl_num):
        jnt = cmds.createNode(
            'joint', name='{}_{:02d}_skin_jnt'.format(name, i + 1),
            parent=skin_jnt_grp, skipSelect=True)

        cmds.setAttr(jnt + '.radius', keyable=False, channelBox=False)
        cmds.setAttr(jnt + '.visibility', keyable=False, channelBox=False)
        cmds.setAttr(jnt + '.drawStyle', 2)  # Don't draw the bone.
        create_on_transform(CTL_TYPE.BALL, jnt)
        for shape in cmds.listRelatives(jnt, shapes=True):
            cmds.connectAttr(main_ctl + '.' + SHOW_XTRA_ATTR,
                             shape + '.visibility')

        hook = add_hook(jnt)
        scale_hook = add_hook(jnt)
        scale_hook = cmds.rename(
            scale_hook, name + '_{:02d}_skin_scaleHook'.format(i + 1))
        jnt = cmds.listRelatives(scale_hook)[0]

        ctl_shapes.extend(cmds.listRelatives(jnt, shapes=True))

        # TODO: This is a short cut and won't work with non-uniform scale. But
        # this should do it for now.
        cmds.connectAttr(main_ctl + '.scale', scale_hook + '.scale')
        lock_and_hide_transform_attrs(scale_hook)

        poci = cmds.createNode('pointOnCurveInfo', name=jnt + '_poci',
                               skipSelect=True)
        cmds.setAttr(poci + '.ihi', 0)
        cmds.connectAttr(rebuild_crv + '.outputCurve', poci + '.inputCurve')
        cmds.connectAttr(poci + '.position', hook + '.translate')

        mod = (i - 1) % 3
        if mod == 0:
            param_mult = cmds.createNode(
                'multiplyDivide', name='{}_paramMult_{:02d}'.format(name, j),
                skipSelect=True)
            cmds.setAttr(param_mult + '.ihi', 0)
            j += 1
        if i:
            axis = AXISES[mod]
            cmds.setAttr(
                param_mult + '.input2{}'.format(axis),
                (float(i) / float(second_level_ctl_num - 1)) * float(len(ctls))
                )
            cmds.connectAttr(main_ctl + '.' + SLIDE_ATTR,
                             param_mult + '.input1{}'.format(axis))
            cmds.connectAttr(param_mult + '.output{}'.format(axis),
                             poci + '.parameter')

        # Aim constraint setup.
        const = cmds.createNode(
            'aimConstraint', name=hook.split('|')[-1] + '_ac', parent=hook,
            skipSelect=True)
        cmds.setAttr(const + '.ihi', 0)
        cmds.setAttr(const + '.aimVector', aim_vec.x, aim_vec.y, aim_vec.z)
        cmds.setAttr(const + '.upVector', up_vec.x, up_vec.y, up_vec.z)

        cmds.connectAttr(poci + '.tangent',
                         const + '.target[0].targetTranslate')

        up_vec_ramp = cmds.createNode(
            'remapValue', name=name + '_{:02d}_upVec_ramp'.format(i + 1),
            skipSelect=True)
        cmds.setAttr(up_vec_ramp + '.ihi', 0)
        cmds.setAttr(up_vec_ramp + '.inputMax', len(ctls))

        for k, up_vec_node in enumerate(up_vec_nodes):
            cmds.setAttr(up_vec_ramp + '.color[{}].color_Position'.format(k),
                         float(k) / float(len(up_vec_nodes) - 1))
            # Set to smooth interpolation.
            cmds.setAttr(up_vec_ramp + '.color[{}].color_Interp'.format(k), 1)
            cmds.connectAttr(up_vec_node + '.output',
                             up_vec_ramp + '.color[{}].color_Color'.format(k))

        cmds.connectAttr(poci + '.parameter', up_vec_ramp + '.inputValue')
        cmds.connectAttr(up_vec_ramp + '.outColor', const + '.worldUpVector')

        cmds.connectAttr(const + '.constraintRotate', hook + '.rotate')

        # Scale setup.
        scale_ramp = cmds.createNode(
            'remapValue', name=name + '_{:02d}_scale_ramp'.format(i + 1))
        cmds.setAttr(scale_ramp + '.ihi', 0)
        cmds.setAttr(scale_ramp + '.inputMax', len(ctls))

        for k, ctl in enumerate(ctls):
            cmds.setAttr(scale_ramp + '.color[{}].color_Position'.format(k),
                         float(k) / float(len(up_vec_nodes) - 1))
            # Set to smooth interpolation.
            cmds.setAttr(scale_ramp + '.color[{}].color_Interp'.format(k), 1)
            cmds.connectAttr(ctl + '.scale',
                             scale_ramp + '.color[{}].color_Color'.format(k))

        cmds.connectAttr(poci + '.parameter', scale_ramp + '.inputValue')
        cmds.connectAttr(scale_ramp + '.outColor', hook + '.scale')

        # Clean up.
        lock_and_hide_transform_attrs(hook)
        lock_and_hide_keyable_attrs(const)

    for shape in ctl_shapes:
        cmds.setAttr(shape + '.overrideEnabled', True)
        cmds.setAttr(shape + '.overrideColor', CTL_COLOR[side])
