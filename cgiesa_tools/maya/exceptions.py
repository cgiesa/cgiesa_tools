# -*- coding: utf-8 -*-

# A series of standard exceptions are defined here that are used throughout
# the scripts of the cgiesa_tools.maya package. You should always check
# against the provided exceptions to be sure to not miss any unexpected
# exception.

# Every package will have its own exceptions.


class NodeDoesNotExistError(Exception):
    pass


class AttributeDoesNotExistError(Exception):
    pass


class ClashingNameError(Exception):
    pass


class NodeTypeError(Exception):
    pass


class SelectionError(Exception):
    pass
