import random

import maya.cmds as cmds


def set_hair_strands_on_ramp(ramp_node, total_count, thickness,
                             random_amount=0.0):
    '''
    Creates color entries to a ramp node that looks like hair strands.
    '''
    # First, we clean any existing color entries.
    cmds.removeMultiInstance(ramp_node + '.colorEntryList', b=True,
                             allChildren=True)

    # We start with a black color at the beginning.
    cmds.setAttr(ramp_node + '.colorEntryList[0].position', 0.0)
    cmds.setAttr(ramp_node + '.colorEntryList[0].color',
                 0, 0, 0, type='double3')

    # Now, we put the strands.
    last_position = 0.0
    for i in range(1, total_count + 1, 2):
        position = float(i + 1) / float(total_count + 1)

        if random_amount > 0.0:
            rand = random.uniform(-1, 1) * random_amount
            if rand < 0.0:
                position = position + rand * (position - last_position)
            else:
                position = min(position + rand * (
                    float(i + 3) / float(total_count + 1) - position), 1.0)
            last_postion = position

        cmds.setAttr(ramp_node + '.colorEntryList[{}].position'.format(i),
                     position)
        cmds.setAttr(ramp_node + '.colorEntryList[{}].color'.format(i),
                     1, 1, 1, type='double3')
        cmds.setAttr(ramp_node + '.colorEntryList[{}].position'.format(i + 1),
                     position + thickness)
        cmds.setAttr(ramp_node + '.colorEntryList[{}].color'.format(i + 1),
                     0, 0, 0, type='double3')
