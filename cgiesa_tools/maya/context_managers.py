# -*- coding: utf-8 -*-

from contextlib import contextmanager

import maya.cmds as cmds

from cgiesa_tools.maya.config import MODEL_EDITOR_DISPLAY_LIST


@contextmanager
def open_undo_chunk():
    '''
    This makes sure that all executed code inside this context will be inside
    one single undo chunk. This works around an issue when calling Python code
    from a Qt interface. By default, Maya stores every single action in one
    chunk.
    '''
    cmds.undoInfo(openChunk=True)

    try:
        yield
    except Exception, e:
        raise e
    finally:
        cmds.undoInfo(closeChunk=True)


@contextmanager
def isolate_model_views():
    '''
    This will temporally hide all objects in all model panels. This is
    useful for faster scene evaluation for baking or exports.
    '''
    undo_dic = {}
    for panel in cmds.getPanel(type='modelPanel'):
        undo_dic[panel] = {}
        for obj in MODEL_EDITOR_DISPLAY_LIST:
            args = {
                'query': True,
                obj: True}
            undo_dic[panel][obj] = cmds.modelEditor(panel, **args)
            args = {
                'edit': True,
                obj: False}
            cmds.modelEditor(panel, **args)

    try:
        yield
    except Exception, e:
        raise e
    finally:
        for panel in undo_dic.keys():
            for obj in undo_dic[panel].keys():
                args = {
                    'edit': True,
                    obj: undo_dic[panel][obj]}
                cmds.modelEditor(panel, **args)
