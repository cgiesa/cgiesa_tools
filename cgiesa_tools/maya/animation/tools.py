import maya.cmds as cmds

from cgiesa_tools.maya.exceptions import NodeDoesNotExistError, \
  ClashingNameError, AttributeDoesNotExistError


def add_inbetween_key(node, attrs, bias, frame):
    '''
    This will add a new keyframe at the given frame, for the given attributes
    and for the given node. There must be existing keyframes to make this
    happen and there should be at least one keyframe before and after the
    given frame. Otherwise the methods skips the attribute.

    The bias defines how close the new value should be to the key before and
    after the given frame. 0.0 would mean that we copy the same value than the
    keyframe before, 1.0 the same than the keyframe after. All values inbetween
    correspond to a linear interpolation.

    @param node: string, node name

    @param attrs: string[], list of attribute names

    @param bias: float (from 0.0 to 1.0)

    @param frame: float

    @return boolean (True if success)
    '''
    node = cmds.ls(node)
    if not node:
        raise NodeDoesNotExistError(node)
    if len(node) > 1:
        raise ClashingNameError(node)
    node = node[0]

    for attr in attrs:
        plug = node + '.' + attr
        if not cmds.objExists(plug):
            AttributeDoesNotExistError(plug)

    if bias < 0.0 or bias > 1.0:
        raise ValueError('The given bias must be between 0.0 and 1.0.')

    # Go through each plug, check for existing keyframes and compute the in-
    # between keyframe. If keyframes before or after are missing a warning
    # is printed and the attribute is ignored.
    for attr in attrs:
        fr1 = cmds.findKeyframe(node, attribute=attr, time=(frame, frame),
                                which='previous')
        fr2 = cmds.findKeyframe(node, attribute=attr, time=(frame, frame),
                                which='next')
        plug = node + '.' + attr
        if not fr1 < frame:
            cmds.warning('There is not keyframe before {} for plug {}. '
                         'Inbetween frame skipped.'.format(frame, plug))
            continue
        if not fr2 > frame:
            cmds.warning('There is not keyframe after {} for plug {}. '
                         'Inbetween frame skipped.'.format(frame, plug))
            continue

        val1 = cmds.keyframe(node, attribute=attr, time=(fr1, fr1), query=True,
                             valueChange=True)[0]
        val2 = cmds.keyframe(node, attribute=attr, time=(fr2, fr2), query=True,
                             valueChange=True)[0]

        new_val = val1 * (1 - bias) + val2 * bias
        cmds.setKeyframe(node, attribute=attr, time=frame, value=new_val)
