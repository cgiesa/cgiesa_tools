# stdlib modules
from __future__ import absolute_import

# third party modules
from PySide2 import QtWidgets, QtCore, QtGui
from maya import cmds

# tool modules
from cgiesa_tools.maya.ui.qt.decorator import undoable
from cgiesa_tools.maya.rigging import controller

# for backwards compatibility
if not hasattr(QtCore, "QStringListModel"):
    QtCore.QStringListModel = QtGui.QStringListModel


class ControllerWidget(QtWidgets.QWidget):

    def __init__(self, *args, **kwargs):
        super(ControllerWidget, self).__init__(*args, **kwargs)

        self._controllers = None

        self._build_ui()
        self._initialize_widgets()
        self._connect_signals()

    def _build_ui(self):
        """Builds the widget's UI."""
        self._controller_group = QtWidgets.QGroupBox("Controller", self)

        self._type_combobox = QtWidgets.QComboBox(self._controller_group)
        self._name_lineedit = QtWidgets.QLineEdit(self._controller_group)
        self._size_spinbox = QtWidgets.QDoubleSpinBox(self._controller_group)
        self._add_hook_checkbox = QtWidgets.QCheckBox(self._controller_group)

        self._add_new_button = QtWidgets.QPushButton("Add New")
        self._add_on_trs_button = QtWidgets.QPushButton("Add on Transform")
        self._add_below_button = QtWidgets.QPushButton("Add below Selection")

        self._group_layout = QtWidgets.QFormLayout(self._controller_group)

        self._group_layout.addRow("Type:", self._type_combobox)
        self._group_layout.addRow("Name:", self._name_lineedit)
        self._group_layout.addRow("Size:", self._size_spinbox)
        self._group_layout.addRow("Add Hook:", self._add_hook_checkbox)
        self._group_layout.addRow(self._add_new_button)
        self._group_layout.addRow(self._add_on_trs_button)
        self._group_layout.addRow(self._add_below_button)

        self._layout = QtWidgets.QVBoxLayout(self, spacing=4)
        self._layout.setContentsMargins(4, 4, 4, 4)
        self._layout.addWidget(self._controller_group)

    def _initialize_widgets(self):
        """Sets initial values on widgets."""
        self._controllers = {v["display_name"]: k for (k, v) in controller.CONTROLLERS.items()}
        controller_names = self._controllers.keys()
        controller_names.sort()

        self._type_combobox.setModel(QtCore.QStringListModel(controller_names))

        self._name_lineedit.setText("controller")

        self._size_spinbox.setMinimum(0.001)
        self._size_spinbox.setMaximum(100)
        self._size_spinbox.setValue(1.0)

        self._add_hook_checkbox.setChecked(False)

    def _connect_signals(self):
        """Connects all signals of this widget."""
        self._add_new_button.released.connect(self._add_new_released)
        self._add_below_button.released.connect(self._add_below_released)
        self._add_on_trs_button.released.connect(self._add_on_trs_released)

    @undoable
    def _add_new_released(self):
        """Slot called by the add new button."""
        controller_name = self._type_combobox.currentText()
        name = self._name_lineedit.text()
        size = self._size_spinbox.value()
        add_hook = self._add_hook_checkbox.isChecked()

        controller_type = self._controllers[controller_name]

        controller.create_ctl(controller_type, name, size, parent=None, add_hook=add_hook)

    @undoable
    def _add_below_released(self):
        """Slot called by the add below button."""
        controller_name = self._type_combobox.currentText()
        name = self._name_lineedit.text()
        size = self._size_spinbox.value()
        add_hook = self._add_hook_checkbox.isChecked()

        controller_type = self._controllers[controller_name]

        parent = cmds.ls(selection=True, type="transform")
        if not parent:
            QtWidgets.QMessageBox.warning(
                self, "Error", "Please select a transform to build a controller below.")
            return

        controller.create_ctl(controller_type, name, size, parent[0], add_hook=add_hook)

    @undoable
    def _add_on_trs_released(self):
        """Slot called by the add on trs button."""
        controller_name = self._type_combobox.currentText()
        size = self._size_spinbox.value()

        controller_type = self._controllers[controller_name]

        parent = cmds.ls(selection=True, type="transform")
        if not parent:
            QtWidgets.QMessageBox.warning(
                self, "Error", "Please select a transform to build a controller below.")
            return

        controller.create_on_transform(controller_type, parent[0], size)
