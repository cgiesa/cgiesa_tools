# -*- coding: utf-8 -*-

from PySide2 import QtWidgets

from maya import cmds

from cgiesa_tools.maya.ui.qt.decorator import undoable
from cgiesa_tools.maya.ui.qt.qtutils import open_widget_as_window
from cgiesa_tools.maya.ui.configuration import WINDOW_TITLE_PREFIX

from cgiesa_tools.maya.ui.rigging.controller import ControllerWidget
from cgiesa_tools.maya.ui.rigging.deformer import WeightsWidget
from cgiesa_tools.maya.ui.rigging.deformer import OffsettableClusterWidget
from cgiesa_tools.maya.ui.rigging.deformer import MovingDeformerWidget
from cgiesa_tools.maya.ui.rigging.naming import NamingWidget
from cgiesa_tools.maya.rigging.locator import create_locator_on_selection_center
from cgiesa_tools.maya.rigging.transform import lock_and_hide_trs_attrs_on_selection
from cgiesa_tools.maya.rigging.transform import unlock_and_show_trs_attrs_on_selection
from cgiesa_tools.maya.rigging.transform import add_hook_on_selection
from cgiesa_tools.maya.rigging.follicle import create_follicle_on_closest_point_from_selection


class RigToolBoxGroupWidget(QtWidgets.QGroupBox):
    '''
    A small wrapper for simple widgets that should be packed inside a group
    box.
    '''
    def __init__(self, title='', parent=None, widget=None):
        super(RigToolBoxGroupWidget, self).__init__(title, parent)

        self._layout = QtWidgets.QVBoxLayout(self)
        self._layout.setContentsMargins(2, 2, 2, 2)

        if widget is not None:
            widget.setParent(self)
            self._layout.addWidget(widget)


class DeformerWidget(QtWidgets.QWidget):
    '''
    A widget combining all deformer linked tools.
    '''
    def __init__(self, parent=None):
        super(DeformerWidget, self).__init__(parent)

        self._deformer_widget_grp = RigToolBoxGroupWidget("Weights", self, WeightsWidget())
        self._offsettable_cluster_grp = RigToolBoxGroupWidget('Offsettable cluster', self, OffsettableClusterWidget())
        self._moving_deformer_grp = RigToolBoxGroupWidget('Moving Deformer', self, MovingDeformerWidget())

        self._layout = QtWidgets.QVBoxLayout(self)
        self._layout.setContentsMargins(4, 4, 4, 4)
        self._layout.setSpacing(4)

        self._layout.addWidget(self._deformer_widget_grp)
        self._layout.addWidget(self._offsettable_cluster_grp)
        self._layout.addWidget(self._moving_deformer_grp)
        self._layout.addStretch(1)


class MiscWidget(QtWidgets.QWidget):
    '''
    This widget contains misc rigging tools.
    '''
    def __init__(self, parent=None):
        super(MiscWidget, self).__init__(parent)

        # ---------------------------------------------------------------------
        # Display
        self._display_grp = QtWidgets.QGroupBox('Display', self)

        self._show_label = QtWidgets.QLabel("Show Local Axises:")
        self._show_all_axises = QtWidgets.QPushButton("All", self._display_grp)
        self._show_selected_axises = QtWidgets.QPushButton("Selected", self._display_grp)
        self._show_hierarchy_axises = QtWidgets.QPushButton("Hierarchy", self._display_grp)
        self._hide_label = QtWidgets.QLabel("Hide Local Axises:")
        self._hide_all_axises = QtWidgets.QPushButton("All", self._display_grp)
        self._hide_selected_axises = QtWidgets.QPushButton("Selected", self._display_grp)
        self._hide_hierarchy_axises = QtWidgets.QPushButton("Hierarchy", self._display_grp)

        self._show_all_axises.released.connect(self._call_show_all_axises)
        self._show_selected_axises.released.connect(self._call_show_selected_axises)
        self._show_hierarchy_axises.released.connect(self._call_show_hierarchy_axises)
        self._hide_all_axises.released.connect(self._call_hide_all_axises)
        self._hide_selected_axises.released.connect(self._call_hide_selected_axises)
        self._hide_hierarchy_axises.released.connect(self._call_hide_hierarchy_axises)

        self._display_grp_layout = QtWidgets.QGridLayout(self._display_grp)
        self._display_grp_layout.setContentsMargins(4, 4, 4, 4)
        self._display_grp_layout.setSpacing(4)
        self._display_grp_layout.addWidget(self._show_label, 0, 0)
        self._display_grp_layout.addWidget(self._show_all_axises, 0, 1)
        self._display_grp_layout.addWidget(self._show_selected_axises, 0, 2)
        self._display_grp_layout.addWidget(self._show_hierarchy_axises, 0, 3)
        self._display_grp_layout.addWidget(self._hide_label, 1, 0)
        self._display_grp_layout.addWidget(self._hide_all_axises, 1, 1)
        self._display_grp_layout.addWidget(self._hide_selected_axises, 1, 2)
        self._display_grp_layout.addWidget(self._hide_hierarchy_axises, 1, 3)

        # ---------------------------------------------------------------------
        # Locator
        self._center_locator_grp = QtWidgets.QGroupBox('Locator', self)
        self._center_locator_layout = QtWidgets.QVBoxLayout(
            self._center_locator_grp)
        self._center_locator_layout.setContentsMargins(4, 4, 4, 4)
        self._center_locator_layout.setSpacing(4)

        self._center_locator_button = QtWidgets.QPushButton(
            'Create locator at selection center', self._center_locator_grp)
        self._center_locator_layout.addWidget(self._center_locator_button)
        self._center_locator_button.clicked.connect(
            self._call_create_locator_at_center)

        # ---------------------------------------------------------------------
        # Transform / Joints
        self._transform_grp = QtWidgets.QGroupBox('Transforms/Joints', self)

        self._transform_layout = QtWidgets.QGridLayout(self._transform_grp)
        self._transform_layout.setContentsMargins(4, 4, 4, 4)
        self._transform_layout.setSpacing(4)

        self._lock_attrs_btn = QtWidgets.QPushButton(
            'Lock', self._transform_grp)
        self._lock_attrs_btn.setToolTip(
            'Lock and hide all transform attributes on selected nodes.')
        self._lock_attrs_btn.clicked.connect(self._call_lock_attrs)
        self._transform_layout.addWidget(self._lock_attrs_btn, 0, 0)

        self._unlock_attrs_btn = QtWidgets.QPushButton(
            'Unlock', self._transform_grp)
        self._unlock_attrs_btn.setToolTip(
            'Unlock and show all transform attributes on selected nodes.')
        self._unlock_attrs_btn.clicked.connect(self._call_unlock_attrs)
        self._transform_layout.addWidget(self._unlock_attrs_btn, 0, 1)

        self._add_hook_btn = QtWidgets.QPushButton(
            'Add Hook', self._transform_grp)
        self._add_hook_btn.setToolTip(
            'Add a hook transform above all selected nodes.')
        self._add_hook_btn.clicked.connect(self._call_add_hook)
        self._transform_layout.addWidget(self._add_hook_btn, 0, 2)

        # ---------------------------------------------------------------------
        # Follicles
        self._follicle_grp = QtWidgets.QGroupBox('Follicles', self)

        self._follicle_layout = QtWidgets.QGridLayout(self._follicle_grp)
        self._follicle_layout.setContentsMargins(4, 4, 4, 4)
        self._follicle_layout.setSpacing(4)

        self._create_closest_follicle_btn = QtWidgets.QPushButton(
            'Create closest follicle', self._follicle_grp)
        self._create_closest_follicle_btn.setToolTip(
            'Select a transform and a surface. This will create a follicle at'
            'the closest point.')
        self._create_closest_follicle_btn.clicked.connect(
            self._call_create_closest_follicle)
        self._follicle_layout.addWidget(
            self._create_closest_follicle_btn, 0, 0)

        # ---------------------------------------------------------------------
        # Main layout
        self._layout = QtWidgets.QVBoxLayout(self)
        self._layout.setContentsMargins(4, 4, 4, 4)
        self._layout.setSpacing(4)

        self._layout.addWidget(self._display_grp)
        self._layout.addWidget(self._center_locator_grp)
        self._layout.addWidget(self._transform_grp)
        self._layout.addWidget(self._follicle_grp)
        self._layout.addStretch(1)

    @undoable
    def _call_show_all_axises(self):
        for node in cmds.ls(type="transform"):
            cmds.setAttr("{}.displayLocalAxis".format(node), True)

    @undoable
    def _call_show_selected_axises(self):
        for node in cmds.ls(selection=True, type="transform"):
            cmds.setAttr("{}.displayLocalAxis".format(node), True)

    @undoable
    def _call_show_hierarchy_axises(self):
        for node in cmds.ls(selection=True, dag=True, type="transform"):
            cmds.setAttr("{}.displayLocalAxis".format(node), True)

    @undoable
    def _call_hide_all_axises(self):
        for node in cmds.ls(type="transform"):
            cmds.setAttr("{}.displayLocalAxis".format(node), False)

    @undoable
    def _call_hide_selected_axises(self):
        for node in cmds.ls(selection=True, type="transform"):
            cmds.setAttr("{}.displayLocalAxis".format(node), False)

    @undoable
    def _call_hide_hierarchy_axises(self):
        for node in cmds.ls(selection=True, dag=True, type="transform"):
            cmds.setAttr("{}.displayLocalAxis".format(node), False)

    @undoable
    def _call_create_locator_at_center(self):
        create_locator_on_selection_center()

    @undoable
    def _call_lock_attrs(self):
        lock_and_hide_trs_attrs_on_selection()

    @undoable
    def _call_unlock_attrs(self):
        unlock_and_show_trs_attrs_on_selection()

    @undoable
    def _call_add_hook(self):
        add_hook_on_selection()

    @undoable
    def _call_create_closest_follicle(self):
        create_follicle_on_closest_point_from_selection()


class RigToolboxWidget(QtWidgets.QWidget):
    '''
    This is the main widget of a rig tool box which will include all kinds
    of different tools helping the daily work of rigging.
    '''
    def __init__(self, *args, **kwargs):
        super(RigToolboxWidget, self).__init__(*args, **kwargs)

        self._layout = QtWidgets.QVBoxLayout(self)
        self._layout.setContentsMargins(4, 4, 4, 4)
        self._layout.setSpacing(4)

        self._tabs = QtWidgets.QTabWidget(self)
        self._layout.addWidget(self._tabs)

        self._controller_widget = ControllerWidget()
        self._tabs.addTab(self._controller_widget, 'Controllers')

        self._deformer_widget = DeformerWidget()
        self._tabs.addTab(self._deformer_widget, 'Deformers')

        self._misc_widget = MiscWidget()
        self._tabs.addTab(self._misc_widget, 'Misc')

        self._naming_widget = NamingWidget()
        self._tabs.addTab(self._naming_widget, 'Naming')


def open_window():
    open_widget_as_window(RigToolboxWidget,
                          WINDOW_TITLE_PREFIX + ' - Rig Toolbox')
