# -*- coding: utf-8 -*-

# third party modules
from PySide2 import QtWidgets
from maya import cmds

# tool modules
from cgiesa_tools.maya.ui.qt.decorator import undoable
from cgiesa_tools.maya.rigging.deformer import create_offsettable_cluster_from_selection
from cgiesa_tools.maya.rigging.deformer import create_moving_deformer_from_selection
from cgiesa_tools.maya.rigging.deformer import prune_deformer_membership


class WeightsWidget(QtWidgets.QWidget):
    """A widget containing multiple deformer tools."""

    def __init__(self, *args, **kwargs):
        super(WeightsWidget, self).__init__(*args, **kwargs)

        self._prune_membership_label = QtWidgets.QLabel("Prune Membership:", self)
        self._prune_membership_spinbox = QtWidgets.QDoubleSpinBox(minimum=0.0, maximum=1.0, decimals=4, value=0.001, singleStep=0.001)
        self._prune_membership_button = QtWidgets.QPushButton("Apply")
        self._prune_membership_button.released.connect(self._prune_membership_released)

        self._layout = QtWidgets.QGridLayout(self)
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._layout.addWidget(self._prune_membership_label, 0, 0)
        self._layout.addWidget(self._prune_membership_spinbox, 0, 1)
        self._layout.addWidget(self._prune_membership_button, 0, 2)

        self._layout.setRowStretch(0, 0)
        self._layout.setRowStretch(1, 1)
        self._layout.setRowStretch(2, 1)

    @undoable
    def _prune_membership_released(self):
        deformer = cmds.ls(selection=True, type="weightGeometryFilter")
        if not deformer:
            return
        below = self._prune_membership_spinbox.value()
        prune_deformer_membership(deformer[0], below)


class OffsettableClusterWidget(QtWidgets.QWidget):
    '''
    This is just a simple widget with one single button to call a function
    that creates an offsettable cluster from the current selection. Please
    refer to the called function for more details.
    '''

    def __init__(self, parent=None):
        super(OffsettableClusterWidget, self).__init__(parent)

        self._layout = QtWidgets.QVBoxLayout(self)
        self._layout.setContentsMargins(0, 0, 0, 0)

        self._button = QtWidgets.QPushButton('Make offsettable cluster')
        self._button.clicked.connect(self._call_button_func)
        self._layout.addWidget(self._button)

    @undoable
    def _call_button_func(self):
        create_offsettable_cluster_from_selection()


class MovingDeformerWidget(QtWidgets.QWidget):
    '''
    A widget to create a moving deformer on a selected geometry (one or more).
    '''

    def __init__(self, parent=None):
        super(MovingDeformerWidget, self).__init__(parent)

        self._layout = QtWidgets.QVBoxLayout(self)
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._layout.setSpacing(4)

        self._line_edit = QtWidgets.QLineEdit('Name:', self)
        self._line_edit.setText('movingDef_01')
        self._layout.addWidget(self._line_edit)

        self._button = QtWidgets.QPushButton('Create', self)
        self._button.clicked.connect(self._call_create_moving_deformer)
        self._layout.addWidget(self._button)

    @undoable
    def _call_create_moving_deformer(self):
        name = self._line_edit.text()
        create_moving_deformer_from_selection(name)
