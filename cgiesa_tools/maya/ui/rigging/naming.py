# -*- coding: utf-8 -*-

import logging
from PySide2 import QtCore, QtWidgets, QtGui

import maya.cmds as cmds

from cgiesa_tools.general.enum import enum
from cgiesa_tools.maya.rigging.naming import add_prefix, add_suffix, replace, \
  multi_name


class NodeNamingSelector(object):
    '''
    An object that returns a node list based on the given selection mode. There
    are currently three different modes, the entire scene, the current
    selection or the selected hierarchy.
    '''
    MODE = enum('ALL', 'SELECTION', 'HIERARCHY')
    DEFAULT_MODE = MODE.SELECTION

    def __init__(self):
        super(NodeNamingSelector, self).__init__()
        self._mode = self.DEFAULT_MODE

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode):
        self._mode = mode

    def get_node_names(self):
        # ALL should be used very carefully since it will rename all nodes.
        # TODO: Locked nodes should be ignored.
        if self.mode == self.MODE.ALL:
            return cmds.ls()
        elif self.mode == self.MODE.SELECTION:
            return cmds.ls(selection=True)
        elif self.mode == self.MODE.HIERARCHY:
            return cmds.ls(selection=True, dag=True)


class SelectorWidget(QtWidgets.QWidget):
    '''
    A small widget that allows to choose among the different node selection
    modes.
    '''
    def __init__(self, selector, parent=None):
        super(SelectorWidget, self).__init__(parent)
        self._selector = selector

        self._layout = QtWidgets.QHBoxLayout(self)
        self._layout.setContentsMargins(4, 4, 4, 4)
        self._layout.setSpacing(4)

        self._all_button = QtWidgets.QRadioButton('All', self)
        self._all_button.clicked.connect(self._set_selector_all)
        self._layout.addWidget(self._all_button)

        self._selected_button = QtWidgets.QRadioButton('Selected', self)
        self._selected_button.clicked.connect(self._set_selector_selected)
        self._layout.addWidget(self._selected_button)

        self._hierarchy_button = QtWidgets.QRadioButton('Hierarchy', self)
        self._hierarchy_button.clicked.connect(self._set_selector_hierarchy)
        self._layout.addWidget(self._hierarchy_button)

        self._layout.addStretch(1)

        self._selected_button.setChecked(QtCore.Qt.Checked)

    def _set_selector_all(self):
        self._selector.mode = self._selector.MODE.ALL

    def _set_selector_selected(self):
        self._selector.mode = self._selector.MODE.SELECTION

    def _set_selector_hierarchy(self):
        self._selector.mode = self._selector.MODE.HIERARCHY


class AddPrefixWidget(QtWidgets.QWidget):
    '''
    A widget to add a specified prefix to all selected nodes. You have to
    provide a node naming selector that provides the node name list to act on
    based on defined selection modes.
    '''

    def __init__(self, selector, parent=None):
        super(AddPrefixWidget, self).__init__(parent)
        self._selector = selector

        self._layout = QtWidgets.QHBoxLayout(self)
        self._layout.setContentsMargins(4, 4, 4, 4)
        self._layout.setSpacing(4)

        self._label = QtWidgets.QLabel('Prefix:', self)
        self._label.setMinimumWidth(200)
        self._layout.addWidget(self._label)

        self._line_edit = QtWidgets.QLineEdit(self)
        self._layout.addWidget(self._line_edit)

        self._button = QtWidgets.QPushButton('Apply', self)
        self._button.clicked.connect(self._call_add_prefix)
        self._layout.addWidget(self._button)

        self._layout.addStretch(1)

    def set_label_width(self, width):
        self._label.setMinimumWidth(width)

    def _call_add_prefix(self):
        prefix = self._line_edit.text()

        if not prefix:
            logging.warning('No valid prefix defined. Skipped.')
            return

        add_prefix(prefix, self._selector.get_node_names())


class AddSuffixWidget(QtWidgets.QWidget):
    '''
    A widget to add a specified suffix to all selected nodes. You have to
    provide a node naming selector that provides the node name list to act on
    based on defined selection modes.
    '''

    def __init__(self, selector, parent=None):
        super(AddSuffixWidget, self).__init__(parent)
        self._selector = selector

        self._layout = QtWidgets.QHBoxLayout(self)
        self._layout.setContentsMargins(4, 4, 4, 4)
        self._layout.setSpacing(4)

        self._label = QtWidgets.QLabel('Suffix:', self)
        self._layout.addWidget(self._label)

        self._line_edit = QtWidgets.QLineEdit(self)
        self._layout.addWidget(self._line_edit)

        self._button = QtWidgets.QPushButton('Apply', self)
        self._button.clicked.connect(self._call_add_suffix)
        self._layout.addWidget(self._button)

        self._layout.addStretch(1)

    def set_label_width(self, width):
        self._label.setMinimumWidth(width)

    def _call_add_suffix(self):
        prefix = self._line_edit.text()

        if not prefix:
            logging.warning('No valid prefix defined. Skipped.')
            return

        add_suffix(prefix, self._selector.get_node_names())


class ReplaceWidget(QtWidgets.QWidget):
    '''
    A widget to replace a specific string with another. You have to provide a
    node naming selector that provides the node name list to act on based on
    defined selection modes.
    '''

    def __init__(self, selector, parent=None):
        super(ReplaceWidget, self).__init__(parent)
        self._selector = selector

        self._layout = QtWidgets.QHBoxLayout(self)
        self._layout.setContentsMargins(4, 4, 4, 4)
        self._layout.setSpacing(4)

        self._search_label = QtWidgets.QLabel('Search:', self)
        self._layout.addWidget(self._search_label)

        self._search_line_edit = QtWidgets.QLineEdit(self)
        self._layout.addWidget(self._search_line_edit)

        self._replace_label = QtWidgets.QLabel('Replace:', self)
        self._layout.addWidget(self._replace_label)

        self._replace_line_edit = QtWidgets.QLineEdit(self)
        self._layout.addWidget(self._replace_line_edit)

        self._button = QtWidgets.QPushButton('Apply', self)
        self._button.clicked.connect(self._call_replace)
        self._layout.addWidget(self._button)

        self._layout.addStretch(1)

    def set_label_width(self, width):
        self._search_label.setMinimumWidth(width)
        self._replace_label.setMinimumWidth(width)

    def _call_replace(self):
        search_str = self._search_line_edit.text()
        replace_str = self._replace_line_edit.text()

        if not search_str:
            logging.warning('No valid search string defined. Skipped.')
            return

        replace(search_str, replace_str, self._selector.get_node_names())


class IncrementorWidget(QtWidgets.QWidget):
    '''
    A widget to add a number at the end of the current node name. You can
    define the start number, the number of digits and the amount to increment
    for each node. You have to provide a node naming selector that provides the
    node name list to act on based on defined selection modes.
    '''

    def __init__(self, selector, parent=None):
        super(IncrementorWidget, self).__init__(parent)
        self._selector = selector
        self._int_validator = QtGui.QIntValidator(self)

        self._layout = QtWidgets.QHBoxLayout(self)
        self._layout.setContentsMargins(4, 4, 4, 4)
        self._layout.setSpacing(4)

        self._name_label = QtWidgets.QLabel('Name:', self)
        self._layout.addWidget(self._name_label)

        self._name_line_edit = QtWidgets.QLineEdit(self)
        self._layout.addWidget(self._name_line_edit)

        self._start_label = QtWidgets.QLabel('Start:', self)
        self._layout.addWidget(self._start_label)

        self._start_spin_box = QtWidgets.QSpinBox(self)
        self._start_spin_box.setMinimum(0)
        self._start_spin_box.setSingleStep(1)
        self._start_spin_box.setValue(1)
        self._layout.addWidget(self._start_spin_box)

        self._digits_label = QtWidgets.QLabel('Digits:', self)
        self._layout.addWidget(self._digits_label)

        self._digits_spin_box = QtWidgets.QSpinBox(self)
        self._digits_spin_box.setMinimum(0)
        self._digits_spin_box.setSingleStep(1)
        self._digits_spin_box.setValue(2)
        self._layout.addWidget(self._digits_spin_box)

        self._increment_label = QtWidgets.QLabel('Increment by:', self)
        self._layout.addWidget(self._increment_label)

        self._increment_spin_box = QtWidgets.QSpinBox(self)
        self._increment_spin_box.setMinimum(0)
        self._increment_spin_box.setSingleStep(1)
        self._increment_spin_box.setValue(1)
        self._layout.addWidget(self._increment_spin_box)

        self._button = QtWidgets.QPushButton('Apply', self)
        self._button.clicked.connect(self._call_apply)
        self._layout.addWidget(self._button)

        self._layout.addStretch(1)

    def _call_apply(self):
        name = self._name_line_edit.text()
        start = self._start_spin_box.value()
        digits = self._digits_spin_box.value()
        by = self._increment_spin_box.value()

        multi_name(name, self._selector.get_node_names(), start, by, digits)


class NamingWidget(QtWidgets.QWidget):
    '''A widget to give access to different renaming features.'''

    LABEL_WIDTH = 50

    def __init__(self, parent=None):
        super(NamingWidget, self).__init__(parent)
        self._selector = NodeNamingSelector()
        self._layout = QtWidgets.QVBoxLayout(self)
        self._layout.setContentsMargins(4, 4, 4, 4)
        self._layout.setSpacing(4)

        self._selector_group = QtWidgets.QGroupBox('Node selection', self)
        self._layout.addWidget(self._selector_group)

        self._selector_layout = QtWidgets.QHBoxLayout(self._selector_group)
        self._selector_layout.setContentsMargins(2, 2, 2, 2)
        self._selector_layout.setSpacing(4)

        self._selector_widget = SelectorWidget(
            self._selector, self._selector_group)
        self._selector_layout.addWidget(self._selector_widget)

        self._add_prefix_group = QtWidgets.QGroupBox('Add Prefix', self)
        self._layout.addWidget(self._add_prefix_group)

        self._add_prefix_layout = QtWidgets.QHBoxLayout(self._add_prefix_group)
        self._add_prefix_layout.setContentsMargins(2, 2, 2, 2)
        self._add_prefix_layout.setSpacing(4)

        self._add_prefix_widget = AddPrefixWidget(
            self._selector, self._add_prefix_group)
        self._add_prefix_widget.set_label_width(self.LABEL_WIDTH)
        self._add_prefix_layout.addWidget(self._add_prefix_widget)

        self._add_suffix_group = QtWidgets.QGroupBox('Add Suffix', self)
        self._layout.addWidget(self._add_suffix_group)

        self._add_suffix_layout = QtWidgets.QHBoxLayout(self._add_suffix_group)
        self._add_suffix_layout.setContentsMargins(2, 2, 2, 2)
        self._add_suffix_layout.setSpacing(4)

        self._add_suffix_widget = AddSuffixWidget(
            self._selector, self._add_suffix_group)
        self._add_suffix_widget.set_label_width(self.LABEL_WIDTH)
        self._add_suffix_layout.addWidget(self._add_suffix_widget)

        self._replace_group = QtWidgets.QGroupBox('Replace', self)
        self._layout.addWidget(self._replace_group)

        self._replace_layout = QtWidgets.QHBoxLayout(self._replace_group)
        self._replace_layout.setContentsMargins(2, 2, 2, 2)
        self._replace_layout.setSpacing(4)

        self._replace_widget = ReplaceWidget(
            self._selector, self._replace_group)
        self._replace_widget.set_label_width(self.LABEL_WIDTH)
        self._replace_layout.addWidget(self._replace_widget)

        self._increment_group = QtWidgets.QGroupBox('Name incrementor', self)
        self._layout.addWidget(self._increment_group)

        self._increment_layout = QtWidgets.QHBoxLayout(self._increment_group)
        self._increment_layout.setContentsMargins(2, 2, 2, 2)
        self._increment_layout.setSpacing(4)

        self._increment_widget = IncrementorWidget(
            self._selector, self._increment_group)
        self._increment_layout.addWidget(self._increment_widget)

        self._layout.addStretch(1)
