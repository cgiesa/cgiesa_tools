# -*- coding: utf-8 -*-

from PySide2 import QtWidgets
import maya.cmds as cmds

from cgiesa_tools.maya.ui.qt.qtutils import open_widget_as_window
from cgiesa_tools.maya.ui.configuration import WINDOW_TITLE_PREFIX

from cgiesa_tools.general.ui.slider import FloatSliderGroup
from cgiesa_tools.maya.animation.tools import add_inbetween_key

_widget = None


class AddInbetweenKeyframeWidget(QtWidgets.QWidget):
    '''
    A widget to use the add_inbetween_key function. The widget take the current
    selection and allows to add inbetween frames either on all animated
    attributes, or the user can choose among each attribute. The UI updates
    when the selection changes.
    '''
    def __init__(self, parent=None):
        super(AddInbetweenKeyframeWidget, self).__init__(parent)

        self._layout = QtWidgets.QVBoxLayout(self)
        self._label = QtWidgets.QLabel(
            'Drag the slider around to create in-between keys.', self)
        self._slider = FloatSliderGroup(self)
        self._update_cb = QtWidgets.QCheckBox('Update scene', self)

        self._layout.addWidget(self._label)
        self._layout.addWidget(self._slider)
        self._layout.addWidget(self._update_cb)
        self._layout.addStretch()
        self._layout.setContentsMargins(4, 4, 4, 4)
        self._layout.setSpacing(4)

        # This must come first to make undo work.
        self._slider.slider_pressed.connect(self._open_undo_chunk)

        self._slider.slider_pressed.connect(self._process_selection)
        self._slider.slider_moved.connect(self.execute)
        # We call again the execute after release, because there is a bug that
        # execute is not called when the user does not drag but only click.
        self._slider.slider_released.connect(self._execute_from_release)

        # This must come last to make undo work.
        self._slider.slider_released.connect(self._close_undo_chunk)

        self._selection = {}
        self._current_time = None
        self._update = False

    def _process_selection(self):
        '''
        When the slider is clicked, we do as much as preprocessing as possible
        to make the update fast.
        '''
        self._selection = {}
        self._current_time = cmds.currentTime(query=True)
        self._update = self._update_cb.isChecked()

        selection = cmds.ls(selection=True)
        if not selection:
            cmds.warning('Please select a node to apply the in-between key.')
        for node in selection:
            # Check for animated attributes.
            attr_list = []
            for attr in cmds.listAttr(node, keyable=True, multi=True):
                if cmds.keyframe(node, attribute=attr, query=True,
                                 keyframeCount=True):
                    # We add an additional check (to avoid nasty warnings
                    # while dragging around the slider) to make sure that we
                    # have keyframes before and after the current frame.
                    fr1 = cmds.findKeyframe(
                        node, attribute=attr,
                        time=(self._current_time, self._current_time),
                        which='previous')
                    fr2 = cmds.findKeyframe(
                        node, attribute=attr,
                        time=(self._current_time, self._current_time),
                        which='next')
                    if not fr1 < self._current_time:
                        continue
                    if not fr2 > self._current_time:
                        continue
                    attr_list.append(attr)
            if not attr_list:
                cmds.warning('No animated attributes found on node {}. '
                             'Skipped.'.format(node))
                continue
            self._selection[node] = {}
            self._selection[node]['attr_list'] = attr_list

    def _execute_from_release(self):
        '''
        This is a special work around to make the update work when the user
        only clicks without dragging.
        '''
        self.execute(self._slider.value)

    def execute(self, value):
        for node in self._selection.keys():
            add_inbetween_key(node, self._selection[node]['attr_list'], value,
                              self._current_time)
            if self._update:
                cmds.dgdirty(node)

    def _open_undo_chunk(self):
        cmds.undoInfo(openChunk=True)

    def _close_undo_chunk(self):
        cmds.undoInfo(closeChunk=True)


def open_window():
    open_widget_as_window(AddInbetweenKeyframeWidget,
                          WINDOW_TITLE_PREFIX + ' - Inbetween keyer')
