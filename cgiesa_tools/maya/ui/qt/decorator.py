# -*- coding: utf-8 -*-

import functools

import maya.cmds as cmds


def undoable(func):
    '''
    This decorator should be used when calling functions from PySide2
    interfaces. Maya has problems to handle correctly the undo. It is not put
    inside one single stack which makes undo almost impossible (you undo every
    single step of your function separately).
    '''
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        cmds.undoInfo(openChunk=True)
        try:
            return func(*args, **kwargs)
        finally:
            cmds.undoInfo(closeChunk=True)
    return wrapper


# TODO: Add the redoable decorator with correct error handling.
