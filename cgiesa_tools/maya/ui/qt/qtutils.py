# -*- coding: utf-8 -*-

from PySide2 import QtWidgets, QtCore
import shiboken2
import maya.OpenMayaUI as omui

_widget_windows = {}


def get_main_window_instance():
    '''
    Returns the Maya main window instance to use inside Python.
    '''
    return shiboken2.wrapInstance(long(omui.MQtUtil.mainWindow()), QtWidgets.QMainWindow)


def open_widget_as_window(widget_class, title):
    '''
    You can use this to simply open single widgets inside a window.
    '''
    global _widget_windows

    if widget_class not in _widget_windows:
        widget = widget_class(parent=get_main_window_instance())
        widget.setWindowFlags(QtCore.Qt.Window)
        widget.setWindowTitle(title)
        _widget_windows[widget_class] = widget
    _widget_windows[widget_class].show()
