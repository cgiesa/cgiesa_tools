# -*- coding: utf-8 -*-

from PySide2 import QtWidgets
from cgiesa_tools.maya.ui.qt.qtutils import open_widget_as_window


class CorrectiveBlendshapeWidget(QtWidgets.QWidget):
    '''
    GUI to create and manage corrective blendshapes.
    '''
    def __init__(self, parent=None):
        super(CorrectiveBlendshapeWidget, self).__init__(parent)

        self._layout = QtWidgets.QVBoxLayout(self)
        self._layout.setContentsMargins(4, 4, 4, 4)
        self._layout.setSpacing(4)

        self._sculpt_btn = QtWidgets.QPushButton(
            'Sculpt selected mesh(es)', self)
        self._sculpt_btn.clicked.connect(self._call_sculpt)
        self._layout.addWidget(self._sculpt_btn)

        self._apply_btn = QtWidgets.QPushButton(
            'Apply selected sculpt(s)', self)
        self._apply_btn.clicked.connect(self._call_apply)
        self._layout.addWidget(self._apply_btn)

        self._apply_new_btn = QtWidgets.QPushButton(
            'Apply sculpt(s) on new blendshape', self)
        self._apply_new_btn.clicked.connect(self._call_apply_new)
        self._layout.addWidget(self._apply_new_btn)

        self._cancel_btn = QtWidgets.QPushButton(
            'Cancel scult(s)', self)
        self._cancel_btn.clicked.connect(self._call_cancel)
        self._layout.addWidget(self._cancel_btn)

        self._layout.addStretch(1)

    def _call_sculpt(self):
        print 'Call sculpt...'

    def _call_apply(self):
        print 'Call sculpt...'

    def _call_apply_new(self):
        print 'Call sculpt...'

    def _call_cancel(self):
        print 'Call sculpt...'


def open_window():
    open_widget_as_window(CorrectiveBlendshapeWidget,
                          u'© umedia - Corrective Blendshape')
