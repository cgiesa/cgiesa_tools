# -*- coding: utf-8 -*-

import functools

import maya.cmds as cmds


def need_plugin(*names):
    '''
    When a plugin is needed in any function, use this decorator to make sure
    that it is loaded. You can provide one or more plugins in one go.
    '''
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            for name in names:
                if not cmds.pluginInfo(name, query=True, loaded=True):
                    cmds.loadPlugin(name)
            return func(*args, **kwargs)
        return wrapper
    return decorator
