# -*- coding: utf-8 -*-

import maya.mel as mel
import maya.cmds as cmds
import pymel.core as pm


def apply_ref_edits_again(ref_node):
    '''
    I came to a situation where reference edits were not applied correctly
    although all node and attribute names did match between the different
    referenced files.

    My guess is that the having different reference hierarchy configurations
    (that was at least the only difference in my case) makes break the
    reference edit application.

    This script does just apply again the same edits. Since nodes can be
    parented and do not match the full path names anymore that is provided by
    the referenceQuery command, we store first all node names and their
    corresponding pymel node in a dictonary that will get replaced in all
    reference edit commands.
    '''
    # We store the pymel nodes first using full paths as key value.
    node_dict = {}
    for node in cmds.referenceQuery(
            ref_node, editNodes=True, failedEdits=True):
        node = cmds.ls(node, long=True)
        if node:
            if node[0] in node_dict:
                continue
            node_dict[node[0]] = pm.ls(node[0])[0]

    for cmd in cmds.referenceQuery(
            ref_node, editStrings=True, failedEdits=True):
        # We loop through all nodes and replace the long name with their
        # current long name.
        for node, pmnode in node_dict.iteritems():
            cmd = cmd.replace(node, pmnode.longName())
        try:
            mel.eval(cmd)
            # This is ugly, but I could not figure out how to avoid
            # RuntimeErrors when the ref edits try to access locked attributes
            # for example.
        except RuntimeError:
            pass
