# -*- coding: utf-8 -*-


def clamp(x, minval, maxval):
    '''
    This will clamp x between minval and maxval.
    '''
    return min(max(x, minval), maxval)


def linear(x, minval, maxval):
    '''
    This will interpolate linearly between min and max with x between 0 and 1.
    '''
    return minval * (1 - x) + maxval * x


def smoothstep(x):
    '''
    X must be between 0 and 1. This will return the 3rd order smoothstepped
    interpolation.
    '''
    return x * x * (3 - 2 * x)


def smootherstep(x):
    '''
    X must be between 0 and 1. This will return the 5rd order smoothstepped
    interpolation.
    '''
    return x * x * x * (x * (x * 6 - 15) + 10)


def smootheststep(x):
    '''
    X must be between 0 and 1. This will return the 7rd order smoothstepped
    interpolation.
    '''
    return x * x * x * x * (x * (x * (70 - 20 * x) - 84) + 35)
