#!/usr/bin/env python3

# stdlib modules
import random
import datetime
import sys

class Point():
    """Simple 3D point class."""

    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        return "Point({}, {}, {})".format(self.x, self.y, self.z)

    def __getitem__(self, index):
        if index == 0:
            return self.x
        if index == 1:
            return self.y
        if index == 2:
            return self.z

    def squared_distance(self, other):
        value = (other.x - self.x) ** 2 + (other.y - self.y) ** 2 + (other.z - self.z) ** 2
        return value


class Node():
    """KD-Tree node."""

    def __init__(self):
        self.dimension = None
        self.left = None
        self.right = None
        self.point = None
        self.point_index = None

    def __repr__(self):
        return "N({})".format(self.point_index)


class KDTree():

    def __init__(self, points, dimension=0):
        dimension = dimension % 3

        points.sort(key=lambda x: x[1][dimension])
        median = len(points) // 2
        index, point = points[median]

        node = Node()
        node.point = point
        node.point_index = index
        node.dimension = dimension
        
        #print(len(points), "median", median)
        #if median:
            #print("left length", len(points[:median]))
        #if median < len(points) - 1:
            #print("right length", len(points[median + 1:]))
        
        if median:
            node.left = KDTree(points[:median], dimension + 1)
        if median < len(points) - 1:
            node.right = KDTree(points[median + 1:], dimension + 1)

        self.root = node

    def find_closest(self, point, data=None):
        node = self.root
        data = data or {"closest_index": None,
                        "closest_distance": sys.float_info.max}

        distance = point.squared_distance(node.point)
        if distance < data["closest_distance"]:
            data["closest_index"] = node.point_index
            data["closest_distance"] = distance
        
        dim_distance = (node.point[node.dimension] - point[node.dimension]) ** 2
        
        if point[node.dimension] <= node.point[node.dimension]:
            if node.left is not None:
                node.left.find_closest(point, data)
            if dim_distance < data["closest_distance"] and node.right is not None:
                node.right.find_closest(point, data)
        else:
            if node.right is not None:
                node.right.find_closest(point, data)
            if dim_distance < data["closest_distance"] and node.left is not None:
                node.left.find_closest(point, data)

        return data["closest_index"]


def find_closest(search_pnt, points):
    """Returns the index of the closest point.
    
    :param search_pnt: The search point.
    :type search_pnt: Point
    
    :param points: The list of points to compare with.
    :type points: list(Point)
    
    :rtype: int
    """
    closest = None
    closest_distance = sys.float_info.max

    for i, point in enumerate(points):
        distance = search_pnt.squared_distance(point)
        if distance < closest_distance:
            closest_distance = distance
            closest = i
    
    return closest


if __name__ == "__main__":
    print("Test KD-Tree implementation and compare with brute-force approach.")

    print("Generate 50000 random points.")

    
    points = [Point(random.random(), random.random(), random.random()) for _ in range(100000)]

    print("Generate 20 search points.")
    
    search = [Point(random.random(), random.random(), random.random()) for _ in range(20)]
    
    print("Run brute force search.")
    
    start_time = datetime.datetime.now()

    check = []
    for each in search:
        closest = find_closest(each, points)
        print(each, closest)
        check.append(closest)

    print("Execution time: {}".format(datetime.datetime.now() - start_time))

    print("Build KD-Tree")
    
    start_time = datetime.datetime.now()
    
    kd_points = [(i, p) for i, p in enumerate(points)]
    kdtree = KDTree(kd_points)
    print("Finished building the tree: {}".format(datetime.datetime.now() - start_time))

    for i, each in enumerate(search):
        closest = kdtree.find_closest(each)
        print(each, closest)
        assert closest == check[i]

    print("Execution time: {}".format(datetime.datetime.now() - start_time))
