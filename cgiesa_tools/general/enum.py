# -*- coding: utf-8 -*-


def enum(*sequential, **named):
    '''Use this function to simulate enums similar to C enums.'''
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)
