from PySide2 import QtWidgets, QtGui, QtCore


class ResizeSlider(QtWidgets.QSlider):
    '''
    This class will set its max value depending on its width.
    '''
    def resizeEvent(self, evt):
        self.setMaximum(evt.size().width())


class FloatSliderGroup(QtWidgets.QWidget):
    '''
    A widget that combines a float text field and a slider. The slider updates
    the float text field and vice versa.
    '''
    slider_pressed = QtCore.Signal()
    slider_moved = QtCore.Signal(float)
    slider_released = QtCore.Signal()

    def __init__(self, parent=None):
        super(FloatSliderGroup, self).__init__(parent)
        self._layout = QtWidgets.QHBoxLayout(self)
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._layout.setSpacing(4)

        self._text_field = QtWidgets.QLineEdit(self)
        self._slider = ResizeSlider(self)
        self._validator = QtGui.QDoubleValidator()

        self._layout.addWidget(self._text_field)
        self._layout.addWidget(self._slider)
        self._layout.setStretch(0, 0)
        self._layout.setStretch(1, 1)

        self._text_field.setValidator(self._validator)

        self._min = 0.0
        self._max = 1.0
        self._digits = 2

        self._slider.setOrientation(QtCore.Qt.Horizontal)

        self._slider.valueChanged.connect(self._update_text_field)
        self._slider.sliderPressed.connect(self._call_slider_pressed)
        self._slider.sliderMoved.connect(self._call_slider_moved)
        self._slider.sliderReleased.connect(self._call_slider_released)
        self._text_field.editingFinished.connect(self._update_slider)

        self._avoid_cycle = False

        self.value = 0.0

    def _call_slider_pressed(self):
        '''
        We emit the signal.
        '''
        self.slider_pressed.emit()

    def _call_slider_moved(self, slider_val):
        '''
        We emit the signal.
        '''
        self.slider_moved.emit(self.value)

    def _call_slider_released(self):
        '''
        We emit the signal.
        '''
        self.slider_released.emit()

    @property
    def digits(self):
        return self._digits

    @digits.setter
    def digits(self, digits):
        self._digits = digits
        self._set_value(self.value)

    @property
    def value(self):
        lerp = (float(self._slider.value() - self._slider.minimum()) /
                float(self._slider.maximum() - self._slider.minimum()))
        return self._min * (1 - lerp) + self._max * lerp

    @value.setter
    def value(self, val):
        self._avoid_cycle = True
        self._set_value(val)
        self._avoid_cycle = False

    @property
    def min(self):
        return self._min

    @min.setter
    def min(self, min_val):
        '''
        Sets the min value of the slider. This will redraw the widget.
        '''
        if not min_val < self._max:
            return
        val = self.value
        self._min = min_val
        self._set_value(val)

    @property
    def max(self):
        return self._max

    @max.setter
    def max(self, max_val):
        '''
        Sets the max value of the slider. This will redraw the widget.
        '''
        if not max_val > self._min:
            return
        val = self.value
        self._max = max_val
        self._set_value(val)

    def _set_value(self, val):
        '''
        This sets the text field and slider correctly. No cycle checking is
        done here. This should not called directly. Better use the value
        setter.
        '''
        limit = max(min(val, self._max), self._min)
        lerp = float(limit - self._min) / float(self._max - self._min)
        slider_val = (self._slider.minimum() * (1 - lerp) +
                      self._slider.maximum() * lerp)

        self._slider.setValue(slider_val)
        self._text_field.setText(
            ('{{:.{}f}}'.format(self.digits)).format(limit))

    def _update_slider(self):
        '''
        This is called by the text field signal and updates the slider.
        '''
        if self._avoid_cycle:
            return
        self._avoid_cycle = True
        value = float(self._text_field.text())
        self._set_value(value)
        self._avoid_cycle = False

    def _update_text_field(self, slider_int_value):
        '''
        This is called by the slider signal and updates the text field.
        '''
        if self._avoid_cycle:
            return
        self._avoid_cycle = True
        relative = (float(slider_int_value - self._slider.minimum()) /
                    float(self._slider.maximum() - self._slider.minimum()))
        val = self._min * (1 - relative) + self._max * relative
        self._set_value(val)
        self._avoid_cycle = False

    def text_field(self):
        '''
        Returns the text field widget to allow size hints and other things on
        widget level from the outside.
        '''
        return self._text_field

    def slider(self):
        '''
        Returns the slider for any modifications from the outside.
        '''
        return self._slider
